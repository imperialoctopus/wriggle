## 1.0.0

- Initial version.

## 1.0.1

- Move connected border component out of Dialog and into DialogSurface to allow easier customisation.

## 1.1.0

- Change to how PageViewHeader titles work to allow mixed formatting.
- Change to how AspectRatio works (add optional cover/float fitting on top of sizing).
- Modifications to FlexComponent tight sizing.
- Add KeyRelease component.
- Add a convenience dialog for SelectorFormItem.

## 1.2.0

- Add Navigator component with associated Route model.

## 1.3.0

- Added Animated mixin and support in the engine for animation.
- Fake transparency for RGBColor (as ANSI transparency doesn't work in Windows Terminal).
- Allow DialogSurface onPop to push new dialogs.

## 1.4.0

- Added support for web.
- Re-worked key input (removing the broken paste function).
- Breaking changes to Color.

## 1.5.0-dev.1

- Adds clipboard support using Rust interop (with Dart native-assets experiment).
- Re-work ScrollingTextField to improve performance with a lot of state updates (e.g. typing).

## 1.6.0-dev.1

- Remove web support (incompatible with native-assets experiment for now).
- Fixes to flex layouts using cross axis tight (may break layouts).

## 1.6.0-dev.2

- Export connected border data to allow custom connected borders.

## 1.6.0-dev.3

- Changes stateful component ".read" to provide non-null result with startingState parameter.
- Add .read to ScrollingSelector.

## 1.6.0-dev.4

- Expose state provider in ScrollingSelector.

## 1.6.0-dev.5

- Fix error in border component sizing (not applying border to cross size given to child).

## 1.6.0-dev.6

- Update Theme component to use ColorPlaceholder.

## 1.6.0-dev.7

- Add CopyPaste usage info to text fields and dialogs.

## 1.6.0-dev.8

- Stack component passes keys through to top child instead of bottom.
- Rework of DialogSurfaceComponent to fit new Stack; all example dialogs now don't absorb keys allowing control of components above the surface.

## 1.7.0-dev.1

- Add file browser.
- Remove ill-fated web support.
- Allow DialogSurfaceComponent to hold more than one route at once.
- Place render loop inside a Riverpod provider to make use of ref.watch.

## 1.7.0-dev.2

- Fix default state of file browser when given starting path.

## 1.7.0-dev.3

- Expose dependency in page view (for e.g. navigation state).

## 1.7.0-dev.4

- Fix crash in scrolling text field.

## 1.8.0

- Remove native-assets experiment flag,
- Switch to win32-clipboard for clipboard access.
- Add experimental audio engine.

## 1.8.1

- Downgrade SDK constraint to most recent stable (3.7.0).

## 1.8.2

- Fix file browser dialog defaulting to '/' path when FileBrowser defaults to ''.
