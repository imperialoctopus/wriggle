/// Exception that causes the engine to exit without an error.
class QuitApp implements Exception {
  /// Exception that causes the engine to exit without an error.
  const QuitApp();
}

/// Exception which forces an immediate screen refresh.
class RefreshScreen implements Exception {
  /// Exception which forces an immediate screen refresh.
  const RefreshScreen();
}
