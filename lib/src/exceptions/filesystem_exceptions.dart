/// Exception for file not found.
class FileNotFound implements Exception {
  /// Path of file.
  final String? message;

  /// Exception for file not found.
  const FileNotFound([this.message]);

  @override
  String toString() => 'File not found: $message';
}
