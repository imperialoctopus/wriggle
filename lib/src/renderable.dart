import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:riverpod/riverpod.dart';

import 'model/axel_map.dart';
import 'model/axis.dart';
import 'model/key_input/key_input.dart';
import 'model/usage_info.dart';

export 'package:riverpod/riverpod.dart';

/// Base class for Renderables.
@immutable
abstract class Renderable {
  /// Base class for Renderables.
  const Renderable();

  /// Produce an AxelMap of the specified size to be shown on screen.
  AxelMap render(ProviderContainer container, int width, int height);

  /// Return usage info for this renderable, or redirect to its child.
  UsageInfo getUsageInfo(ProviderContainer container);

  /// Report this item's intrinsic size, or null if it has none.
  int? getSize(ProviderContainer container, Axis axis, int crossSize);

  /// Handle key presses. Return [true] if the key was absorbed, or [false] if another listener further up the tree should handle it.
  bool keyPressed(ProviderContainer container, KeyInput key);
}
