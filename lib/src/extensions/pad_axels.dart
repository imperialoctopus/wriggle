import '../model/axel.dart';
import '../model/text_alignment.dart';

/// Resize a list of axels, padding with spaces.
extension PadAxels on Iterable<Axel> {
  /// Resize a list of axels, padding with spaces.
  List<Axel> pad(
    int width, [
    TextAlignment alignment = TextAlignment.left,
    Axel space = Axel.space,
  ]) {
    final extra = width - length;
    if (extra > 0) {
      switch (alignment) {
        case TextAlignment.left:
          return [
            ...this,
            for (int i = 0; i < extra; i++) space,
          ];
        case TextAlignment.center:
          return [
            for (int i = 0; i < (extra / 2).floor(); i++) space,
            ...this,
            for (int i = 0; i < (extra / 2).ceil(); i++) space,
          ];
        case TextAlignment.right:
          return [
            for (int i = 0; i < extra; i++) space,
            ...this,
          ];
      }
    } else {
      return [...take(width)];
    }
  }
}
