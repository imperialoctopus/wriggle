import 'dart:io';

import '../context.dart';
import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/color/color.dart';
import '../model/color/color_scheme.dart';
import '../model/color/extensions/ansi_color_extension.dart';
import '../model/key_input/key_input.dart';
import '../model/key_input/key_input_terminal.dart';
import '../packages/console/console.dart';

/// Terminal interface.
class TerminalAdapter {
  TerminalAdapter._();

  late final Console _console;
  late final bool _throwOnUnknownKey;

  /// Height of the terminal in cells.
  int height = 0;

  /// Width of the terminal in cells.
  int width = 0;

  /// Initialise this adapter.
  void init({
    required bool throwOnUnknownKey,
  }) {
    _console = Console();
    _console.rawMode = true;
    _console.hideCursor();
    _throwOnUnknownKey = throwOnUnknownKey;
    updateSize();
  }

  /// Stream of key events. Return true if the event was handled.
  Stream<void Function(bool Function(KeyInput))> get inputStream async* {
    await for (final event in stdin.asBroadcastStream()) {
      final key = KeyInputTerminal.fromRunes(event);
      if (key == KeyInput.unknown) {
        if (_throwOnUnknownKey && Wriggle.isDebugMode) {
          throw event;
        }

        final modifiableList = event.toList();
        final resultKeys = <KeyInput>[];

        while (modifiableList.isNotEmpty) {
          for (int i = modifiableList.length - 1; i >= 0; i--) {
            if (i == 0) {
              modifiableList.clear();
              break;
            }
            final key = KeyInputTerminal.fromRunes(modifiableList.take(i));
            if (key != KeyInput.unknown) {
              resultKeys.add(key);
              modifiableList.removeRange(0, i);
              break;
            }
          }
        }

        if (resultKeys.isNotEmpty) {
          if (resultKeys.length == 1) {
            yield (function) {
              function(resultKeys.single);
            };
          }
          // Dropped multiple keys here because of new KeyInput scheme.
        }
      } else {
        yield (function) {
          function(key);
        };
      }
    }
  }

  /// Render a screen of axels.
  void writeScreen(AxelMap axelMap, ColorScheme colorScheme) {
    final rows = axelMap.data;
    final buffer = StringBuffer();
    for (int i = 0; i < rows.length; i++) {
      Axel? previousAxel;
      for (final character in rows[i]) {
        buffer.write(
          buildAxelString(character, previousAxel, scheme: colorScheme),
        );
        previousAxel = character;
      }
      if (i != rows.length - 1) {
        buffer.write(_console.newLine);
      }
    }
    //console.clearScreen();
    _console.cursorPosition = const Coordinate(0, 0);
    _console.write(buffer);
  }

  /// Generate a String which produces this character, including ANSI decoration sequences.
  String buildAxelString(
    Axel axel,
    Axel? previous, {
    ColorScheme scheme = const ColorScheme(),
  }) {
    final buffer = StringBuffer();

    if (axel.textStyle != previous?.textStyle) {
      //buffer.write(ansiResetColor);
      buffer.write(
        ansiSetTextStyles(
          bold: axel.textStyle.bold,
          faint: axel.textStyle.faint,
          //italic: textStyle!.italic,
          underscore: axel.textStyle.underscore,
          //blink: textStyle!.blink,
          inverted: axel.textStyle.inverted,
          //invisible: textStyle!.invisible,
          strikethru: axel.textStyle.strikethrough,
        ),
      );
    }

    if (axel.color != previous?.color ||
        axel.background != previous?.background ||
        axel.textStyle != previous?.textStyle) {
      final backcol = axel.background.withinScheme(scheme);
      final forecol = axel.color.withinScheme(scheme);
      buffer.write(
        AnsiColorExtension.buildAnsiSequence(
          forecol.interpolateWith(backcol),
          backcol,
        ),
      );
      // if (color == null || background == null) {
      //   buffer.write(ansiResetColor);
      // }
      // if (color != null) {
      //   buffer.write(color!.ansiSetBackgroundColorSequence);
      // }
      // if (background !=  ) {
      //   buffer.write(background!.ansiSetBackgroundColorSequence);
      // }
    }

    buffer.write(axel.character.stripEscapeCharacters());
    //buffer.write(ansiResetColor);
    return buffer.toString();
  }

  /// Cleanup function called when this should be disposed.
  void onExit([Object? error]) {
    _console.rawMode = false;
    _console.clearScreen();
    _console.resetCursorPosition();
    _console.showCursor();
    _console.resetColorAttributes();
    if (error != null) {
      _console.writeErrorLine(error);
    }
    exit(0);
  }

  /// Recalculates size and margins. Returns true if the size changed.
  bool updateSize([bool forceUpdate = false]) {
    final newHeight = _console.windowHeight;
    final newWidth = _console.windowWidth;
    if (forceUpdate || height != newHeight || width != newWidth) {
      height = newHeight;
      width = newWidth;
      return true;
    }
    return false;
  }
}

/// Gets terminal adapter for your platform.
final TerminalAdapter terminalAdapter = TerminalAdapter._();
