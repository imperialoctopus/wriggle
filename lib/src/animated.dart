import 'dart:async';

import 'renderable.dart';

/// Allows UI items to be animated.
mixin Animated on Renderable {
  /// Allows this component to be animated. Causes the component to re-build
  /// every (rate * 50) milliseconds. Returned int increments by 1 each re-build.
  int animate(
    ProviderContainer container,
    int rate,
  ) {
    container.read(animationFramesWaiting.notifier).registerAnimation(rate);
    //final g = container.read(animationRebuild);

    final value =
        (container.read(baseline).value ?? 0) - startingValue(container);
    return (value / rate).floor();
  }

  /// Override to allow animations to reset based on other providers.
  int startingValue(ProviderContainer container) => 0;

  /// Animation frame number. Starts at 0 and increments by 1 every 50 milliseconds.
  static final baseline = StreamProvider<int>((ref) async* {
    int frame = 0;

    while (true) {
      yield frame;
      frame++;
      await Future<void>.delayed(const Duration(milliseconds: 50));
      ref.read(animationFramesWaiting.notifier).decrement();
    }
  });

  /// Frames remaining before animation rebuild is triggered.
  static final animationFramesWaiting =
      NotifierProvider<_AnimationFramesWaitingNotifier, int>(
    () => _AnimationFramesWaitingNotifier(),
  );

  /// Provider that's updated to cause animation rebuilds.
  static final animationRebuild =
      NotifierProvider<_AnimationRebuildNotifier, int>(
    () => _AnimationRebuildNotifier(),
  );
}

class _AnimationFramesWaitingNotifier extends Notifier<int> {
  @override
  int build() => 0;

  void decrement() {
    if (state > 0) {
      state--;
    }
  }

  void registerAnimation(int rate) {
    if (state == 0) {
      state = rate;
      return;
    }
    if (rate < state) {
      state = rate;
    }
  }
}

class _AnimationRebuildNotifier extends Notifier<int> {
  @override
  int build() {
    ref.listen(
      Animated.animationFramesWaiting,
      (previous, next) {
        if (previous != next && next == 0) {
          state++;
        }
      },
    );
    return 0;
  }
}
