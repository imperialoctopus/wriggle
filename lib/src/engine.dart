import 'dart:async';

import 'animated.dart';
import 'component.dart';
import 'context.dart';
import 'dialog/dialog_surface.dart';
import 'exceptions/quit_app.dart';
import 'packages/hot_reload/hot_reload.dart';
import 'provider_observer.dart';
import 'provides_application_context.dart';
import 'renderable.dart';
import 'services/audio/audio_adapter.dart';
import 'terminal_adapter/terminal_adapter.dart';

/// Run the provided component as a full-screen application, passing keyboard input to it.
Future<void> runApp(
  Component application, {

  /// Throws an error if an unknown key is pressed in debug mode.
  bool throwOnUnknownKey = false,

  /// Enabled hot reloading in debug mode.
  bool hotReload = true,
}) async {
  bool needsUpdate = true;
  Timer? sizeCheckTimer;
  Timer? frameCheckTimer;

  HotReload? reloader;
  if (hotReload && Wriggle.isDebugMode) {
    try {
      reloader = HotReload.reloader;
      reloader.init(() => needsUpdate = true);
    } catch (_) {}
  }

  final container = ProviderContainer(
    observers: [EngineProviderObserver(onUpdate: () => needsUpdate = true)],
  );

  final terminal = terminalAdapter;
  terminal.init(throwOnUnknownKey: throwOnUnknownKey);
  //terminal.updateSize(true);

  // attempt to initialise audio library
  initialiseAudioAdapter();

  void onExit([Object? error]) {
    sizeCheckTimer?.cancel();
    frameCheckTimer?.cancel();
    reloader?.stop();
    terminal.onExit(error);
  }

  Future<void> renderScreen(ProviderContainer container) async {
    needsUpdate = false;
    final withDialogs = DialogSurface(
      child: application,
      connectedBorder:
          (application is ProvidesApplicationContext)
              ? application.getDialogBorders(container)
              : ProvidesApplicationContext.defaultDialogBorders,
    );
    final axelMap = withDialogs.render(
      container,
      terminal.width,
      terminal.height,
    );
    final colorScheme =
        ((application is ProvidesApplicationContext)
            ? application.getColorScheme(container)
            : null) ??
        ProvidesApplicationContext.defaultColorScheme;
    return terminal.writeScreen(axelMap, colorScheme);
  }

  Timer setSizeCheckTimer() {
    return Timer(const Duration(milliseconds: 300), () async {
      if (terminal.updateSize()) {
        needsUpdate = true;
      }
      sizeCheckTimer?.cancel();
      sizeCheckTimer = setSizeCheckTimer();
    });
  }

  Timer setFrameCheckTimer() {
    return Timer(const Duration(milliseconds: 1), () async {
      if (needsUpdate) {
        await renderScreen(container);
      }
      frameCheckTimer?.cancel();
      frameCheckTimer = setFrameCheckTimer();
    });
  }

  try {
    container.read(Animated.animationRebuild);
    await renderScreen(container);

    sizeCheckTimer = setSizeCheckTimer();
    frameCheckTimer = setFrameCheckTimer();

    final withDialogs = DialogSurface(
      child: application,
      connectedBorder:
          (application is ProvidesApplicationContext)
              ? application.getDialogBorders(container)
              : ProvidesApplicationContext.defaultDialogBorders,
    );

    // Keyboard polling loop
    await for (final handler in terminal.inputStream) {
      try {
        handler((key) => withDialogs.keyPressed(container, key));
        await container.pump();
      } on RefreshScreen {
        needsUpdate = true;
        await container.pump();
      } on QuitApp {
        onExit();
      } catch (error) {
        onExit(error);
      }
    }
  }
  // Make sure raw mode gets disabled if we hit some unhandled problem
  catch (exception) {
    onExit(exception);
  }
}
