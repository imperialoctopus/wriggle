import 'hot_reload.dart';

/// Get the hot reloader for your platform.
HotReload get hotReloaderStub =>
    throw UnsupportedError('Not supported on this platform');
