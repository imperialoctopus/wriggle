import 'hot_reload_stub.dart' if (dart.library.io) 'hot_reload_io.dart';

/// Hot reloader wrapper for cross-platform support.
abstract interface class HotReload {
  /// Get the hot reloader for your platform.
  static HotReload get reloader => hotReloaderStub;

  const HotReload();

  /// Initialises the reloader.
  Future<void> init(void Function() doReload);

  /// Cancels hot reloader.
  Future<void> stop();
}
