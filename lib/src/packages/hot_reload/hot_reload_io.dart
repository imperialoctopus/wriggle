import 'package:hotreloader/hotreloader.dart';

import 'hot_reload.dart';

/// Hot reloader for platforms with dart:io.
class HotReloadIo implements HotReload {
  HotReloader? _hotReloader;

  /// Hot reloader for platforms with dart:io.
  HotReloadIo();

  @override
  Future<void> init(void Function() doReload) async {
    _hotReloader = await HotReloader.create(onAfterReload: (_) => doReload());
  }

  @override
  Future<void> stop() async {
    return _hotReloader?.stop();
  }
}

/// Get the hot reloader for your platform.
HotReload get hotReloaderStub => HotReloadIo();
