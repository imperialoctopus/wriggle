import 'component.dart';

/// A promise for dialogs to return an object of T type.
mixin RouteContract<T> on Component {}
