import 'axel.dart';

/// Holds data describing a border.
class BorderData {
  /// Space to the left of the child.
  final int left;

  /// Space to the top of the child.
  final int top;

  /// Space to the right of the child.
  final int right;

  /// Space to the bottom of the child.
  final int bottom;

  /// Axel to fill the border with. Defaults to Axel.space.
  final Axel fill;

  /// Custom border describing all four directions.
  const BorderData({
    this.left = 0,
    this.top = 0,
    this.right = 0,
    this.bottom = 0,
    this.fill = Axel.space,
  });

  /// [vertical] affects top and bottom together, [horizontal] affects left and right together.
  const BorderData.symmetric({
    int vertical = 0,
    int horizontal = 0,
    this.fill = Axel.space,
  })  : bottom = vertical,
        top = vertical,
        right = horizontal,
        left = horizontal;

  /// Produces a border of [thickness] thickness in all directions.
  const BorderData.all(int thickness, {this.fill = Axel.space})
      : left = thickness,
        top = thickness,
        right = thickness,
        bottom = thickness;
}
