import 'axel.dart';
import 'color/color.dart';
import 'text_style.dart';

/// Two-dimensional array of axels.
class AxelMap extends Iterable<Iterable<Axel>> {
  /// Contained data.
  final List<List<Axel>> data;

  /// Two-dimensional array of axels.
  AxelMap(Iterable<Iterable<Axel>> axels)
      : assert(!axels.any((row) => row.length != axels.first.length)),
        data = axels.map((e) => e.toList()).toList();

  /// Generate a map containing spaces.
  AxelMap.empty(int width, int height) : this.filled(width, height, Axel.space);

  /// Generate a map filled with the provided axel.
  AxelMap.filled(int width, int height, Axel axel)
      : data = List.generate(
          height,
          (_) => List.generate(
            width,
            (_) => axel,
          ),
        );

  @override
  Iterator<Iterable<Axel>> get iterator => data.iterator;

  /// Height of this map.
  int get height => data.length;

  /// Width of this map.
  int get width => data.firstOrNull?.length ?? 0;

  /// Returns a copy of this map rotated by 90 degrees.
  Iterable<Iterable<Axel>> get asColumns {
    return Iterable<int>.generate(width).map(
      (i) => Iterable<int>.generate(height).map((j) => data[j][i]),
    );
  }

  /// Get axel from the specified position.
  Axel? getPosition(int x, int y) {
    if (x < 0 || x >= width) {
      return null;
    }
    if (y < 0 || y >= height) {
      return null;
    }
    return data[y][x];
  }

  /// Update a single axel.
  void setPosition(int x, int y, Axel axel) {
    data[y][x] = axel;
  }

  /// Add a row. Defaults to the bottom, top with [start].
  /// Length must be equal to this map's [width].
  void addRow(Iterable<Axel> row, {bool start = false}) {
    if (row.length != width) {
      throw ArgumentError('Cannot add row of invalid size.');
    }
    if (start) {
      data.insert(0, row.toList());
    } else {
      data.add(row.toList());
    }
  }

  /// Add multiple rows as with [addRow].
  void addRows(Iterable<Iterable<Axel>> rows, {bool start = false}) {
    if (start) {
      for (final row in rows.toList().reversed) {
        addRow(row, start: true);
      }
    } else {
      for (final row in rows) {
        addRow(row);
      }
    }
  }

  /// Add a column to the right of this map. Left with []
  void addColumn(Iterable<Axel> column, {bool start = false}) {
    if (column.length != height) {
      throw ArgumentError('Cannot add column of invalid size.');
    }
    if (start) {
      int i = 0;
      for (final element in column) {
        data[i].insert(0, element);
        i++;
      }
    } else {
      int i = 0;
      for (final element in column) {
        data[i].add(element);
        i++;
      }
    }
  }

  /// Add columns to this map. Columns must all be the same size, and the same length as the height of this map.
  /// [start] causes them to be added to the left side of the map instead of the right.
  void addColumns(Iterable<Iterable<Axel>> columns, {bool start = false}) {
    if (start) {
      for (final column in columns.toList().reversed) {
        addColumn(column, start: true);
      }
    } else {
      for (final column in columns) {
        addColumn(column);
      }
    }
  }

  /// Remove [number] columns from the end (or start if [start]).
  void removeColumns(int number, {bool start = false}) {
    if (number < 1) {
      return;
    }
    if (start) {
      for (final row in data) {
        row.removeRange(0, number);
      }
    } else {
      for (final row in data) {
        row.removeRange(row.length - number, row.length);
      }
    }
  }

  /// Remove [number] rows from end, or start if [start].
  void removeRows(int number, {bool start = false}) {
    if (number < 1) {
      return;
    }
    if (start) {
      data.removeRange(0, number);
    } else {
      data.removeRange(data.length - number, data.length);
    }
  }

  /// Resizes this map to the provided sizes.
  /// horizontal/vertical start adds/removes lines from the end of the map.
  void resize(
    int? width,
    int? height, {
    bool horizontalStart = true,
    bool verticalStart = true,
  }) {
    if (height != null) {
      if (height > this.height) {
        pad(null, height, verticalStart: !verticalStart);
      } else if (height < this.height) {
        removeRows(this.height - height, start: !verticalStart);
      }
    }
    if (width != null) {
      if (width > this.width) {
        pad(width, null, horizontalStart: !horizontalStart);
      } else if (width < this.width) {
        removeColumns(this.width - width, start: !horizontalStart);
      }
    }
  }

  /// Resizes this map to the provided sizes, keeping it centred.
  void resizeCentered(int? width, int? height) {
    if (height != null) {
      if (height > this.height) {
        pad(null, height - ((height - this.height) / 2).floor());
        pad(null, height, verticalStart: false);
      } else if (height < this.height) {
        removeRows(((this.height - height) / 2).floor());
        removeRows(((this.height - height) / 2).ceil(), start: true);
      }
    }
    if (width != null) {
      if (width > this.width) {
        pad(width - ((width - this.width) / 2).floor(), null);
        pad(width, null, horizontalStart: false);
      } else if (width < this.width) {
        removeColumns(((this.width - width) / 2).floor());
        removeColumns(((this.width - width) / 2).ceil(), start: true);
      }
    }
  }

  /// Increases this map's size by padding it with spaces.
  void pad(
    int? width,
    int? height, {
    bool horizontalStart = true,
    bool verticalStart = true,
  }) {
    if (width != null && width != this.width) {
      if (width < this.width) {
        throw ArgumentError('Cannot decrease width with pad.');
      }
      final gain = width - this.width;
      for (final row in data) {
        if (horizontalStart) {
          row.addAll(Axel.space * gain);
        } else {
          row.insertAll(0, Axel.space * gain);
        }
      }
    }
    if (height != null && height != this.height) {
      final gain = height - this.height;
      if (gain < 0) {
        throw ArgumentError('Cannot decrease height with pad.');
      }
      if (verticalStart) {
        data.addAll(
          List.generate(
            gain,
            (_) => List.filled(this.width, Axel.space, growable: true),
          ),
        );
      } else {
        data.insertAll(
          0,
          List.generate(
            gain,
            (_) => List.filled(this.width, Axel.space, growable: true),
          ),
        );
      }
    }
  }

  /// Update a single axel in the provided position.
  void editAxel(int row, int col, Axel Function(Axel) editFunction) {
    data[row][col] = editFunction(data[row][col]);
  }

  /// Replace the [color], [background], or [textStyle] of all axels.
  void overrideStyle({
    ColorPlaceholder? color,
    ColorPlaceholder? background,
    TextStyle? textStyle,
  }) {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        data[i][j] = textStyle != null
            ? data[i][j].copyWith(
                color: color ?? Colors.foreground,
                background: background ?? Colors.background,
                textStyle: textStyle,
              )
            : data[i][j].copyWith(
                color: color ?? Colors.foreground,
                background: background ?? Colors.background,
              );
      }
    }
  }
}
