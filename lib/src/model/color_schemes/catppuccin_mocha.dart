import '../color/color.dart';

/// Colour scheme using Windows Terminal Catppuccin Mocha colours.
class CatppuccinMochaColorScheme extends ColorScheme {
  /// Colour scheme using Windows Terminal Catppuccin Mocha colours.
  const CatppuccinMochaColorScheme();

  @override
  Color get background => const Color.fromInt(0xFF1E1E2E);
  @override
  Color get foreground => const Color.fromInt(0xFFCDD6F4);
  @override
  Color get black => const Color.fromInt(0xFF45475A);
  @override
  Color get blue => const Color.fromInt(0xFF89B4FA);
  @override
  Color get gray => const Color.fromInt(0xFF585B70);
  @override
  Color get lightBlue => const Color.fromInt(0xFF89B4FA);
  @override
  Color get aqua => const Color.fromInt(0xFF94E2D5);
  @override
  Color get lightGreen => const Color.fromInt(0xFFA6E3A1);
  @override
  Color get lightPurple => const Color.fromInt(0xFFF5C2E7);
  @override
  Color get lightRed => const Color.fromInt(0xFFF38BA8);
  @override
  Color get lightGray => const Color.fromInt(0xFFA6ADC8);
  @override
  Color get lightYellow => const Color.fromInt(0xFFF9E2AF);
  @override
  Color get cyan => const Color.fromInt(0xFF94E2D5);
  @override
  Color get green => const Color.fromInt(0xFFA6E3A1);
  @override
  Color get purple => const Color.fromInt(0xFFF5C2E7);
  @override
  Color get red => const Color.fromInt(0xFFF38BA8);
  @override
  Color get white => const Color.fromInt(0xFFBAC2DE);
  @override
  Color get yellow => const Color.fromInt(0xFFF9E2AF);
}
