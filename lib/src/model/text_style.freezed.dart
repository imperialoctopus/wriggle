// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'text_style.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$TextStyle {
  bool get bold => throw _privateConstructorUsedError;
  bool get faint => throw _privateConstructorUsedError;
  bool get underscore => throw _privateConstructorUsedError;
  bool get inverted => throw _privateConstructorUsedError;
  bool get strikethrough => throw _privateConstructorUsedError;
  bool get italic => throw _privateConstructorUsedError;

  /// Create a copy of TextStyle
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $TextStyleCopyWith<TextStyle> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TextStyleCopyWith<$Res> {
  factory $TextStyleCopyWith(TextStyle value, $Res Function(TextStyle) then) =
      _$TextStyleCopyWithImpl<$Res, TextStyle>;
  @useResult
  $Res call(
      {bool bold,
      bool faint,
      bool underscore,
      bool inverted,
      bool strikethrough,
      bool italic});
}

/// @nodoc
class _$TextStyleCopyWithImpl<$Res, $Val extends TextStyle>
    implements $TextStyleCopyWith<$Res> {
  _$TextStyleCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of TextStyle
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? bold = null,
    Object? faint = null,
    Object? underscore = null,
    Object? inverted = null,
    Object? strikethrough = null,
    Object? italic = null,
  }) {
    return _then(_value.copyWith(
      bold: null == bold
          ? _value.bold
          : bold // ignore: cast_nullable_to_non_nullable
              as bool,
      faint: null == faint
          ? _value.faint
          : faint // ignore: cast_nullable_to_non_nullable
              as bool,
      underscore: null == underscore
          ? _value.underscore
          : underscore // ignore: cast_nullable_to_non_nullable
              as bool,
      inverted: null == inverted
          ? _value.inverted
          : inverted // ignore: cast_nullable_to_non_nullable
              as bool,
      strikethrough: null == strikethrough
          ? _value.strikethrough
          : strikethrough // ignore: cast_nullable_to_non_nullable
              as bool,
      italic: null == italic
          ? _value.italic
          : italic // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TextStyleImplCopyWith<$Res>
    implements $TextStyleCopyWith<$Res> {
  factory _$$TextStyleImplCopyWith(
          _$TextStyleImpl value, $Res Function(_$TextStyleImpl) then) =
      __$$TextStyleImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool bold,
      bool faint,
      bool underscore,
      bool inverted,
      bool strikethrough,
      bool italic});
}

/// @nodoc
class __$$TextStyleImplCopyWithImpl<$Res>
    extends _$TextStyleCopyWithImpl<$Res, _$TextStyleImpl>
    implements _$$TextStyleImplCopyWith<$Res> {
  __$$TextStyleImplCopyWithImpl(
      _$TextStyleImpl _value, $Res Function(_$TextStyleImpl) _then)
      : super(_value, _then);

  /// Create a copy of TextStyle
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? bold = null,
    Object? faint = null,
    Object? underscore = null,
    Object? inverted = null,
    Object? strikethrough = null,
    Object? italic = null,
  }) {
    return _then(_$TextStyleImpl(
      bold: null == bold
          ? _value.bold
          : bold // ignore: cast_nullable_to_non_nullable
              as bool,
      faint: null == faint
          ? _value.faint
          : faint // ignore: cast_nullable_to_non_nullable
              as bool,
      underscore: null == underscore
          ? _value.underscore
          : underscore // ignore: cast_nullable_to_non_nullable
              as bool,
      inverted: null == inverted
          ? _value.inverted
          : inverted // ignore: cast_nullable_to_non_nullable
              as bool,
      strikethrough: null == strikethrough
          ? _value.strikethrough
          : strikethrough // ignore: cast_nullable_to_non_nullable
              as bool,
      italic: null == italic
          ? _value.italic
          : italic // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$TextStyleImpl implements _TextStyle {
  const _$TextStyleImpl(
      {this.bold = false,
      this.faint = false,
      this.underscore = false,
      this.inverted = false,
      this.strikethrough = false,
      this.italic = false});

  @override
  @JsonKey()
  final bool bold;
  @override
  @JsonKey()
  final bool faint;
  @override
  @JsonKey()
  final bool underscore;
  @override
  @JsonKey()
  final bool inverted;
  @override
  @JsonKey()
  final bool strikethrough;
  @override
  @JsonKey()
  final bool italic;

  @override
  String toString() {
    return 'TextStyle(bold: $bold, faint: $faint, underscore: $underscore, inverted: $inverted, strikethrough: $strikethrough, italic: $italic)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TextStyleImpl &&
            (identical(other.bold, bold) || other.bold == bold) &&
            (identical(other.faint, faint) || other.faint == faint) &&
            (identical(other.underscore, underscore) ||
                other.underscore == underscore) &&
            (identical(other.inverted, inverted) ||
                other.inverted == inverted) &&
            (identical(other.strikethrough, strikethrough) ||
                other.strikethrough == strikethrough) &&
            (identical(other.italic, italic) || other.italic == italic));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, bold, faint, underscore, inverted, strikethrough, italic);

  /// Create a copy of TextStyle
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$TextStyleImplCopyWith<_$TextStyleImpl> get copyWith =>
      __$$TextStyleImplCopyWithImpl<_$TextStyleImpl>(this, _$identity);
}

abstract class _TextStyle implements TextStyle {
  const factory _TextStyle(
      {final bool bold,
      final bool faint,
      final bool underscore,
      final bool inverted,
      final bool strikethrough,
      final bool italic}) = _$TextStyleImpl;

  @override
  bool get bold;
  @override
  bool get faint;
  @override
  bool get underscore;
  @override
  bool get inverted;
  @override
  bool get strikethrough;
  @override
  bool get italic;

  /// Create a copy of TextStyle
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$TextStyleImplCopyWith<_$TextStyleImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
