/// Represents axes; horizontal or vertical.
enum Axis {
  /// Horizontal axis.
  horizontal,

  /// Vertical axis.
  vertical;

  /// Gets the other axis.
  Axis get cross => switch (this) {
        Axis.horizontal => Axis.vertical,
        Axis.vertical => Axis.horizontal,
      };
}
