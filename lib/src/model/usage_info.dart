/// Information informing the user of a hotkey.
/// List of pairs containing the [key] to press and a short description of the effect of that key, [name].
typedef UsageInfo = List<({String key, String name})>;

/// Extension to allow usage info lists to be added.
extension UsageInfoExt on UsageInfo {
  /// Combines the data from two UsageInfo instances.
  UsageInfo operator +(UsageInfo other) {
    if (isEmpty) {
      return other;
    }
    if (other.isEmpty) {
      return this;
    }
    return [
      ...this,
      ...other,
    ];
  }
}
