import 'package:freezed_annotation/freezed_annotation.dart';

import 'color/color.dart';
import 'text_style.dart';

part 'axel.freezed.dart';

/// Model of a single character used. Portmanteau of "ASCII" and "pixel".
@freezed
class Axel with _$Axel {
  /// Model of a single character used. Portmanteau of "ASCII" and "pixel".
  /// [character] should be a single-character string. Longer strings are not clipped (to support strange unicode characters) but will break rendering.
  /// [color] is foreground color, [background] background.
  /// [textStyle] is self-explanatory.
  /// [flag] should be 0 for user-created Axels.
  /// 1 represents border placeholder Axels, 2 represents opaque spaces (used in the Overlay component).
  /// 3+ are currently undefined.
  const factory Axel(
    String character, {
    @Default(Colors.foreground) ColorPlaceholder color,
    @Default(Colors.background) ColorPlaceholder background,
    @Default(TextStyle()) TextStyle textStyle,
    @Default(0) int flag,
  }) = _Axel;

  const Axel._();

  /// Produces a list of duplicates of this Axel.
  List<Axel> operator *(int multiple) {
    return List.filled(multiple, this);
  }

  /// Is this Axel a border placeholder?
  bool get isBorder => flag == borderPlaceholder.flag;

  /// Empty space.
  static const Axel space = Axel(' ');

  /// Space used in Overlay component for dialogs.
  static const Axel opaqueSpace = Axel(' ', flag: 2);

  /// Axel used to end rows in rendering. Do not use.
  static const Axel newLine = Axel('\n');

  /// Border placeholder. Add a ConnectedBorder parent to replace with connected borders.
  static const Axel borderPlaceholder = Axel('@', flag: 1);

  /// Border placeholder which only connects horizontally.
  static const Axel borderPlaceholderHorizontal = Axel('-', flag: 1);

  /// Border placeholder which only connects vertically.
  static const Axel borderPlaceholderVertical = Axel('|', flag: 1);
}
