import 'package:freezed_annotation/freezed_annotation.dart';

part 'text_style.freezed.dart';

/// Data for styling text.
@freezed
class TextStyle with _$TextStyle {
  /// Data for styling text. Each parameter flags its respective ANSI text decoration.
  const factory TextStyle({
    @Default(false) bool bold,
    @Default(false) bool faint,
    @Default(false) bool underscore,
    @Default(false) bool inverted,
    @Default(false) bool strikethrough,
    @Default(false) bool italic,
  }) = _TextStyle;
}

/// Convenience classes for different text decorations.
class TextStyles {
  /// Text style producing bold text.
  static const TextStyle bold = TextStyle(bold: true);

  /// Text style producing faint text.
  static const TextStyle faint = TextStyle(faint: true);

  /// Text style producing underscored text.
  static const TextStyle underscore = TextStyle(underscore: true);

  /// Text style producing inverted text.
  static const TextStyle inverted = TextStyle(inverted: true);

  /// Text style producing strikethrough-ed text.
  static const TextStyle strikethrough = TextStyle(strikethrough: true);

  /// Text style producing italic text.
  static const TextStyle italic = TextStyle(italic: true);
}
