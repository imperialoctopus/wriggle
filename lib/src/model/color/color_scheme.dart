import 'color.dart';

/// Defines a map from colour placeholder to concrete colour.
/// Used to re-colour your application.
class ColorScheme {
  /// Default colour used for the background.
  Color get background => const ResetColor();

  /// Default colour used for foreground items.
  Color get foreground => const ResetColor();

  /// The colour of the night sky.
  Color get black => const Color.fromInt(0xFF000000);

  /// Physically impossible black.
  Color get trueBlack => const Color.fromInt(0xFF000000);

  /// A colour that clouds seldom are.
  Color get white => const Color.fromInt(0xFFFFFFFF);

  /// Like white but brighter.
  Color get trueWhite => const Color.fromInt(0xFFFFFFFF);

  /// A colour that clouds sometimes are.
  Color get lightGray => const Color.fromInt(0xFFC0C0C0);

  /// The colour that clouds usually are.
  Color get gray => const Color.fromInt(0xFF808080);

  /// Graphitic.
  Color get darkGray => const Color.fromInt(0xFF404040);

  /// More of a tan.
  Color get lightBrown => const Color.fromInt(0xFFBE9664);

  /// Clay, dirt, and scales.
  Color get brown => const Color.fromInt(0xFFA06E3C);

  /// Too brown for my tastes.
  Color get darkBrown => const Color.fromInt(0xFF644020);

  /// Hunger, lust, the drowning waters.
  Color get red => const Color.fromInt(0xFFDC0000);

  /// Blood in the water.
  Color get lightRed => const Color.fromInt(0xFFFFA0A0);

  /// More of a maroon.
  Color get darkRed => const Color.fromInt(0xFF640000);

  /// Soft as a peach.
  Color get lightOrange => const Color.fromInt(0xFFFFC8AA);

  /// Sharp as an orange.
  Color get orange => const Color.fromInt(0xFFFF8000);

  /// Just past sunset.
  Color get darkOrange => const Color.fromInt(0xFF804000);

  /// Danger, disease, and sunflowers. A primary colour in printing.
  Color get yellow => const Color.fromInt(0xFFFFFF00);

  /// A softer colour, too gentle for sunlight.
  Color get lightYellow => const Color.fromInt(0xFFFFFF96);

  /// The glimmer of gold.
  Color get darkYellow => const Color.fromInt(0xFFFFC000);

  /// Pretty green grass. A primary colour of light.
  Color get green => const Color.fromInt(0xFF008000);

  /// Colour that limes seldom are.
  Color get lightGreen => const Color.fromInt(0xFF82FF5A);

  /// Leaves in shade.
  Color get darkGreen => const Color.fromInt(0xFF004000);

  /// Even LIGHTER than cyan.
  Color get aqua => const Color.fromInt(0xFF80FFFF);

  /// Like a tropical ocean. Primary colour in printing.
  Color get cyan => const Color.fromInt(0xFF00FFFF);

  /// Like cyan but darker.
  Color get turquoise => const Color.fromInt(0xFF008080);

  /// Like cyan but less shinier.
  Color get lightTurquoise => const Color.fromInt(0xFF40E0D0);

  /// A colour that the sky can be.
  Color get lightBlue => const Color.fromInt(0xFF80A0FF);

  /// A colour that the sky can't be. A primary colour of light.
  Color get blue => const Color.fromInt(0xFF0040FF);

  /// Like blue but darker.
  Color get darkBlue => const Color.fromInt(0xFF0025A8);

  /// Not dissimilar to the colour of violets.
  Color get lightPurple => const Color.fromInt(0xFFC88CFF);

  /// The colour of royalty.
  Color get purple => const Color.fromInt(0xFF8000FF);

  /// The colour of magic.
  Color get darkPurple => const Color.fromInt(0xFF400080);

  /// The colour of skin tones when nobody appreciates Freddie. A primary colour in printing.
  Color get magenta => const Color.fromInt(0xFFFF00FF);

  /// A colour for fiction.
  Color get pink => const Color.fromInt(0xFFF5C2E7);

  final Map<Colors, Color>? _overrides;

  /// Defines a map from colour placeholder to concrete colour.
  /// Used to re-colour your application.
  const ColorScheme() : _overrides = null;

  /// Construct a color scheme from a map of placeholders to concrete colours.
  const ColorScheme.fromMap(Map<Colors, Color> data) : _overrides = data;

  /// Get colour for a placeholder within a colour scheme.
  Color convertPlaceholder(Colors placeholder) =>
      _overrides?[placeholder] ??
      switch (placeholder) {
        Colors.background => background,
        Colors.foreground => foreground,
        Colors.black => black,
        Colors.trueBlack => trueBlack,
        Colors.white => white,
        Colors.trueWhite => trueWhite,
        Colors.lightGray => lightGray,
        Colors.gray => gray,
        Colors.darkGray => darkGray,
        Colors.lightBrown => lightBrown,
        Colors.brown => brown,
        Colors.darkBrown => darkBrown,
        Colors.red => red,
        Colors.lightRed => lightRed,
        Colors.darkRed => darkRed,
        Colors.lightOrange => lightOrange,
        Colors.orange => orange,
        Colors.darkOrange => darkOrange,
        Colors.yellow => yellow,
        Colors.lightYellow => lightYellow,
        Colors.darkYellow => darkYellow,
        Colors.green => green,
        Colors.lightGreen => lightGreen,
        Colors.darkGreen => darkGreen,
        Colors.aqua => aqua,
        Colors.cyan => cyan,
        Colors.turquoise => turquoise,
        Colors.lightTurquoise => lightTurquoise,
        Colors.lightBlue => lightBlue,
        Colors.blue => blue,
        Colors.darkBlue => darkBlue,
        Colors.lightPurple => lightPurple,
        Colors.purple => purple,
        Colors.darkPurple => darkPurple,
        Colors.magenta => magenta,
        Colors.pink => pink,
      };
}
