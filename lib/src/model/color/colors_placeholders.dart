import 'color.dart';

/// Placeholders for common colours.
enum Colors implements ColorPlaceholder {
  /// Default colour used for the background.
  background,

  /// Default colour used for foreground items.
  foreground,

  /// The colour of the night sky.
  black,

  /// Physically impossible black.
  trueBlack,

  /// A colour that clouds seldom are.
  white,

  /// Like white but brighter.
  trueWhite,

  /// A colour that clouds sometimes are.
  lightGray,

  /// The colour that clouds usually are.
  gray,

  /// Graphitic.
  darkGray,

  /// More of a tan.
  lightBrown,

  /// Clay, dirt, and scales.
  brown,

  /// Too brown for my tastes.
  darkBrown,

  /// Hunger, lust, the drowning waters.
  red,

  /// Blood in the water.
  lightRed,

  /// More of a maroon.
  darkRed,

  /// Soft as a peach.
  lightOrange,

  /// Sharp as an orange.
  orange,

  /// Just past sunset.
  darkOrange,

  /// Danger, disease, and sunflowers. A primary colour in printing.
  yellow,

  /// A softer colour, too gentle for sunlight.
  lightYellow,

  /// The glimmer of gold.
  darkYellow,

  /// Pretty green grass. A primary colour of light.
  green,

  /// Colour that limes seldom are.
  lightGreen,

  /// Leaves in shade.
  darkGreen,

  /// Even LIGHTER than cyan.
  aqua,

  /// Like a tropical ocean. Primary colour in printing.
  cyan,

  /// Like cyan but darker.
  turquoise,

  /// Like cyan but less shiny.
  lightTurquoise,

  /// A colour that the sky can be.
  lightBlue,

  /// A colour that the sky can't be. A primary colour of light.
  blue,

  /// Like blue but darker.
  darkBlue,

  /// Not dissimilar to the colour of violets.
  lightPurple,

  /// The colour of royalty.
  purple,

  /// The colour of magic.
  darkPurple,

  /// The colour of skin tones when nobody appreciates Freddie. A primary colour in printing.
  magenta,

  /// A colour for fiction.
  pink;

  @override
  Color withinScheme(ColorScheme scheme) => scheme.convertPlaceholder(this);
}
