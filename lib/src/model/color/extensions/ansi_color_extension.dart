import '../color.dart';

/// Extension for Color intended for use in a context with dart:io.
extension AnsiColorExtension on Color {
  /// Build ANSI sequence for this colour as a foreground.
  String get ansiForegroundSequence {
    if (this is ResetColor) {
      return ansiResetForeground;
    }
    return (a != 255) ? '38;2;$r;$g;$b;$a' : '38;2;$r;$g;$b';
  }

  /// Build ANSI sequence for this colour as a background.
  String get ansiBackgroundSequence {
    if (this is ResetColor) {
      return ansiResetBackground;
    }
    return (a != 255) ? '48;2;$r;$g;$b;$a' : '48;2;$r;$g;$b';
  }

  /// String to reset foreground.
  static String get ansiResetForeground => '39';

  /// String to reset background.
  static String get ansiResetBackground => '49';

  /// Combine ANSI sequences.
  static String buildAnsiSequence(Color? foreground, Color? background) =>
      '\x1b[${foreground?.ansiForegroundSequence ?? ansiResetForeground};${background?.ansiBackgroundSequence ?? ansiResetBackground}m';
}
