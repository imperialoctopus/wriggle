import 'color_scheme.dart';

export 'color_scheme.dart';
export 'colors_placeholders.dart';

/// Resets colour to the default.
class ResetColor extends Color {
  /// Resets colour to the default.
  const ResetColor() : super(0, 0, 0);
}

/// Placeholder for a color within a color scheme.
abstract interface class ColorPlaceholder {
  /// Get this color within a given color scheme.
  Color withinScheme(ColorScheme scheme);
}

/// Complex colour specifying RGBA values.
class Color implements ColorPlaceholder {
  /// Red channel.
  final int r;

  /// Green channel.
  final int g;

  /// Blue channel.
  final int b;

  /// Alpha channel.
  final int a;

  /// Complex colour specifying RGBA values.
  const Color(this.r, this.g, this.b, [this.a = 255]);

  /// Construct a colour from the lower 32 bits of an int.
  /// Ordered as 0xAARRGGBB
  /// E.g. 0xFFFFFFFF
  const Color.fromInt(int value)
      : a = (0xff000000 & value) >> 24,
        r = (0x00ff0000 & value) >> 16,
        g = (0x0000ff00 & value) >> 8,
        b = (0x000000ff & value) >> 0;

  /// Construct a colour from a hex string.
  Color.fromHex(String hex)
      : this.fromInt(int.parse(hex.replaceAll('#', ''), radix: 16));

  /// Blend this colour with a background based on our alpha.
  Color interpolateWith(Color background) {
    if (this is ResetColor) {
      return this;
    }
    if (background is ResetColor) {
      return Color(
        (r * a / 255).floor(),
        (g * a / 255).floor(),
        (b * a / 255).floor(),
      );
    }
    if (a == 255) {
      return this;
    }
    if (a == 0) {
      return background;
    }
    return Color(
      (r * a / 255).floor() + (background.r * (255 - a) / 255).floor(),
      (g * a / 255).floor() + (background.g * (255 - a) / 255).floor(),
      (b * a / 255).floor() + (background.b * (255 - a) / 255).floor(),
    );
  }

  @override
  int get hashCode => a.hashCode ^ r.hashCode ^ g.hashCode ^ b.hashCode;

  @override
  bool operator ==(Object other) {
    if (other is Color) {
      return a == other.a && r == other.r && g == other.g && b == other.b;
    }
    return false;
  }

  @override
  Color withinScheme(ColorScheme scheme) => this;
}
