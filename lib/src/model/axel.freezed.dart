// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'axel.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Axel {
  String get character => throw _privateConstructorUsedError;
  ColorPlaceholder get color => throw _privateConstructorUsedError;
  ColorPlaceholder get background => throw _privateConstructorUsedError;
  TextStyle get textStyle => throw _privateConstructorUsedError;
  int get flag => throw _privateConstructorUsedError;

  /// Create a copy of Axel
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $AxelCopyWith<Axel> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AxelCopyWith<$Res> {
  factory $AxelCopyWith(Axel value, $Res Function(Axel) then) =
      _$AxelCopyWithImpl<$Res, Axel>;
  @useResult
  $Res call(
      {String character,
      ColorPlaceholder color,
      ColorPlaceholder background,
      TextStyle textStyle,
      int flag});

  $TextStyleCopyWith<$Res> get textStyle;
}

/// @nodoc
class _$AxelCopyWithImpl<$Res, $Val extends Axel>
    implements $AxelCopyWith<$Res> {
  _$AxelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Axel
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? character = null,
    Object? color = null,
    Object? background = null,
    Object? textStyle = null,
    Object? flag = null,
  }) {
    return _then(_value.copyWith(
      character: null == character
          ? _value.character
          : character // ignore: cast_nullable_to_non_nullable
              as String,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as ColorPlaceholder,
      background: null == background
          ? _value.background
          : background // ignore: cast_nullable_to_non_nullable
              as ColorPlaceholder,
      textStyle: null == textStyle
          ? _value.textStyle
          : textStyle // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      flag: null == flag
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }

  /// Create a copy of Axel
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $TextStyleCopyWith<$Res> get textStyle {
    return $TextStyleCopyWith<$Res>(_value.textStyle, (value) {
      return _then(_value.copyWith(textStyle: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$AxelImplCopyWith<$Res> implements $AxelCopyWith<$Res> {
  factory _$$AxelImplCopyWith(
          _$AxelImpl value, $Res Function(_$AxelImpl) then) =
      __$$AxelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String character,
      ColorPlaceholder color,
      ColorPlaceholder background,
      TextStyle textStyle,
      int flag});

  @override
  $TextStyleCopyWith<$Res> get textStyle;
}

/// @nodoc
class __$$AxelImplCopyWithImpl<$Res>
    extends _$AxelCopyWithImpl<$Res, _$AxelImpl>
    implements _$$AxelImplCopyWith<$Res> {
  __$$AxelImplCopyWithImpl(_$AxelImpl _value, $Res Function(_$AxelImpl) _then)
      : super(_value, _then);

  /// Create a copy of Axel
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? character = null,
    Object? color = null,
    Object? background = null,
    Object? textStyle = null,
    Object? flag = null,
  }) {
    return _then(_$AxelImpl(
      null == character
          ? _value.character
          : character // ignore: cast_nullable_to_non_nullable
              as String,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as ColorPlaceholder,
      background: null == background
          ? _value.background
          : background // ignore: cast_nullable_to_non_nullable
              as ColorPlaceholder,
      textStyle: null == textStyle
          ? _value.textStyle
          : textStyle // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      flag: null == flag
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$AxelImpl extends _Axel {
  const _$AxelImpl(this.character,
      {this.color = Colors.foreground,
      this.background = Colors.background,
      this.textStyle = const TextStyle(),
      this.flag = 0})
      : super._();

  @override
  final String character;
  @override
  @JsonKey()
  final ColorPlaceholder color;
  @override
  @JsonKey()
  final ColorPlaceholder background;
  @override
  @JsonKey()
  final TextStyle textStyle;
  @override
  @JsonKey()
  final int flag;

  @override
  String toString() {
    return 'Axel(character: $character, color: $color, background: $background, textStyle: $textStyle, flag: $flag)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AxelImpl &&
            (identical(other.character, character) ||
                other.character == character) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.background, background) ||
                other.background == background) &&
            (identical(other.textStyle, textStyle) ||
                other.textStyle == textStyle) &&
            (identical(other.flag, flag) || other.flag == flag));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, character, color, background, textStyle, flag);

  /// Create a copy of Axel
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$AxelImplCopyWith<_$AxelImpl> get copyWith =>
      __$$AxelImplCopyWithImpl<_$AxelImpl>(this, _$identity);
}

abstract class _Axel extends Axel {
  const factory _Axel(final String character,
      {final ColorPlaceholder color,
      final ColorPlaceholder background,
      final TextStyle textStyle,
      final int flag}) = _$AxelImpl;
  const _Axel._() : super._();

  @override
  String get character;
  @override
  ColorPlaceholder get color;
  @override
  ColorPlaceholder get background;
  @override
  TextStyle get textStyle;
  @override
  int get flag;

  /// Create a copy of Axel
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$AxelImplCopyWith<_$AxelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
