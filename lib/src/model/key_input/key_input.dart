/// Represents key presses.
enum KeyInput {
  /// Ctrl+space
  controlSpace,

  /// Ctrl+a
  controlA,

  /// Ctrl+b
  controlB,

  /// Ctrl+c
  controlC,

  /// Ctrl+d
  controlD,

  /// Ctrl+e
  controlE,

  /// Ctrl+f
  controlF,

  /// Ctrl+g
  controlG,

  /// Ctrl+h
  controlH,

  /// Ctrl+l
  controlI,

  /// Tab
  tab,

  /// Ctrl+j
  controlJ,

  /// Ctrl+k
  controlK,

  /// Ctrl+l
  controlL,

  /// Ctrl+m
  controlM,

  /// Enter/return
  enter,

  /// Ctrl+n
  controlN,

  /// Ctrl+o
  controlO,

  /// Ctrl+p
  controlP,

  /// Ctrl+q
  controlQ,

  /// Ctrl+r
  controlR,

  /// Ctrl+s
  controlS,

  /// Ctrl+t
  controlT,

  /// Ctrl+u
  controlU,

  /// Ctrl+v
  controlV,

  /// Ctrl+w
  controlW,

  /// Ctrl+x
  controlX,

  /// Ctrl+y
  controlY,

  /// Ctrl+z
  controlZ,

  /// Esc
  escape,

  /// Ctrl+\
  controlBackslash,

  /// Ctrl+]
  controlRightSquareBracket,

  /// Ctrl+~
  controlTilde,

  /// Ctrl+/
  controlForwardSlash,

  /// Space
  space(' '),

  /// !
  exclamation('!'),

  /// "
  doubleQuote('"'),

  /// #
  numberSign('#'),

  /// $
  dollar('\$'),

  /// %
  percent('%'),

  /// &
  ampersand('&'),

  /// '
  apostrophe("'"),

  /// (
  leftParenthesis('('),

  /// )
  rightParenthesis(')'),

  /// *
  asterisk('*'),

  /// +
  plus('+'),

  /// ,
  comma(','),

  /// -
  minus('-'),

  /// .
  fullStop('.'),

  /// /
  forwardSlash('/'),

  /// 0
  number0('0'),

  /// 1
  number1('1'),

  /// 2
  number2('2'),

  /// 3
  number3('3'),

  /// 4
  number4('4'),

  /// 5
  number5('5'),

  /// 6
  number6('6'),

  /// 7
  number7('7'),

  /// 8
  number8('8'),

  /// 9
  number9('9'),

  /// :
  colon(':'),

  /// ;
  semicolon(';'),

  /// <
  lessThan('<'),

  /// =
  equals('='),

  /// >
  greaterThan('>'),

  /// ?
  questionMark('?'),

  /// @
  at('@'),

  /// A
  uppercaseA('A'),

  /// B
  uppercaseB('B'),

  /// C
  uppercaseC('C'),

  /// D
  uppercaseD('D'),

  /// E
  uppercaseE('E'),

  /// F
  uppercaseF('F'),

  /// G
  uppercaseG('G'),

  /// H
  uppercaseH('H'),

  /// I
  uppercaseI('I'),

  /// J
  uppercaseJ('J'),

  /// K
  uppercaseK('K'),

  /// L
  uppercaseL('L'),

  /// M
  uppercaseM('M'),

  /// N
  uppercaseN('N'),

  /// O
  uppercaseO('O'),

  /// P
  uppercaseP('P'),

  /// Q
  uppercaseQ('Q'),

  /// R
  uppercaseR('R'),

  /// S
  uppercaseS('S'),

  /// T
  uppercaseT('T'),

  /// U
  uppercaseU('U'),

  /// V
  uppercaseV('V'),

  /// W
  uppercaseW('W'),

  /// X
  uppercaseX('X'),

  /// Y
  uppercaseY('Y'),

  /// Z
  uppercaseZ('Z'),

  /// [
  leftSquareBracket('['),

  /// \
  backslash('\\'),

  /// ]
  rightSquareBracket(']'),

  /// ^
  circumflex('^'),

  /// _
  underscore('_'),

  /// `
  grave('`'),

  /// a
  lowercaseA('a'),

  /// b
  lowercaseB('b'),

  /// c
  lowercaseC('c'),

  /// d
  lowercaseD('d'),

  /// e
  lowercaseE('e'),

  /// f
  lowercaseF('f'),

  /// g
  lowercaseG('g'),

  /// h
  lowercaseH('h'),

  /// i
  lowercaseI('i'),

  /// j
  lowercaseJ('j'),

  /// k
  lowercaseK('k'),

  /// l
  lowercaseL('l'),

  /// m
  lowercaseM('m'),

  /// n
  lowercaseN('n'),

  /// o
  lowercaseO('o'),

  /// p
  lowercaseP('p'),

  /// q
  lowercaseQ('q'),

  /// r
  lowercaseR('r'),

  /// s
  lowercaseS('s'),

  /// t
  lowercaseT('t'),

  /// u
  lowercaseU('u'),

  /// v
  lowercaseV('v'),

  /// w
  lowercaseW('w'),

  /// x
  lowercaseX('x'),

  /// y
  lowercaseY('y'),

  /// z
  lowercaseZ('z'),

  /// {
  leftCurlyBracket('{'),

  /// |
  verticalLine('|'),

  /// }
  rightCurlyBracket('}'),

  /// ~
  tilde('~'),

  /// #
  pound('#'),

  /// ¬
  notSign('¬'),

  /// F1
  function1,

  /// F2
  function2,

  /// F3
  function3,

  /// F4
  function4,

  /// F5
  function5,

  /// F6
  function6,

  /// F7
  function7,

  /// F8
  function8,

  /// F9
  function9,

  /// F10
  function10,

  /// F11
  function11,

  /// F12
  function12,

  /// Backspace
  backspace,

  /// Up arrow
  arrowUp,

  /// Down arrow
  arrowDown,

  /// Right arrow
  arrowRight,

  /// Left arrow
  arrowLeft,

  /// End
  end,

  /// Home
  home,

  /// Shift+Tab
  shiftTab,

  /// Delete
  delete,

  /// Page up
  pageUp,

  /// Page down
  pageDown,

  /// Control+Up arrow
  controlArrowUp,

  /// Control+Down arrow
  controlArrowDown,

  /// Control+Right arrow
  controlArrowRight,

  /// Control+Left arrow
  controlArrowLeft,

  /// Control+End
  controlEnd,

  /// Control+Home
  controlHome,

  /// Control+Delete
  controlDelete,

  /// Alt + A
  altUppercaseA,

  /// Alt + B
  altUppercaseB,

  /// Alt + C
  altUppercaseC,

  /// Alt + D
  altUppercaseD,

  /// Alt + E
  altUppercaseE,

  /// Alt + F
  altUppercaseF,

  /// Alt + G
  altUppercaseG,

  /// Alt + H
  altUppercaseH,

  /// Alt + I
  altUppercaseI,

  /// Alt + J
  altUppercaseJ,

  /// Alt + K
  altUppercaseK,

  /// Alt + L
  altUppercaseL,

  /// Alt + M
  altUppercaseM,

  /// Alt + N
  altUppercaseN,

  /// Alt + O
  altUppercaseO,

  /// Alt + P
  altUppercaseP,

  /// Alt + Q
  altUppercaseQ,

  /// Alt + R
  altUppercaseR,

  /// Alt + S
  altUppercaseS,

  /// Alt + T
  altUppercaseT,

  /// Alt + U
  altUppercaseU,

  /// Alt + V
  altUppercaseV,

  /// Alt + W
  altUppercaseW,

  /// Alt + X
  altUppercaseX,

  /// Alt + Y
  altUppercaseY,

  /// Alt + Z
  altUppercaseZ,

  /// Alt + [
  altLeftSquareBracket,

  /// Alt + \
  altBackslash,

  /// Alt + ]
  altRightSquareBracket,

  /// Alt + ^
  altCircumflex,

  /// Alt + _
  altUnderscore,

  /// Alt + `
  altGrave,

  /// Alt + a
  altLowercaseA,

  /// Alt + b
  altLowercaseB,

  /// Alt + c
  altLowercaseC,

  /// Alt + d
  altLowercaseD,

  /// Alt + e
  altLowercaseE,

  /// Alt + f
  altLowercaseF,

  /// Alt + g
  altLowercaseG,

  /// Alt + h
  altLowercaseH,

  /// Alt + i
  altLowercaseI,

  /// Alt + j
  altLowercaseJ,

  /// Alt + k
  altLowercaseK,

  /// Alt + l
  altLowercaseL,

  /// Alt + m
  altLowercaseM,

  /// Alt + n
  altLowercaseN,

  /// Alt + o
  altLowercaseO,

  /// Alt + p
  altLowercaseP,

  /// Alt + q
  altLowercaseQ,

  /// Alt + r
  altLowercaseR,

  /// Alt + s
  altLowercaseS,

  /// Alt + t
  altLowercaseT,

  /// Alt + u
  altLowercaseU,

  /// Alt + v
  altLowercaseV,

  /// Alt + w
  altLowercaseW,

  /// Alt + x
  altLowercaseX,

  /// Alt + y
  altLowercaseY,

  /// Alt + z
  altLowercaseZ,

  /// Alt+Up arrow
  altArrowUp,

  /// Alt+Down arrow
  altArrowDown,

  /// Alt+Right arrow
  altArrowRight,

  /// Alt+Left arrow
  altArrowLeft,

  /// Unknown key.
  unknown;

  /// Represents key presses.
  const KeyInput([this.string = '']);

  /// Complete string that makes up this key.
  final String string;

  /// Whether this key has a string.
  bool get isPrintable => string.isNotEmpty;

  /// Whether this key is a recognised number key.
  bool get isNumber =>
      this == number0 ||
      this == number1 ||
      this == number2 ||
      this == number3 ||
      this == number4 ||
      this == number5 ||
      this == number6 ||
      this == number7 ||
      this == number8 ||
      this == number9;
}
