/// Class to represent directions.
enum Direction {
  /// Class to represent directions.
  up,

  /// Class to represent directions.
  down,

  /// Class to represent directions.
  left,

  /// Class to represent directions.
  right;

  /// Gets the opposite direction.
  Direction get opposite {
    switch (this) {
      case up:
        return down;
      case down:
        return up;
      case left:
        return right;
      case right:
        return left;
    }
  }
}
