/// Alignment for text.
enum TextAlignment {
  /// Left aligned.
  left,

  /// Centre aligned.
  center,

  /// Right aligned.
  right,
}
