import '../../components.dart';
import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Fixed-size divider for use in Flex layouts.
class Divider extends Renderable with InflexibleSized {
  /// Main axis of this item's parent.
  final Axis axis;

  /// Space padding around the divider.
  final int padding;

  /// End of the divider at the top for vertical, left for horizontal.
  final Axel proximalCap;

  /// End of the divider at the bottom for vertical, right for horizontal.
  final Axel distalCap;

  /// Fill for the body of the divider.
  final Axel fill;

  /// Fixed-size divider for use in Flex layouts.
  const Divider({
    required this.axis,
    this.padding = 0,
    required this.proximalCap,
    required this.distalCap,
    required this.fill,
  });

  @override
  int get size => 1 + 2 * padding;

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      axis == this.axis ? null : size;

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => const [];

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) => false;

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    if (axis == Axis.vertical) {
      if (height < 2) {
        return const Filled(fill: Axel.space).render(container, width, height);
      }
      return AxelMap(
        [proximalCap, ...fill * (height - 2), distalCap]
            .map((e) => [...Axel.space * padding, e, ...Axel.space * padding]),
      );
    } else {
      if (width < 2) {
        return const Filled(fill: Axel.space).render(container, width, height);
      }
      return AxelMap(
        [
          ...List.filled(padding, Axel.space * width),
          [proximalCap, ...fill * (width - 2), distalCap],
          ...List.filled(padding, Axel.space * width),
        ],
      );
    }
  }
}

/// Divider oriented horizontally. For use in vertical Flex layouts (e.g. Column).
class HorizontalDivider extends Divider {
  /// Divider oriented horizontally. For use in vertical Flex layouts (e.g. Column).
  const HorizontalDivider({super.padding})
      : super(
          axis: Axis.horizontal,
          // proximalCap: const Axel('╶', textStyle: TextStyle(faint: true)),
          proximalCap: const Axel('─', textStyle: TextStyle(faint: true)),
          fill: const Axel('─', textStyle: TextStyle(faint: true)),
          // distalCap: const Axel('╴', textStyle: TextStyle(faint: true)),
          distalCap: const Axel('─', textStyle: TextStyle(faint: true)),
        );

  /// Divider oriented horizontally. For use in vertical Flex layouts (e.g. Column).
  /// Made of border placeholder characters to be replaced by ConnectedBorder.
  const HorizontalDivider.borderPlaceholder({super.padding})
      : super(
          axis: Axis.horizontal,
          proximalCap: Axel.borderPlaceholder,
          fill: Axel.borderPlaceholder,
          distalCap: Axel.borderPlaceholder,
        );
}

/// Divider oriented vertically. For use in horizontal Flex layouts (e.g. Row).
class VerticalDivider extends Divider {
  /// Divider oriented vertically. For use in horizontal Flex layouts (e.g. Row).
  const VerticalDivider({super.padding})
      : super(
          axis: Axis.vertical,
          //proximalCap: const Axel('╷', textStyle: TextStyle(faint: true)),
          proximalCap: const Axel('│', textStyle: TextStyle(faint: true)),
          fill: const Axel('│', textStyle: TextStyle(faint: true)),
          //distalCap: const Axel('╵', textStyle: TextStyle(faint: true)),
          distalCap: const Axel('│', textStyle: TextStyle(faint: true)),
        );

  /// Divider oriented vertically. For use in horizontal Flex layouts (e.g. Row).
  /// Made of border placeholder characters to be replaced by ConnectedBorder.
  const VerticalDivider.borderPlaceholder({super.padding})
      : super(
          axis: Axis.vertical,
          proximalCap: Axel.borderPlaceholder,
          fill: Axel.borderPlaceholder,
          distalCap: Axel.borderPlaceholder,
        );
}
