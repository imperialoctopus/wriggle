import '../../component.dart';
import '../../context.dart';
import '../../model/key_input/key_input.dart';
import '../../renderable.dart';
import '../../route_contract.dart';
import 'dialog_component.dart';
import 'selector_form_item.dart';

/// Simple dialog wrapping SelectorFormItem to provider easy list picking.
class SelectorDialog<T> extends Component with RouteContract<T> {
  /// Title in the dialog border.
  final String title;

  /// Text displayed above the form item.
  final String? formLabel;

  /// Default state.
  final T startingValue;

  /// Items to display in the list. Must contain [startingValue].
  final Map<T, String> items;

  /// Simple dialog wrapping SelectorFormItem to provider easy list picking.
  const SelectorDialog({
    required this.title,
    this.formLabel,
    required this.startingValue,
    required this.items,
  });

  static const _formKey = 'dialog-select';

  @override
  Renderable build(ProviderContainer container) {
    return Dialog(
      title: title,
      child: SelectorFormItem(
        key: _formKey,
        focused: true,
        items: items,
        label: formLabel,
        startingValue: startingValue,
        dependency: Wriggle.dialog.state,
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (super.keyPressed(container, key)) {
      return true;
    }
    switch (key) {
      case KeyInput.escape:
        Wriggle.dialog.pop<void>(container);
        return true;
      case KeyInput.enter:
        final state =
            SelectorFormItem.read<T>(container, _formKey, startingValue);
        Wriggle.dialog.pop<T>(container, state);
        return true;
      default:
        return false;
    }
  }
}
