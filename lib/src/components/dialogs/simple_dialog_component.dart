import 'package:riverpod/riverpod.dart';

import '../../../components.dart';
import '../../context.dart';
import '../../model/key_input/key_input.dart';
import '../../route_contract.dart';

/// Simple no-return dialog for wrapping continuous-update form items.
class SimpleDialog extends Dialog with RouteContract<void> {
  /// Keys that close this dialog.
  final List<KeyInput> popOn;

  /// Simple no-return dialog for wrapping continuous-update form items.
  SimpleDialog({
    required super.child,
    super.width = 60,
    this.popOn = const [KeyInput.enter, KeyInput.escape],
  });

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (super.keyPressed(container, key)) {
      return true;
    }
    if (popOn.contains(key)) {
      Wriggle.dialog.pop<void>(container);
      return true;
    }
    return false;
  }
}
