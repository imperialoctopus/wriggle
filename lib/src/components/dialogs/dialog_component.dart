import '../../component.dart';
import '../../model/axel.dart';
import '../../model/axis.dart';
import '../../model/border_data.dart';
import '../../model/key_input/key_input.dart';
import '../../model/usage_info.dart';
import '../../renderable.dart';
import '../border_component.dart';
import '../center_component.dart';
import '../sized_component.dart';
import '../surface_component.dart';
import '../titled_border_component.dart';

/// Bordered floating popup. Intended to be pushed onto a DialogSurface.
class Dialog extends Component {
  /// Height of the dialog.
  final int? height;

  /// Width of the dialog.
  final int? width;

  /// Text to display within the titled border.
  final String title;

  /// Child to draw within the dialog.
  final Renderable child;

  /// Bordered floating popup. Intended to be pushed onto a DialogSurface.
  const Dialog({
    this.height,
    this.width = 60,
    this.title = '',
    required this.child,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) => null;

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  Renderable build(ProviderContainer container) {
    return Center(
      child: Surface(
        child: TitledBorder(
          title: title,
          borderFill: Axel.borderPlaceholder,
          child: Border(
            border: const BorderData(
              left: 1,
              right: 1,
              bottom: 1,
              fill: Axel.borderPlaceholder,
            ),
            child: Border(
              border: const BorderData.symmetric(horizontal: 1),
              child: Sized(
                //height: (height == null) ? null : height! - 2,
                //width: (width == null) ? null : width! - 4,
                height: height,
                width: width,
                child: child,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
