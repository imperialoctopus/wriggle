import '../../component.dart';
import '../../context.dart';
import '../../model/key_input/key_input.dart';
import '../../renderable.dart';
import '../../route_contract.dart';
import 'dialog_component.dart';
import 'text_form_item.dart';

/// Dialog to get a text string.
class TextDialog extends Component with RouteContract<String> {
  /// Text in the title border.
  final String title;

  /// Starting text.
  final String startingValue;

  /// Dialog to get a text string.
  const TextDialog({
    required this.title,
    required this.startingValue,
  });

  static const _formKey = 'dialog-text';

  @override
  Renderable build(ProviderContainer container) {
    return Dialog(
      title: title,
      width: 40,
      child: TextFormItem(
        key: _formKey,
        focused: true,
        dependency: Wriggle.dialog.state,
        startingValue: startingValue,
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (super.keyPressed(container, key)) {
      return true;
    }
    switch (key) {
      case KeyInput.escape:
        Wriggle.dialog.pop<void>(container);
        return true;
      case KeyInput.enter:
        final state = TextFormItem.read(container, _formKey, startingValue);
        Wriggle.dialog.pop<String>(container, state);
        return true;
      default:
        return false;
    }
  }
}
