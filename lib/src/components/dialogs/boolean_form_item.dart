import '../../../components.dart';
import '../../model/axel.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../stateful_component.dart';

/// Form item that allows checkbox boolean selection.
class BooleanFormItem extends StatefulComponent<bool?> {
  /// Focused.
  final bool focused;

  /// Label for this field.
  final String label;

  /// Starting state.
  final bool startingValue;

  /// State changed.
  final void Function(ProviderContainer container, bool newValue)? valueChanged;

  /// Form item that allows checkbox boolean selection.
  const BooleanFormItem({
    required super.key,
    required this.focused,
    super.dependency,
    required this.label,
    required this.startingValue,
    this.valueChanged,
  });

  @override
  Renderable build(ProviderContainer container) {
    return Invert(
      enable: focused,
      faint: false,
      child: Row(
        crossAxisTight: true,
        children: [
          Text('$label:', style: const TextStyle(faint: true)),
          const Spacer(),
          if (getState(container))
            const SingleAxel(Axel('●'))
          else
            const SingleAxel(Axel('○')),
          const InflexibleSpace(1),
        ],
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    switch (key) {
      case KeyInput.space:
        setState(container, (state) => !state);
        return true;
      case KeyInput.plus || KeyInput.arrowRight:
        setState(container, (_) => true);
        return true;
      case KeyInput.minus || KeyInput.arrowLeft:
        setState(container, (_) => false);
        return true;
      default:
        return false;
    }
  }

  @override
  bool getState(ProviderContainer container) {
    return super.getState(container) ?? startingValue;
  }

  @override
  void setState(ProviderContainer container, bool? Function(bool p1) update) {
    final value = update(getState(container));
    super.setState(container, (_) => value);
    valueChanged?.call(container, value ?? startingValue);
  }

  @override
  StateProvider<bool?> get stateProvider => _stateProvider(key);

  static final _stateProvider =
      StateProvider.family<bool?, String?>((_, __) => null);

  /// Read this component's internal state.
  static bool read(
    ProviderContainer container,
    String? key, [
    bool startingState = false,
  ]) {
    return container.read(_stateProvider(key)) ?? startingState;
  }
}
