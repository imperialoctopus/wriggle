import '../../../components.dart';
import '../../model/text_style.dart';
import '../../model/usage_info.dart';
import '../../renderable.dart';
import '../../stateful_component.dart';

/// Form item to get a text string.
class TextFormItem extends StatefulComponent<String?> {
  /// Focused.
  final bool focused;

  /// Label to show with this field.
  final String? label;

  /// Item to place before the text line.
  final Renderable? leading;

  /// Item to place after the text line.
  final Renderable? trailing;

  /// Text content.
  final String? startingValue;

  /// Style override for the content text.
  final TextStyle? contentStyle;

  /// Called when content should be changed.
  final void Function(ProviderContainer container, String newValue)?
      valueChanged;

  /// Form item to get a text string.
  const TextFormItem({
    required super.key,
    required this.focused,
    this.label,
    this.leading,
    this.trailing,
    this.startingValue,
    this.contentStyle,
    this.valueChanged,
    super.dependency,
  });

  @override
  Renderable build(ProviderContainer container) {
    return Column(
      alignment: FlexAlignment.tight,
      children: [
        if (label != null)
          SkipSelect(
            Text('$label:', style: const TextStyle(faint: true)),
          ),
        Row(
          crossAxisTight: true,
          children: [
            if (leading != null) SkipSelect(leading!),
            Flexible(
              child: TextField(
                key: '$key#form',
                dependency: dependency,
                focused: focused,
                content: getState(container) ?? startingValue?.toString() ?? '',
                contentChanged: (container, value) {
                  valueChanged?.call(container, value);
                  setState(container, (_) => value);
                },
                textStyle: contentStyle,
              ),
            ),
            if (trailing != null) SkipSelect(trailing!),
          ],
        ),
      ],
    );
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => [
        (key: 'Ctrl+K', name: 'Cut'),
        (key: 'Ctrl+Y', name: 'Copy'),
        (key: 'Ctrl+U', name: 'Paste'),
      ];

  @override
  StateProvider<String?> get stateProvider => _stateProvider(key);

  /// Public provider.
  static final _stateProvider =
      StateProvider.family<String?, String?>((_, __) => null);

  /// Read this component's internal state.
  static String read(
    ProviderContainer container,
    String? key, [
    String startingState = '',
  ]) {
    return container.read(_stateProvider(key)) ?? startingState;
  }
}
