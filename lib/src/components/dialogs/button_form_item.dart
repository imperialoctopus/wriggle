import '../../../components.dart';
import '../../component.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';

/// Form item that wraps KeyListener for submission or extra pages.
class ButtonFormItem extends Component {
  /// Focused.
  final bool focused;

  /// Label to display above button.
  final String? label;

  /// Content for the button
  final String content;

  /// Text alignment for the button's text.
  final TextAlignment contentAlignment;

  /// Whether to show leading arrow (implying a back button).
  final bool leadingArrow;

  /// Whether to show trailing arrow (implying a nested menu).
  final bool trailingArrow;

  /// Starting selection.
  final void Function(ProviderContainer container) onPressed;

  /// Form item that wraps KeyListener for submission or extra pages.
  const ButtonFormItem({
    required this.focused,
    this.label,
    required this.content,
    required this.onPressed,
    this.contentAlignment = TextAlignment.center,
    this.leadingArrow = false,
    this.trailingArrow = false,
  });

  @override
  Renderable build(ProviderContainer container) {
    return Column(
      alignment: FlexAlignment.tight,
      children: [
        if (label != null) Text('$label:', style: const TextStyle(faint: true)),
        Invert(
          enable: focused && !(leadingArrow || trailingArrow),
          child: Row(
            crossAxisTight: true,
            children: [
              if (leadingArrow)
                Text(' < ', style: TextStyle(inverted: focused)),
              Flexible(child: Text(content, textAlignment: contentAlignment)),
              if (trailingArrow)
                Text(' > ', style: TextStyle(inverted: focused)),
            ],
          ),
        ),
      ],
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    switch (key) {
      case KeyInput.enter:
      case KeyInput.space:
        onPressed(container);
        return true;
      default:
        return false;
    }
  }
}
