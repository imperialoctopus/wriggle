import '../../context.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../route_contract.dart';
import '../../stateful_component.dart';
import '../flex_component.dart';
import '../text_component.dart';
import 'dialog_component.dart';

/// Dialog displaying a message and buttons for "Confirm" and "Cancel".
class ConfirmDialog extends StatefulComponent<bool?> with RouteContract<bool> {
  /// Title of the dialog (within the border)
  final String title;

  /// Message to ask.
  final String message;

  /// Text for the cancel button.
  final String cancelText;

  /// Text for the confirm button.
  final String confirmText;

  /// "True" to start with the confirm button selected, cancel if false.
  final bool startingValue;

  /// Dialog displaying a message and buttons for "Confirm" and "Cancel".
  const ConfirmDialog({
    required this.title,
    required this.message,
    this.cancelText = "Cancel",
    this.confirmText = "Confirm",
    this.startingValue = false,
  });

  @override
  Renderable build(ProviderContainer container) {
    final state = getState(container);
    return Dialog(
      title: title,
      child: Column(
        alignment: FlexAlignment.tight,
        children: [
          Text(
            message,
            style: const TextStyle(faint: true),
            wrap: true,
          ),
          Row(
            alignment: FlexAlignment.center,
            crossAxisTight: true,
            children: [
              Text(
                ' $confirmText ',
                style: state ? const TextStyle(inverted: true) : null,
              ),
              const Text(' / '),
              Text(
                ' $cancelText ',
                style: !state ? const TextStyle(inverted: true) : null,
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    switch (key) {
      case KeyInput.escape:
        Wriggle.dialog.pop<void>(container);
        return true;
      case KeyInput.enter:
        Wriggle.dialog.pop<bool>(container, getState(container));
        return true;
      case KeyInput.arrowLeft:
        setState(container, (state) => true);
        return true;
      case KeyInput.arrowRight:
        setState(container, (state) => false);
        return true;
      default:
        return false;
    }
  }

  @override
  bool getState(ProviderContainer container) =>
      super.getState(container) ?? startingValue;

  @override
  StateProvider<bool?> get stateProvider => _stateProvider;

  static final _stateProvider = StateProvider<bool?>((ref) {
    ref.watch(Wriggle.dialog.state);
    return null;
  });
}
