import '../../component.dart';
import '../../context.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../route_contract.dart';
import '../flex_component.dart';
import '../text_component.dart';
import 'dialog_component.dart';

/// Dialog to show a message.
class MessageDialog extends Component with RouteContract<void> {
  /// Title within the border.
  final String title;

  /// Message to display.
  final String message;

  /// Dialog to show a message.
  const MessageDialog({
    required this.title,
    required this.message,
  });

  @override
  Renderable build(ProviderContainer container) {
    return Dialog(
      title: title,
      child: Row(
        alignment: FlexAlignment.center,
        crossAxisTight: true,
        children: [
          Text(message),
          const Text(' Close ', style: TextStyle(inverted: true)),
        ],
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    switch (key) {
      case KeyInput.escape:
      case KeyInput.enter:
        Wriggle.dialog.pop<void>(container);
        return true;
      default:
        return false;
    }
  }
}
