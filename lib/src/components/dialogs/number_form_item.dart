import '../../../components.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../stateful_component.dart';

/// Form item that allows only numbers
class NumberFormItem extends StatefulComponent<String?> {
  /// Focused.
  final bool focused;

  /// Label above the field.
  final String? label;

  /// Item to place before the text line.
  final Renderable? leading;

  /// Item to place after the text line.
  ///
  final Renderable? trailing;

  /// Content within the field.
  final int? startingValue;

  /// Update content.
  final void Function(ProviderContainer container, int? newValue)? valueChanged;

  /// Form item that allows only numbers
  const NumberFormItem({
    required super.key,
    required this.focused,
    this.label,
    this.leading,
    this.trailing,
    this.startingValue,
    this.valueChanged,
    super.dependency,
  });

  @override
  Renderable build(ProviderContainer container) {
    return Column(
      alignment: FlexAlignment.tight,
      children: [
        if (label != null)
          SkipSelect(
            Text('$label:', style: const TextStyle(faint: true)),
          ),
        Row(
          crossAxisTight: true,
          children: [
            if (leading != null) SkipSelect(leading!),
            Flexible(
              child: TextField(
                key: '$key#form',
                dependency: dependency,
                focused: focused,
                content: getState(container) ?? startingValue?.toString() ?? '',
                contentChanged: (container, value) {
                  valueChanged?.call(container, int.tryParse(value));
                  setState(container, (_) => value);
                },
              ),
            ),
            if (trailing != null) SkipSelect(trailing!),
          ],
        ),
      ],
    );
  }

  @override
  StateProvider<String?> get stateProvider => _stateProvider(key);

  static final _stateProvider =
      StateProvider.family<String?, String?>((_, __) => null);

  /// Read this component's internal state.
  static int read(
    ProviderContainer container,
    String? key, [
    int startingState = 0,
  ]) {
    final state = container.read(_stateProvider(key));
    if (state == null) {
      return startingState;
    } else if (state.isEmpty) {
      return 0;
    } else {
      return int.tryParse(state) ?? startingState;
    }
  }
}
