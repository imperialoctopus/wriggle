import 'dart:math';

import '../../../components.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../stateful_component.dart';

typedef _State<T> = ({T value});

/// Form item that allows selection from a list.
class SelectorFormItem<T> extends StatefulComponent<_State<Object?>?> {
  /// Focused.
  final bool focused;

  /// Label for this field.
  final String? label;

  /// Items to allow choice from.
  final Map<T, String> items;

  /// Starting selection.
  final T startingValue;

  /// Selected value changed.
  final void Function(ProviderContainer container, T newValue)? valueChanged;

  /// Form item that allows selection from a list.
  const SelectorFormItem({
    required super.key,
    required this.focused,
    this.label,
    required this.items,
    required this.startingValue,
    this.valueChanged,
    super.dependency,
  });

  @override
  Renderable build(ProviderContainer container) {
    assert(items.containsKey(startingValue));
    final state = _getState(container);
    final selected = state == null ? startingValue : state.value;

    return Column(
      alignment: FlexAlignment.tight,
      children: [
        if (label != null) Text('$label:', style: const TextStyle(faint: true)),
        Row(
          crossAxisTight: true,
          children: [
            Text(
              ' < ',
              style: TextStyle(
                inverted: focused,
                faint: selected == items.keys.first,
              ),
            ),
            Flexible(
              child: Text(
                items[selected] ?? '',
                textAlignment: TextAlignment.center,
              ),
            ),
            Text(
              ' > ',
              style: TextStyle(
                inverted: focused,
                faint: selected == items.keys.last,
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (key == KeyInput.arrowLeft) {
      final keys = items.keys.toList();
      _setState(
        container,
        (selected) => keys[max(0, keys.indexOf(selected) - 1)],
      );
      return true;
    }
    if (key == KeyInput.arrowRight) {
      final keys = items.keys.toList();
      _setState(
        container,
        (selected) => keys[min(keys.length - 1, keys.indexOf(selected) + 1)],
      );
      return true;
    }
    return false;
  }

  _State<T>? _getState(ProviderContainer container) {
    final state = super.getState(container);
    if (state is _State<T>) {
      return state;
    } else {
      return null;
    }
  }

  void _setState(
    ProviderContainer container,
    T Function(T p1) update,
  ) {
    final state = _getState(container);
    final value = state == null ? startingValue : state.value;
    final newValue = update(value);
    super.setState(container, (_) => (value: newValue));
    valueChanged?.call(container, newValue);
  }

  @override
  StateProvider<_State<Object?>?> get stateProvider => _stateProvider(key);

  static final _stateProvider =
      StateProvider.family<_State<Object?>?, String?>((_, __) {
    return null;
  });

  /// Read this component's internal state.
  static T read<T>(ProviderContainer container, String? key, T startingState) {
    final state = container.read(_stateProvider(key));
    if (state is _State<T>) {
      return state.value;
    } else {
      return startingState;
    }
  }
}
