import '../../component.dart';
import '../../context.dart';
import '../../model/key_input/key_input.dart';
import '../../renderable.dart';
import '../../route_contract.dart';
import 'dialog_component.dart';
import 'number_form_item.dart';

/// Dialog to type a number.
class NumberDialog extends Component with RouteContract<int> {
  /// Title within the border.
  final String title;

  /// Starting value.
  final int startingValue;

  /// Dialog to type a number.
  const NumberDialog({required this.title, required this.startingValue});

  static const _formKey = 'number-dialog';

  @override
  Renderable build(ProviderContainer container) {
    return Dialog(
      title: title,
      //height: 1,
      width: 20,
      child: NumberFormItem(
        key: _formKey,
        dependency: Wriggle.dialog.state,
        focused: true,
        startingValue: startingValue,
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (super.keyPressed(container, key)) {
      return true;
    }
    switch (key) {
      case KeyInput.escape:
        Wriggle.dialog.pop<void>(container);
        return true;
      case KeyInput.enter:
        final state = NumberFormItem.read(container, _formKey, startingValue);
        Wriggle.dialog.pop<int>(container, state);
        return true;
      default:
        return false;
    }
  }
}
