import '../../component.dart';
import '../../context.dart';
import '../../model/border_data.dart';
import '../../model/key_input/key_input.dart';
import '../../renderable.dart';
import '../../route_contract.dart';
import '../border_component.dart';
import '../file_browser_component.dart';
import 'dialog_component.dart';

/// Dialog to get a file using file browser.
class FileBrowserDialog extends Component with RouteContract<String> {
  /// Text in the title border.
  final String title;

  /// Starting path.
  final String startingValue;

  /// If true allows folders to be selected instead of files.
  final bool folderMode;

  /// Dialog to get a file using file browser.
  const FileBrowserDialog({
    required this.title,
    this.startingValue = '',
    this.folderMode = false,
  });

  static const _formKey = 'dialog-file';

  @override
  Renderable build(ProviderContainer container) {
    return Border(
      border: const BorderData.symmetric(vertical: 2),
      child: Dialog(
        title: title,
        width: 80,
        child: FileBrowser(
          key: _formKey,
          dependency: Wriggle.dialog.state,
          pathSelected: Wriggle.dialog.pop<String>,
          folderMode: folderMode,
          startingPath: startingValue,
        ),
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (super.keyPressed(container, key)) {
      return true;
    }
    switch (key) {
      case KeyInput.escape:
        Wriggle.dialog.pop<void>(container);
        return true;
      default:
        return false;
    }
  }
}
