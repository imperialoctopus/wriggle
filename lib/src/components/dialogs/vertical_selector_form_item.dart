import '../../../components.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../stateful_component.dart';

/// Form item that allows selection from a vertical list.
class VerticalSelectorFormItem<T extends Object>
    extends StatefulComponent<Object?> {
  /// Focused.
  final bool focused;

  /// Label for this field.
  final String? label;

  /// Items to allow choice from.
  final Map<T, String> items;

  /// Starting value.
  final T startingValue;

  /// Selected value changed.
  final void Function(ProviderContainer container, T newValue)? valueChanged;

  /// Form item that allows selection from a vertical list.
  const VerticalSelectorFormItem({
    required super.key,
    required this.focused,
    this.label,
    required this.items,
    required this.startingValue,
    this.valueChanged,
    super.dependency,
  });

  @override
  Renderable build(ProviderContainer container) {
    final state = _getState(container);
    assert(items.containsKey(state));

    return Column(
      alignment: FlexAlignment.tight,
      children: [
        if (label != null) Text('$label:', style: const TextStyle(faint: true)),
        Column(
          alignment: FlexAlignment.tight,
          children: [
            for (final entry in items.entries)
              Invert(
                enable: entry.key == state,
                child: Text(entry.value),
              ),
          ],
        ),
      ],
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    switch (key) {
      case KeyInput.arrowUp:
        _setState(container, (state) {
          final keys = items.keys.toList();
          final newIndex = keys.indexOf(state) - 1;
          if (newIndex < 0) {
            return state;
          }
          return keys[newIndex];
        });
        return true;
      case KeyInput.arrowDown:
        _setState(container, (state) {
          final keys = items.keys.toList();
          final newIndex = keys.indexOf(state) + 1;
          if (newIndex >= keys.length) {
            return state;
          }
          return keys[newIndex];
        });
        return true;
      default:
        return false;
    }
  }

  T _getState(ProviderContainer container) {
    final state = super.getState(container);
    if (state is T) {
      return state;
    } else {
      return startingValue;
    }
  }

  void _setState(
    ProviderContainer container,
    T Function(T p1) update,
  ) {
    final state = _getState(container);
    final newValue = update(state);
    super.setState(container, (_) => newValue);
    valueChanged?.call(container, newValue);
  }

  @override
  StateProvider<Object?> get stateProvider => _stateProvider(key);

  static final _stateProvider =
      StateProvider.family<Object?, String?>((_, __) => null);

  /// Read this component's internal state.
  static T read<T>(ProviderContainer container, String? key, T startingValue) {
    final state = container.read(_stateProvider(key));
    if (state is T) {
      return state;
    } else {
      return startingValue;
    }
  }
}
