import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Unsized component filled with [fill].
class Filled extends Renderable {
  /// Axel to fill with.
  final Axel fill;

  /// Unsized component filled with [fill].
  const Filled({required this.fill});

  /// Unsized component filled with spaces.
  const Filled.space() : fill = Axel.space;

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) => null;

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) => false;

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => [];

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final row = fill * width;
    return AxelMap(List.filled(height, row));
  }
}
