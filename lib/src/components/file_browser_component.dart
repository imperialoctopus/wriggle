import 'dart:async';
import 'dart:io';

import '../component.dart';
import '../context.dart';
import '../model/key_input/key_input.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import '../stateful_component.dart';
import 'center_component.dart';
import 'dialogs/text_dialog.dart';
import 'flex_component.dart';
import 'flexible.dart';
import 'inflexible_space.dart';
import 'invert_component.dart';
import 'scrolling_selector_component.dart';
import 'selector_flex_component.dart';
import 'text_component.dart';

export '../exceptions/filesystem_exceptions.dart';

typedef _FBState = ({String? path, bool thisDirectorySelected});

/// Component to pick a file or folder from the system.
class FileBrowser extends StatefulComponent<_FBState> {
  /// Path of directory to start in.
  final String startingPath;

  /// Whether to pick folders.
  final bool folderMode;

  /// Show header with current directory path.
  final bool pathHeader;

  /// Callback when file/folder is selected.
  final void Function(ProviderContainer container, String path) pathSelected;

  /// Component to pick a file or folder from the system.
  const FileBrowser({
    super.key,
    required this.pathSelected,
    this.folderMode = false,
    this.startingPath = '',
    this.pathHeader = true,
    super.dependency,
  });

  @override
  Renderable build(ProviderContainer container) {
    final notifier = container.read(
      _directoryProvider((key: key, startingPath: startingPath)).notifier,
    );
    dependency?.addListener(
      container,
      (_, __) => container.invalidate(
        _directoryProvider((key: key, startingPath: startingPath)),
      ),
      onError: (_, __) {},
      fireImmediately: false,
      onDependencyMayHaveChanged: () {},
    );
    final path = notifier.path;
    final state = getState(container);
    return Column(
      children: [
        if (pathHeader)
          SkipSelect(Text(' > $path', style: TextStyles.faint, wrap: true)),
        Flexible(
          child: container
              .read(_directoryProvider((key: key, startingPath: startingPath)))
              .when(
                error: (e, __) => Center(child: Text('Error: $e', wrap: true)),
                loading: () => const Center(child: Text('Loading')),
                data: (children) {
                  int? index;
                  if (state.path != null) {
                    index = children.indexWhere((e) => e.path == state.path);
                    if (index == -1) {
                      index = null;
                    } else {
                      index += folderMode ? 2 : 1;
                    }
                  }
                  if (index == null) {
                    if (folderMode) {
                      index = state.thisDirectorySelected ? 1 : 0;
                    } else {
                      index = 0;
                    }
                  }

                  return ScrollingSelector(
                    key: 'filebrowser-$key-col',
                    focused: true,
                    selected: index,
                    selectedChanged: (container, newSelected) {
                      if (folderMode) {
                        if (newSelected < 2) {
                          setState(
                            container,
                            (_) => (
                              path: null,
                              thisDirectorySelected: newSelected != 0,
                            ),
                          );
                          return;
                        }
                        setState(
                          container,
                          (_) => (
                            path:
                                children.elementAtOrNull(newSelected - 2)?.path,
                            thisDirectorySelected: false,
                          ),
                        );
                        return;
                      }
                      if (newSelected == 0) {
                        setState(
                          container,
                          (_) => (path: null, thisDirectorySelected: false),
                        );
                        return;
                      }
                      setState(
                        container,
                        (_) => (
                          path: children.elementAtOrNull(newSelected - 1)?.path,
                          thisDirectorySelected: false,
                        ),
                      );
                      return;
                    },
                    children: [
                      (focused) => _FileBrowserStaticEntry(
                        focused: focused,
                        title: const Row(
                          crossAxisTight: true,
                          children: [
                            Text('..'),
                            Text(
                              ' (parent directory)',
                              style: TextStyles.faint,
                            ),
                          ],
                        ),
                        onPress: (container) {
                          final notifier = container.read(
                            _directoryProvider((
                              key: key,
                              startingPath: startingPath,
                            )).notifier,
                          );
                          notifier.upOneLevel();
                        },
                        usageInfo: const [(key: 'Enter', name: 'Navigate')],
                      ),
                      if (folderMode)
                        (focused) => _FileBrowserStaticEntry(
                          focused: focused,
                          title: const Row(
                            crossAxisTight: true,
                            children: [
                              Text('. '),
                              Text(
                                ' (choose this directory)',
                                style: TextStyles.faint,
                              ),
                            ],
                          ),
                          onPress: (container) {
                            pathSelected(container, path);
                          },
                          usageInfo: const [(key: 'Enter', name: 'Choose')],
                        ),
                      for (final child in children)
                        (focused) => _FileBrowserEntry(
                          focused: focused,
                          path: child.path,
                          directory: child.directory,
                          folderMode: folderMode,
                          entrySelected: (container, path, directory) {
                            if (directory) {
                              notifier.path = path;
                            } else if (!folderMode) {
                              pathSelected(container, path);
                            }
                          },
                        ),
                    ],
                  );
                },
              ),
        ),
      ],
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (super.keyPressed(container, key)) {
      return true;
    }

    switch (key) {
      case KeyInput.altArrowUp:
        final notifier = container.read(
          _directoryProvider((
            key: this.key,
            startingPath: startingPath,
          )).notifier,
        );
        notifier.upOneLevel();
        return true;
      case KeyInput.controlE:
        Wriggle.dialog.push(
          container,
          TextDialog(
            title: 'Path Edit',
            startingValue:
                container
                    .read(
                      _directoryProvider((
                        key: this.key,
                        startingPath: startingPath,
                      )).notifier,
                    )
                    .path,
          ),
          (container, result) =>
              container
                  .read(
                    _directoryProvider((
                      key: this.key,
                      startingPath: startingPath,
                    )).notifier,
                  )
                  .path = result,
        );
        return true;
      default:
        return false;
    }
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      [(key: 'Ctrl+E', name: 'Edit Path'), (key: 'Alt+Up', name: 'Parent')] +
      super.getUsageInfo(container);

  /// Read path of current directory.
  static String read(
    ProviderContainer container,
    String key, [
    String startingValue = '',
  ]) =>
      container
          .read(
            _directoryProvider((
              key: key,
              startingPath: startingValue,
            )).notifier,
          )
          .path;

  static final _selectedItemPathProvider =
      StateProviderFamily<_FBState, String?>(
        (_, __) => (path: null, thisDirectorySelected: false),
      );

  @override
  StateProvider<_FBState> get stateProvider => _selectedItemPathProvider(key);
}

class _FileBrowserEntry extends Component {
  final bool focused;
  final String path;
  final bool directory;
  final bool folderMode;
  final void Function(ProviderContainer container, String path, bool directory)
  entrySelected;

  const _FileBrowserEntry({
    required this.focused,
    required this.path,
    required this.directory,
    required this.folderMode,
    required this.entrySelected,
  });

  @override
  Renderable build(ProviderContainer container) {
    return Invert(
      enable: focused,
      child: Row(
        crossAxisTight: true,
        children: [
          const InflexibleSpace(1),
          if (directory) const Text('> ') else const InflexibleSpace(2),
          Text(Uri.file(path).pathSegments.lastOrNull ?? ''),
          const InflexibleSpace(1),
        ],
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    switch (key) {
      case KeyInput.enter:
        entrySelected(container, path, directory);
        return true;
      default:
        return false;
    }
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => [
    if (directory)
      (key: 'Enter', name: 'Navigate')
    else if (!folderMode)
      (key: 'Enter', name: 'Choose'),
  ];
}

class _FileBrowserStaticEntry extends Component {
  final bool focused;
  final Renderable title;
  final UsageInfo usageInfo;
  final void Function(ProviderContainer container) onPress;

  const _FileBrowserStaticEntry({
    required this.focused,
    required this.title,
    required this.usageInfo,
    required this.onPress,
  });

  @override
  Renderable build(ProviderContainer container) {
    return Invert(
      enable: focused,
      faint: false,
      child: Row(
        crossAxisTight: true,
        children: [const InflexibleSpace(1), title, const InflexibleSpace(1)],
      ),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    switch (key) {
      case KeyInput.enter:
        onPress(container);
        return true;
      default:
        return false;
    }
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => usageInfo;
}

typedef _DirectoryNotifierKey =
    ({
      String? key,
      String startingPath,
      //ProviderListenable<Object?>? dependency
    });
typedef _DirectoryReference = ({String path, bool directory});

final _directoryProvider = AsyncNotifierProviderFamily<
  _DirectoryNotifier,
  List<_DirectoryReference>,
  _DirectoryNotifierKey
>(() => _DirectoryNotifier());

class _DirectoryNotifier
    extends
        FamilyAsyncNotifier<List<_DirectoryReference>, _DirectoryNotifierKey> {
  String? _path;

  String get path => _path ?? arg.startingPath;
  set path(String value) {
    _path = value;
    _regenerate();
  }

  @override
  FutureOr<List<_DirectoryReference>> build(_DirectoryNotifierKey arg) async {
    final file = Directory(path);
    if (await file.exists()) {
      _path = file.absolute.path;
      return _getFiles(file);
    }
    final asFile = File(path);
    if (await asFile.exists()) {
      _path = asFile.parent.absolute.path;
      return _getFiles(asFile.parent);
    }
    _path = Directory.current.absolute.path;
    return _getFiles(Directory.current);
    // final path = workingDirect;
    // final parent = File(path).parent;
    // if (await parent.exists()) {
    //   return _getFiles(parent);
    // } else {
    //   throw FileNotFound('No such directory: $path.');
    // }
  }

  Future<void> _regenerate() async {
    state = const AsyncLoading();
    state = await AsyncValue.guard(() async {
      final file = Directory(path);
      if (await file.exists()) {
        return _getFiles(file);
      }
      final asFile = File(path);
      if (await asFile.exists()) {
        return _getFiles(asFile.parent);
      }
      path = Directory.current.absolute.path;
      return _getFiles(Directory.current);
    });
  }

  Future<List<({bool directory, String path})>> _getFiles(
    Directory file,
  ) async {
    final list =
        await file
            .list()
            .map((e) => (path: e.absolute.path, directory: e is Directory))
            .toList();
    list.sort((a, b) {
      final c = (b.directory ? 1 : 0) - (a.directory ? 1 : 0);
      if (c != 0) {
        return c;
      }
      return a.path.toLowerCase().compareTo(b.path.toLowerCase());
    });
    return list;
  }

  void upOneLevel() {
    final previous = path;
    path = File(path).absolute.parent.path;
    ref.read(FileBrowser._selectedItemPathProvider(arg.key).notifier).state = (
      path: previous,
      thisDirectorySelected: false,
    );
  }
}
