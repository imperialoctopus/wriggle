import 'dart:math';

import 'package:collection/collection.dart';

import '../../components.dart';
import '../context.dart';
import '../extensions/pad_axels.dart';
import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import '../stateful_component.dart';

typedef _State = ({int? cursorPos, int width});

/// Form field for a text string. State of the content of this item should be handled above this component.
class TextField extends StatefulComponent<_State> {
  /// Current content.
  final String content;

  /// Whether this field is focused.
  final bool focused;

  /// Style for content.
  final TextStyle? textStyle;

  /// Default cursor to the end of the content.
  final bool startCursorAtEnd;

  /// Update content function.
  final void Function(
    ProviderContainer container,
    String newContent,
  ) contentChanged;

  /// Form field for a text string. State of the content of this item should be handled above this component.
  const TextField({
    required super.key,
    super.dependency,
    required this.focused,
    required this.content,
    required this.contentChanged,
    this.startCursorAtEnd = true,
    this.textStyle,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    final width = container.read(stateProvider).width;
    switch (axis) {
      case Axis.vertical:
        if (content.isEmpty || width == 0) {
          return 1;
        }
        return Text.wrapText(content, width, null).length;
      case Axis.horizontal:
        return null;
    }
  }

  ({int row, int col})? _cursorCoords(ProviderContainer container) {
    final state = getState(container);
    final width = state.width;
    final cursor = state.cursorPos ?? (startCursorAtEnd ? content.length : 0);

    if (content.isEmpty) {
      return (row: 0, col: 0);
    }
    final lines = Text.wrapText(content, width, null);
    //int runningTotal = 0;

    int cursorRemaining = cursor;
    for (int i = 0; i < lines.length; i++) {
      final length = lines[i].length;
      if (cursorRemaining < length) {
        if (cursorRemaining > 75) throw Error();
        return (row: i, col: cursorRemaining);
      }
      // if (length == width - marginSize) {
      //   cursorRemaining++;
      // }
      cursorRemaining -= length;
    }
    return (row: lines.length - 1, col: min(width - 1, lines.last.length));

    // if (content.isEmpty) {
    //   return (row: 0, col: 0);
    // }
    // final lines = Text.wrapText(content, width, null);
    // int runningTotal = 0;

    // for (int i = 0; i < lines.length; i++) {
    //   if (runningTotal + lines[i].length > cursor) {
    //     return (row: i, col: cursor - runningTotal);
    //   }
    //   runningTotal += lines[i].length;
    // }
    // return (row: lines.length - 1, col: lines.last.length - 1);

    // return (
    //   row: (cursor / (width - marginSize)).floor(),
    //   col: cursor % (width - marginSize),
    // );
  }

  int _cursorCoordsToIndex(ProviderContainer container, int row, int col) {
    final state = container.read(stateProvider);
    final width = state.width;

    if (row == 0) {
      return col;
    }
    if (content.isEmpty) {
      return 0;
    }
    return Text.wrapText(content, width, null)
            .take(row)
            .map((e) => e.length)
            .reduce((a, b) => a + b) +
        col;
    //return lines.take(row).map((s) => s.length).reduce((a, b) => a + b) + col;
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    final state = container.read(stateProvider);
    final width = state.width;
    final cursorPos =
        state.cursorPos ?? (startCursorAtEnd ? content.length : 0);

    final lines = Text.wrapText(content, width, null);

    switch (key) {
      case KeyInput.arrowUp:
        final coords = _cursorCoords(container);
        if (coords == null) {
          return false;
        }
        if (coords.row == 0) {
          return false;
        }

        _setState(
          container,
          cursorPos:
              _cursorCoordsToIndex(container, coords.row - 1, coords.col),
        );
        return true;

      case KeyInput.arrowDown:
        final coords = _cursorCoords(container);
        if (coords == null) {
          return false;
        }
        if (coords.row + 1 == lines.length) {
          return false;
        }
        _setState(
          container,
          cursorPos:
              _cursorCoordsToIndex(container, coords.row + 1, coords.col),
        );
        return true;

      case KeyInput.arrowLeft:
        final coords = _cursorCoords(container);
        if (coords == null) {
          return false;
        }
        final newCursor =
            _cursorCoordsToIndex(container, coords.row, coords.col - 1);
        if (newCursor < 0) {
          return false;
        }
        _setState(container, cursorPos: newCursor);
        return true;

      case KeyInput.arrowRight:
        final coords = _cursorCoords(container);
        if (coords == null) {
          return false;
        }
        final newCursor =
            _cursorCoordsToIndex(container, coords.row, coords.col + 1);
        if (newCursor > content.length) {
          return false;
        }
        _setState(container, cursorPos: newCursor);
        return true;

      case KeyInput.home:
        _setState(container, cursorPos: 0);
        return true;

      case KeyInput.end:
        _setState(container, cursorPos: content.length);
        return true;

      case KeyInput.tab:
        _insertCharacters(container, '    ');
        return true;

      case KeyInput.backspace:
        if (cursorPos > content.length) {
          _deleteCharacter(container, content.length - 1);
          return true;
        }
        _deleteCharacter(container, cursorPos - 1);
        return true;

      case KeyInput.delete:
        _deleteCharacter(container, cursorPos);
        return true;

      case KeyInput.controlDelete:
        _setState(container, cursorPos: 0);
        contentChanged(container, '');
        return true;

      case KeyInput.controlK:
        Wriggle.clipboard.write(content).then((value) {
          if (value) {
            _setState(container, cursorPos: 0);
            contentChanged(container, '');
          }
        });
        return true;
      case KeyInput.controlY:
        Wriggle.clipboard.write(content);
        return true;
      case KeyInput.controlU:
        Wriggle.clipboard.read().then(
              (value) => _insertCharacters(container, value.removeLineBreaks()),
            );
        return true;
      default:
    }

    if (key.isPrintable) {
      _insertCharacters(container, key.string);
      return true;
    }

    return false;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => [
        (key: 'Ctrl+K', name: 'Cut'),
        (key: 'Ctrl+Y', name: 'Copy'),
        (key: 'Ctrl+U', name: 'Paste'),
      ];

  void _insertCharacters(ProviderContainer container, String chars) {
    final cursorPos = (container.read(stateProvider).cursorPos ??
            (startCursorAtEnd ? content.length : 0))
        .clamp(0, content.length);

    final prefix = content.substring(0, cursorPos);
    final suffix = content.substring(cursorPos);
    contentChanged(container, "$prefix$chars$suffix");
    _setState(container, cursorPos: cursorPos + chars.length);
  }

  void _deleteCharacter(ProviderContainer container, int position) {
    if (content.isEmpty) {
      return;
    }
    if (position < 0 || position >= content.length) {
      return;
    }

    final prefix = content.substring(0, position);
    final suffix = content.substring(position + 1);

    contentChanged(container, prefix + suffix);
    _setState(container, cursorPos: position);
  }

  void _setState(ProviderContainer container, {int? width, int? cursorPos}) =>
      setState(
        container,
        (current) => (
          cursorPos: cursorPos ?? current.cursorPos,
          width: width ?? current.width
        ),
      );

  @override
  Renderable build(ProviderContainer container) {
    return Text(content);
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final state = getState(container);

    if (state.width != width) {
      _setState(
        container,
        width: width,
        cursorPos: state.cursorPos,
      );
      return const Filled(fill: Axel.space).render(container, width, height);
    }
    // if (text.isEmpty) {
    //   return AxelMap.empty(width, height);
    // }

    final preparedText =
        content.isEmpty ? [' '] : Text.wrapText(content, width, null);

    // if (offset > 0) {
    //   return AxelMap.filled(
    //       width, height, Axel(preparedText.length.toString()));
    // }

    final axelMap = AxelMap(
      preparedText.mapIndexed(
        (i, row) => row
            .split('')
            .take(width)
            .map(
              (c) => Axel(
                c,
                //color: color,
                //background: background,
                //textStyle: style ?? const TextStyle(),
                textStyle: textStyle ?? const TextStyle(),
              ),
            )
            .pad(width),
      ),
    );
    axelMap.resize(
      width,
      height,
      //verticalStart: (_cursorCoords(container)?.row ?? 0) < height,
      verticalStart: false,
    );

    final cursor = _cursorCoords(container);
    if (focused && cursor != null) {
      //axelMap.editAxel(0, 0, highlight);
      //axelMap.editAxel(0, 1, highlight);
      //axelMap.editAxel(0, 2, highlight);
      //final mapWidth = axelMap.width;

      axelMap.editAxel(
        cursor.row,
        cursor.col,
        (axel) => axel.copyWith(
          textStyle: textStyle?.copyWith(inverted: true) ??
              const TextStyle(inverted: true),
        ),
      );
    }

    // axelMap.editAxel(0, 0, (axel) => axel.copyWith(background: Color.black));
    // axelMap.editAxel(0, 1, (axel) => axel.copyWith(background: Color.black));
    // axelMap.editAxel(0, 2, (axel) => axel.copyWith(background: Color.black));

    return axelMap;
  }

  /// State provider for this field.
  @override
  StateProvider<_State> get stateProvider => _stateProvider(key);

  static final _stateProvider = StateProvider.family<_State, String?>(
    (ref, arg) => (cursorPos: null, width: 0),
  );
}
