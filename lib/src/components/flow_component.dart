import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import 'flex_component.dart';

/// Lays out children in multiple rows/columns, flowing them over lines rather than resizing them if they would exceed this component's main axis size.
class Flow extends Renderable {
  /// Children to lay out.
  final List<Renderable> children;

  /// Fixed cross axis size of children.
  final int itemCrossSize;

  /// Direction to lay out children. Wraps around to cross axis.
  final Axis direction;

  /// Alignment for rows.
  final FlexAlignment mainAxisAlignment;

  /// Alignment for children within rows.
  final FlexAlignment crossAxisAlignment;

  /// Lays out children in multiple rows/columns, flowing them over lines rather than resizing them if they would exceed this component's main axis size.
  const Flow({
    required this.children,
    required this.itemCrossSize,
    this.direction = Axis.horizontal,
    this.mainAxisAlignment = FlexAlignment.start,
    this.crossAxisAlignment = FlexAlignment.start,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    if (axis == direction) {
      return null;
    } else {
      return _groupChildren(container, crossSize).length * itemCrossSize;
    }
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) => false;

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => const [];

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final rowSize = switch (direction) {
      Axis.vertical => height,
      Axis.horizontal => width,
    };

    final groupedChildren = _groupChildren(container, rowSize);

    return FlexComponent(
      alignment: crossAxisAlignment,
      axis: direction.cross,
      children: groupedChildren
          .map(
            (row) => FlexComponent(
              children: row,
              alignment: mainAxisAlignment,
              axis: direction,
            ),
          )
          .toList(),
    ).render(container, width, height);
  }

  List<List<Renderable>> _groupChildren(
    ProviderContainer container,
    int rowSize,
  ) {
    final groupedChildren = <List<Renderable>>[];
    List<Renderable> currentRow = <Renderable>[];
    int runningTotal = 0;

    for (int i = 0; i < children.length; i++) {
      final child = children[i];
      final size = child.getSize(container, direction, itemCrossSize) ?? 0;
      if (runningTotal + size > rowSize) {
        if (currentRow.isNotEmpty) {
          groupedChildren.add(currentRow.toList());
        }
        currentRow = [child];
        runningTotal = size;
      } else {
        currentRow.add(child);
        runningTotal += size;
      }
    }
    if (currentRow.isNotEmpty) {
      groupedChildren.add(currentRow);
    }
    return groupedChildren;
  }
}
