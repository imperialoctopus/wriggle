import 'dart:math';

import 'package:collection/collection.dart';

import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/border_data.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import 'border_component.dart';
import 'flexible.dart';
import 'inflexible.dart';
import 'selector_flex_component.dart';
import 'spacer_component.dart';

/// Alignment method in the main axis for FlexComponent (Row and Column).
enum FlexAlignment {
  /// Cluster children at the start of the container, leaving empty space at the end.
  start,

  /// Cluster children at the end of the container, leaving empty space at the start.
  end,

  /// Cluster children in the centre of the container, splitting empty space at either end.
  center,

  /// Space children evenly, including a share of space at either end of the container.
  spaceAround,

  /// Space children evenly without any extra space at the start or end of the container.
  spaceBetween,

  /// Reports the sum of children's main axis sizes as this component's size, leaving no free space.
  /// This fails if any children are unsized.
  tight,
}

/// Alignment method in the cross axis for FlexComponent (Row and Column).
enum CrossAxisAlignment {
  /// Hang children at the start (left for vertical main axis, top for horizontal main axis).
  start,

  /// Hang children at the end (right for vertical main axis, bottom for horizontal main axis).
  end,

  /// Centre children in the cross axis.
  center,

  /// Force children's cross axis sizes to match this component's.
  stretch,
}

/// Flexible layout component. Base for the Row and Column.
class FlexComponent extends Renderable {
  /// List of children to align.
  final List<Renderable> children;

  /// Alignment method for main axis.
  final FlexAlignment alignment;

  /// Alignment method in the cross axis.
  final CrossAxisAlignment crossAxisAlignment;

  /// Tightens size to the largest child cross axis size.
  final bool crossAxisTight;

  /// Main axis of this layout.
  final Axis axis;

  /// Pass key input events and usage info requests to child with this index.
  final int? passthrough;

  /// Flexible layout component. Base for the Row and Column.
  const FlexComponent({
    required this.children,
    this.alignment = FlexAlignment.start,
    this.crossAxisAlignment = CrossAxisAlignment.stretch,
    this.crossAxisTight = false,
    required this.axis,
    this.passthrough,
  });

  /// Children which can be selected (i.e. not wrapped in SkipSelect).
  Iterable<Renderable> get selectableChildren =>
      children.where((e) => e is! SkipSelect);

  /// All children stripped of SkipSelect utility wrapper.
  Iterable<Renderable> get unwrapChildren =>
      children.map((e) => e is SkipSelect ? e.child : e);

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    final children = unwrapChildren.toList();
    if (children.isEmpty) return 0;

    if (axis != this.axis) {
      if (!crossAxisTight) {
        return null;
      }

      // calculating cross size (height) given total main size (width) of row
      // calculate main sizes given unlimited height

      final mainSizes = _sizeChildren(
        container,
        crossSize,
        100000000000,
      );

      // distribute flexible space

      final crossSizes = [
        for (int i = 0; i < children.length; i++)
          children[i].getSize(container, axis, mainSizes[i].size),
      ];

      if (crossSizes.any((element) => element != null)) {
        return crossSizes.whereType<int>().max;
      } else {
        return null;
      }
    }

    if (alignment != FlexAlignment.tight) return null;

    final sizes = children.map((e) {
      if (e is InflexibleSized) {
        return e.size;
      }
      if (e is Flexible) {
        return null;
      }
      return e.getSize(container, axis, crossSize);
    });
    if (!sizes.any((e) => e != null)) {
      return null;
    } else {
      return sizes.map((e) => e ?? 0).reduce((a, b) => a + b);
    }
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (passthrough != null) {
      return selectableChildren
              .elementAtOrNull(passthrough!)
              ?.keyPressed(container, key) ??
          false;
    }
    if (selectableChildren.length == 1) {
      return selectableChildren.single.keyPressed(container, key);
    }
    return false;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) {
    if (passthrough != null) {
      return selectableChildren
              .elementAtOrNull(passthrough!)
              ?.getUsageInfo(container) ??
          const [];
    }
    if (selectableChildren.length == 1) {
      return selectableChildren.single.getUsageInfo(container);
    }
    return const [];
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    if (width == 0 || height == 0 || children.isEmpty) {
      return AxelMap.empty(width, height);
    }

    final containerMainAxisSize = switch (axis) {
      Axis.horizontal => width,
      Axis.vertical => height,
    };
    final containerCrossAxisSize = switch (axis) {
      Axis.horizontal => height,
      Axis.vertical => width,
    };

    final childSizes = _sizeChildren(
      container,
      containerMainAxisSize,
      containerCrossAxisSize,
    );
    final renderedChildren = _renderChildren(
      container,
      childSizes,
      containerCrossAxisSize,
    )..resize(width, height);

    if (renderedChildren.width != width || renderedChildren.height != height) {
      throw UnsupportedError(
        'Child render failed to match required size. This is an error in the layout algorithm.',
      );
    }
    return renderedChildren;
  }

  List<({Renderable child, int size})> _sizeChildren(
    ProviderContainer container,
    int containerMainAxisSize,
    int containerCrossAxisSize,
  ) {
    final sizedChildren = [
      for (final child in unwrapChildren)
        (
          child: child,
          size: (Renderable e) {
            if (e is InflexibleSized) {
              return e.size;
            }
            if (e is Flexible) {
              return null;
            }
            return e.getSize(container, axis, containerCrossAxisSize);
          }(child)
        ),
    ];

    final totalSizedChildrenLength =
        sizedChildren.map<int>((e) => e.size ?? 0).reduce((a, b) => a + b);
    final unassignedMainAxisSpace =
        containerMainAxisSize - totalSizedChildrenLength;

    if (unassignedMainAxisSpace <= 0) {
      // Sized children overflowed (or no space left).

      // Special case where we have exactly one sized child:

      if (sizedChildren.where((e) => e.size != null).length == 1) {
        return [
          for (final entry in sizedChildren)
            (
              child: entry.child,
              size: entry.size == null ? 0 : containerMainAxisSize,
            ),
        ];
      }

      // Otherwise render with provided sizes (and clip later).
      // Probably better to shrink children but I couldn't figure it out.
      return [
        for (final entry in sizedChildren)
          (
            child: entry.child,
            size: entry.size ?? 0,
          ),
      ];
    }

    final flexValues = sizedChildren.map((entry) {
      final child = entry.child;
      final size = entry.size;
      if (child is Flexible) {
        return child.flex;
      }
      if (size == null) {
        return 1;
      }
      return 0;
    }).toList();

    if (flexValues.sum == 0 && alignment != FlexAlignment.tight) {
      // Add flexible space according to alignment
      // Treat tight as start in this case
      void addSpace() {
        sizedChildren.add((child: const Spacer(), size: 0));
        flexValues.add(1);
      }

      void insertSpace(int index) {
        sizedChildren.insert(index, (child: const Spacer(), size: 0));
        flexValues.insert(index, 1);
      }

      switch (alignment) {
        case FlexAlignment.tight: // Not possible
        case FlexAlignment.start:
          addSpace();
        case FlexAlignment.end:
          insertSpace(0);
        case FlexAlignment.center:
          addSpace();
          insertSpace(0);
        case FlexAlignment.spaceBetween:
          for (int i = sizedChildren.length - 1; i > 0; i--) {
            insertSpace(i);
          }
        case FlexAlignment.spaceAround:
          for (int i = sizedChildren.length - 1; i > 0; i--) {
            insertSpace(i);
          }
          addSpace();
          insertSpace(0);
      }
    }

    // Distribute flexible space to unsized children.

    final flexValuesTotal = flexValues.sum;
    if (flexValuesTotal == 0) {
      // Tight alignment (no spacers added); just return children with space missing.
      return [
        for (final entry in sizedChildren)
          (
            child: entry.child,
            size: entry.size ?? 0,
          ),
      ];
    }

    int? firstFlexibleIndex;

    for (int i = 0; i < sizedChildren.length; i++) {
      final flexValue = flexValues[i];
      if (flexValue == 0) {
        continue;
      }
      firstFlexibleIndex ??= i;
      sizedChildren[i] = (
        child: sizedChildren[i].child,
        size: ((flexValue / flexValuesTotal) * unassignedMainAxisSpace).round()
      );
    }
    // after previous, all child sizes are non-null
    final flexError =
        containerMainAxisSize - sizedChildren.map((e) => e.size ?? 0).sum;
    firstFlexibleIndex ??= 0;

    sizedChildren[firstFlexibleIndex] = (
      child: sizedChildren[firstFlexibleIndex].child,
      size: max(0, sizedChildren[firstFlexibleIndex].size! + flexError),
    );

    return [
      for (final entry in sizedChildren)
        (child: entry.child, size: entry.size ?? 0),
    ];
  }

  AxelMap _renderChildren(
    ProviderContainer container,
    List<({Renderable child, int size})> children,
    int crossAxisSize,
  ) {
    final List<AxelMap> renderedChildren = [];

    for (int i = 0; i < children.length; i++) {
      final child = children[i].child;
      final mainAxisExtent = children[i].size;

      if (mainAxisExtent == 0) {
        continue;
      }

      final crossAxisExtent = _childCrossAxisExtent(
        container,
        child,
        axis,
        mainAxisExtent,
        crossAxisAlignment,
        crossAxisSize,
      );

      final childHeight = switch (axis) {
        Axis.horizontal => crossAxisSize,
        Axis.vertical => mainAxisExtent,
      };
      final childWidth = switch (axis) {
        Axis.horizontal => mainAxisExtent,
        Axis.vertical => crossAxisSize,
      };

      final spareCrossSpace = crossAxisSize - crossAxisExtent;

      if (spareCrossSpace <= 0) {
        final renderedChild = child.render(container, childWidth, childHeight);
        renderedChildren.add(renderedChild);
        continue;
      }

      final int before;
      final int after;

      switch (crossAxisAlignment) {
        case CrossAxisAlignment.center:
          after = (spareCrossSpace / 2).floor();
          before = spareCrossSpace - after;
        case CrossAxisAlignment.end:
          after = 0;
          before = spareCrossSpace;
        case CrossAxisAlignment.start:
          after = spareCrossSpace;
          before = 0;
        case CrossAxisAlignment.stretch:
          throw UnsupportedError(
            'Cross axis remainder found with stretch alignment. This is an error in the layout algorithm.',
          );
      }

      final borderedChild = Border(
        border: switch (axis.cross) {
          Axis.horizontal => BorderData(left: before, right: after),
          Axis.vertical => BorderData(top: before, bottom: after),
        },
        child: child,
      );
      final renderedChild =
          borderedChild.render(container, childWidth, childHeight);
      renderedChildren.add(renderedChild);
    }

    // Combine
    return renderedChildren.reduce((a, b) {
      switch (axis) {
        case Axis.horizontal:
          a.addColumns(b.asColumns);
        case Axis.vertical:
          a.addRows(b);
      }
      return a;
    });
  }

  int _childCrossAxisExtent(
    ProviderContainer container,
    Renderable child,
    Axis mainAxis,
    int mainAxisExtent,
    CrossAxisAlignment crossAxisAlignment,
    int crossAxisSize,
  ) {
    if (crossAxisAlignment == CrossAxisAlignment.stretch) {
      return crossAxisSize;
    }
    final crossAxisExtent =
        child.getSize(container, axis.cross, mainAxisExtent);
    if (crossAxisExtent == null) {
      return crossAxisSize;
    }
    return crossAxisExtent;
  }
}

/// Lay out children in a horizontal row.
/// Places sized children by their intrinsic size, then distributes remaining space among flexible children.
/// Wrap children in Flexible or Inflexible components to change how this component treats them.
class Row extends FlexComponent {
  /// Lay out children in a horizontal row.
  /// Places sized children by their intrinsic size, then distributes remaining space among flexible children.
  /// Wrap children in Flexible or Inflexible components to change how this component treats them.
  const Row({
    required super.children,
    super.alignment = FlexAlignment.start,
    super.crossAxisAlignment = CrossAxisAlignment.stretch,
    super.crossAxisTight = false,
    super.passthrough,
  }) : super(axis: Axis.horizontal);
}

/// Lay out children in a vertical column.
/// Places sized children by their intrinsic size, then distributes remaining space among flexible children.
/// Wrap children in Flexible or Inflexible components to change how this component treats them.
class Column extends FlexComponent {
  /// Lay out children in a vertical column.
  /// Places sized children by their intrinsic size, then distributes remaining space among flexible children.
  /// Wrap children in Flexible or Inflexible components to change how this component treats them.
  const Column({
    required super.children,
    super.alignment = FlexAlignment.start,
    super.crossAxisAlignment = CrossAxisAlignment.stretch,
    super.crossAxisTight = false,
    super.passthrough,
  }) : super(axis: Axis.vertical);
}
