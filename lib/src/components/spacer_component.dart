import '../model/axel.dart';
import '../renderable.dart';
import 'filled_component.dart';
import 'flexible.dart';

/// Flexible space in a flexible layout.
class Spacer extends Flexible {
  /// Axel to draw as fill.
  final Axel fill;

  /// Flexible space in a flexible layout.
  const Spacer({super.flex = 1, this.fill = Axel.space})
      : super(child: const Filled(fill: Axel.space));

  @override
  Renderable build(ProviderContainer container) => Filled(fill: fill);
}
