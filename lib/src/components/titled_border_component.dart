import 'package:characters/characters.dart';

import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/border_data.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import 'border_component.dart';

/// Surrounds the child with a border containing a title in the top left.
class TitledBorder extends Renderable {
  /// Child to surround.
  final Renderable child;

  /// Fill for the border.
  final Axel borderFill;

  /// Title to draw.
  final String title;

  /// Border to guarantee either side of the title.
  final int borderPadding;

  /// Opaque spaces to draw around the title.
  final int spacePadding;

  /// Surrounds the child with a border containing a title in the top left.
  const TitledBorder({
    required this.child,
    required this.title,
    required this.borderFill,
    this.borderPadding = 2,
    this.spacePadding = 1,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    switch (axis) {
      case Axis.horizontal:
        return child.getSize(container, axis, crossSize);
      case Axis.vertical:
        final childSize = child.getSize(container, axis, crossSize);
        if (childSize == null) {
          return null;
        } else {
          return childSize + 1;
        }
    }
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    if (height == 1) {
      return child.render(container, width, height);
    }
    final paddingSize = 2 * (borderPadding + spacePadding);

    // If there's no space for the title just give up.
    if (paddingSize >= width) {
      return Border(border: const BorderData(top: 1), child: child)
          .render(container, width, height);
    }
    final renderedChild = child.render(container, width, height - 1);

    if (title.isEmpty) {
      return renderedChild..addRow(borderFill * width, start: true);
    }

    final titleCharacters = title.characters.take(width - paddingSize);
    final extraBorderLength = width - (titleCharacters.length + paddingSize);

    final titledBorder = [
      ...borderFill * borderPadding,
      ...Axel.space * spacePadding,
      ...titleCharacters.map((c) => Axel(c)),
      ...Axel.space * spacePadding,
      ...borderFill * extraBorderLength,
      ...borderFill * borderPadding,
    ];

    renderedChild.addRow(titledBorder, start: true);
    return renderedChild;
  }
}
