import '../../components.dart';
import '../component.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import '../stateful_component.dart';
import 'flex_component.dart';

/// Flex component which allows children to be selected using the arrow keys.
class SelectorFlex extends StatefulComponent<int?> {
  /// List of builders returning children (depending on [focused] for their decoration).
  /// Children should not change in size depending on [focused].
  final List<Renderable Function(bool focused)> children;

  /// Whether to pass [focused] to selected child.
  final bool focused;

  /// Main axis flex alignment.
  final FlexAlignment alignment;

  /// Cross axis flex alignment.
  final CrossAxisAlignment crossAxisAlignment;

  /// Whether to be tight to children in the cross axis.
  final bool crossAxisTight;

  /// Main axis of the flex.
  final Axis axis;

  /// Starting selected child.
  final int startingState;

  /// Adds extra controls.
  final bool Function(
    ProviderContainer container,
    KeyInput key,
    int Function(ProviderContainer) getState,
    void Function(
      ProviderContainer container,
      int Function(int) update,
    ) setState,
    int length,
  )? extraControl;

  /// Adds extra controls before consulting the child.
  final bool controlOverrideChild;

  /// Flex component which allows children to be selected using the arrow keys.
  const SelectorFlex({
    required super.key,
    required this.children,
    required this.focused,
    this.alignment = FlexAlignment.start,
    this.crossAxisAlignment = CrossAxisAlignment.stretch,
    this.crossAxisTight = false,
    required this.axis,
    this.extraControl,
    this.controlOverrideChild = false,
    this.startingState = 0,
    super.dependency,
  });

  /// Children which can be selected (i.e. not wrapped in SkipSelect).
  Iterable<Renderable Function(bool focused)> get selectableChildren =>
      children.where((e) => e(focused) is! SkipSelect);

  @override
  Renderable build(ProviderContainer container) {
    int state = getState(container);
    final preparedSelectableChildren = selectableChildren.toList();
    if (state >= preparedSelectableChildren.length) {
      setState(container, (_) => preparedSelectableChildren.length - 1);
      state = preparedSelectableChildren.length - 1;
    }
    return FlexComponent(
      children: children
          .map(
            (builder) => builder(
              focused && builder == preparedSelectableChildren[state],
            ),
          )
          .map((e) => e is SkipSelect ? e.child : e)
          .toList(),
      alignment: alignment,
      crossAxisAlignment: crossAxisAlignment,
      crossAxisTight: crossAxisTight,
      axis: axis,
    );
  }

  // @override
  // AxelMap render(ProviderContainer container, int width, int height) =>
  //     super.render(container, width, height);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    final selectableChildrenList = selectableChildren.toList();

    // Allow controls to skip child processing (if we're asked to).
    if (controlOverrideChild && extraControl != null) {
      if (extraControl!(
        container,
        key,
        getState,
        _setState,
        selectableChildrenList.length,
      )) {
        return true;
      }
    }

    if (selectableChildrenList
        .elementAt(getState(container))(focused)
        .keyPressed(container, key)) {
      return true;
    }

    // Normal place to execute extra control.
    if (!controlOverrideChild && extraControl != null) {
      if (extraControl!(
        container,
        key,
        getState,
        _setState,
        selectableChildrenList.length,
      )) {
        return true;
      }
    }

    if (key ==
        switch (axis) {
          Axis.vertical => KeyInput.arrowDown,
          Axis.horizontal => KeyInput.arrowRight,
        }) {
      if (getState(container) == selectableChildrenList.length - 1) {
        return false;
      }
      _setState(container, (state) => state + 1);
      return true;
    }
    if (key ==
        switch (axis) {
          Axis.vertical => KeyInput.arrowUp,
          Axis.horizontal => KeyInput.arrowLeft,
        }) {
      if (getState(container) == 0) {
        return false;
      }
      _setState(container, (state) => state - 1);
      return true;
    }

    return false;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => selectableChildren
      .elementAt(getState(container))(true)
      .getUsageInfo(container);

  void _setState(ProviderContainer container, int Function(int p1) update) =>
      super.setState(container, (_) => update(getState(container)));

  @override
  int getState(ProviderContainer container) {
    final state = super.getState(container);
    return state ?? startingState;
  }

  @override
  StateProvider<int?> get stateProvider => provider(key);

  /// Get this component's internal state provider.
  static final provider =
      StateProvider.family<int?, String?>((ref, key) => null);

  /// Read this component's internal state.
  static int read(
    ProviderContainer container,
    String? key, [
    int startingState = 0,
  ]) {
    final state = container.read(provider(key));
    return state ?? startingState;
  }
}

/// Row component which allows children to be selected with left and right arrow keys.
/// Passes keys through to the selected child.
class SelectorRow extends SelectorFlex {
  /// Row component which allows children to be selected with left and right arrow keys.
  /// Passes keys through to the selected child.
  const SelectorRow({
    required super.key,
    required super.focused,
    required super.children,
    super.alignment = FlexAlignment.start,
    super.crossAxisAlignment = CrossAxisAlignment.stretch,
    super.crossAxisTight = false,
    super.extraControl,
    super.controlOverrideChild = false,
    super.startingState = 0,
    super.dependency,
  }) : super(axis: Axis.horizontal);

  /// Read this component's internal state.
  static int read(
    ProviderContainer container,
    String? key, [
    int startingState = 0,
  ]) =>
      SelectorFlex.read(container, key, startingState);

  /// Get this component's internal state provider.
  static StateProviderFamily<int?, String?> get provider =>
      SelectorFlex.provider;
}

/// Column component which allows children to be selected with up and down arrow keys.
/// Passes keys through to the selected child.
class SelectorColumn extends SelectorFlex {
  /// Column component which allows children to be selected with up and down arrow keys.
  /// Passes keys through to the selected child.
  const SelectorColumn({
    required super.key,
    required super.focused,
    required super.children,
    super.alignment = FlexAlignment.start,
    super.crossAxisAlignment = CrossAxisAlignment.stretch,
    super.crossAxisTight = false,
    super.extraControl,
    super.controlOverrideChild = false,
    super.startingState = 0,
    super.dependency,
  }) : super(axis: Axis.vertical);

  /// Read this component's internal state.
  static int read(
    ProviderContainer container,
    String? key, [
    int startingState = 0,
  ]) =>
      SelectorFlex.read(container, key, startingState);

  /// Get this component's internal state provider.
  static StateProviderFamily<int?, String?> get provider =>
      SelectorFlex.provider;
}

/// Causes child to be skipped from parent SelectorFlex's selection.
/// Also skips child from [passthrough] calculations in FlexComponent and its derivatives.
class SkipSelect extends Component {
  /// Child.
  final Renderable child;

  /// Causes child to be skipped from parent SelectorFlex's selection.
  const SkipSelect(this.child);

  @override
  Renderable build(ProviderContainer container) => child;
}
