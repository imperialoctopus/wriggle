import '../component.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Component to add key listener and usage info to a child.
class KeyListener extends Component {
  /// Key listener function. Return true if the key should be absorbed.
  final bool Function(ProviderContainer container, KeyInput key)?
      keyPressedFunction;

  /// Extra usage info to add to child's.
  final UsageInfo Function(ProviderContainer container)? getUsageInfoFunction;

  /// Component's child.
  final Renderable child;

  /// Causes key listen function to run before checking child, skipping the child if it returns true.
  final bool controlOverrideChild;

  /// Component to add key listener and usage info to a child.
  const KeyListener({
    required this.child,
    bool Function(ProviderContainer container, KeyInput key)? keyPressed,
    UsageInfo Function(ProviderContainer container)? getUsageInfo,
    this.controlOverrideChild = false,
  })  : keyPressedFunction = keyPressed,
        getUsageInfoFunction = getUsageInfo;

  @override
  Renderable build(ProviderContainer container) => child;

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (!controlOverrideChild && super.keyPressed(container, key)) {
      return true;
    }
    if (keyPressedFunction != null && keyPressedFunction!(container, key)) {
      return true;
    }
    if (controlOverrideChild && super.keyPressed(container, key)) {
      return true;
    }
    return false;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) {
    if (getUsageInfoFunction != null) {
      return super.getUsageInfo(container) + getUsageInfoFunction!(container);
    }
    return super.getUsageInfo(container);
  }
}
