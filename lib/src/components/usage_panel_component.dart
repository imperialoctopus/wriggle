import 'package:collection/collection.dart';

import '../component.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import 'flex_component.dart';
import 'flow_component.dart';
import 'inflexible_space.dart';
import 'text_component.dart';

/// Panel to display UsageInfo in a convenient way.
class UsagePanel extends Component {
  /// UsageInfo to display.
  final UsageInfo usageInfo;

  /// Panel to display UsageInfo in a convenient way.
  const UsagePanel({required this.usageInfo});

  // @override
  // int? getSize(_, Axis axis, int crossSize) => switch (axis) {
  //       Axis.horizontal => null,
  //       Axis.vertical => 4,
  //     };

  @override
  Renderable build(ProviderContainer container) {
    return Flow(
      itemCrossSize: 1,
      children: usageInfo
          .mapIndexed(
            (i, helpItem) => Row(
              alignment: FlexAlignment.tight,
              crossAxisTight: true,
              children: [
                Text(
                  ' ${helpItem.key} ',
                  style: const TextStyle(inverted: true),
                ),
                const InflexibleSpace(1),
                if (helpItem.name.isNotEmpty) Text('${helpItem.name} '),
                const InflexibleSpace(1),
              ],
            ),
          )
          .toList(),
    );
  }
}
