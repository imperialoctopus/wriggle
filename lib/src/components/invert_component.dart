import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Applies invert text style to child depending on [enable]; useful for identifying currently selected items.
class Invert extends Renderable {
  /// Whether to apply effect to child.
  final bool enable;

  /// If not null, overrides the child's faint to this value if [enabled].
  final bool? faint;

  /// Child.
  final Renderable child;

  /// Applies invert text style to child depending on [enable]; useful for identifying currently selected items.
  const Invert({
    required this.enable,
    this.faint,
    required this.child,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      child.getSize(container, axis, crossSize);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    if (enable) {
      return AxelMap(
        child.render(container, width, height).map(
              (row) => row.map(
                (axel) => axel.copyWith(
                  textStyle: faint != null
                      ? axel.textStyle.copyWith(inverted: true, faint: faint!)
                      : axel.textStyle.copyWith(inverted: true),
                ),
              ),
            ),
      );
    } else {
      return child.render(container, width, height);
    }
  }
}
