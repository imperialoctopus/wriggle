import '../component.dart';
import '../model/key_input/key_input.dart';
import '../renderable.dart';

/// Component to remove keys from a child's listener.
/// Runs [keyRelease] for every input, only allowing those that return false to reach the child.
class KeyRelease extends Component {
  /// Child to modify.
  final Renderable child;

  /// Return false for keys to pass to the child, true for keys to release.
  final bool Function(ProviderContainer container, KeyInput key) keyRelease;

  /// Component to remove keys from a child's listener.
  /// Runs [keyRelease] for every input, only allowing those that return false to reach the child.
  const KeyRelease({
    required this.child,
    required this.keyRelease,
  });

  @override
  Renderable build(ProviderContainer container) => child;

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (keyRelease(container, key)) {
      return false;
    }
    return super.keyPressed(container, key);
  }
}
