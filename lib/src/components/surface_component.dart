import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Replaces spaces in the child with opaque spaces.
class Surface extends Renderable {
  /// Child.
  final Renderable child;

  /// Replaces spaces in the child with opaque spaces.
  const Surface({required this.child});

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      child.getSize(container, axis, crossSize);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    return AxelMap(
      child.render(container, width, height).map(
            (row) => row.map(
              (axel) => (axel == Axel.space) ? Axel.opaqueSpace : axel,
            ),
          ),
    );
  }
}
