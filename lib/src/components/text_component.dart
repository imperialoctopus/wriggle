import 'dart:math';

import 'package:collection/collection.dart';

import '../extensions/pad_axels.dart';
import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/color/color.dart';
import '../model/key_input/key_input.dart';
import '../model/text_alignment.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

export '../model/text_alignment.dart';

/// Renderable to display text.
class Text extends Renderable {
  /// Text to display.
  final String text;

  /// Text foreground colour.
  final ColorPlaceholder? color;

  /// Text background colour.
  final ColorPlaceholder? background;

  /// Text style.
  final TextStyle? style;

  /// Alignment of text. Does nothing for non-wrapped text as this will report the text's length as its width.
  final TextAlignment textAlignment;

  /// If provided text doesn't fit on one line, allow it to wrap.
  /// If true, this is unsized horizontally and sized to fit the text vertically.
  /// If false, this reports a height of 1 and a width equal to the length of the text.
  final bool wrap;

  /// Renderable to display text.
  const Text(
    this.text, {
    this.color,
    this.background,
    this.style,
    this.textAlignment = TextAlignment.left,
    this.wrap = false,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    switch (axis) {
      case Axis.vertical:
        if (wrap) {
          return wrapText(text, crossSize, null).length;
        } else {
          return 1;
        }
      case Axis.horizontal:
        if (wrap) {
          return null;
        } else {
          return text.length;
        }
    }
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) => false;

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => const [];

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    if (text.isEmpty) {
      return AxelMap.empty(width, height);
    }
    final preparedText = wrap ? Text.wrapText(text, width, height) : [text];

    return AxelMap(
      preparedText.map(
        (row) => row
            .split('')
            .take(width)
            .map(
              (c) => Axel(
                c,
                color: color ?? Colors.foreground,
                background: background ?? Colors.background,
                textStyle: style ?? const TextStyle(),
              ),
            )
            .pad(
              width,
              textAlignment,
              Axel(
                ' ',
                color: color ?? Colors.foreground,
                background: background ?? Colors.background,
                textStyle: style ?? const TextStyle(),
              ),
            ),
      ),
    )..resize(width, height);
  }

  /// Wrap the provided text using [width]. If [height] is provided, constrains the text to that height.
  static List<String> wrapText(String text, int width, int? height) {
    final row = StringBuffer();
    final rows = <String>[];

    final lines = text.split('\n');
    for (final line in lines) {
      // final words = line
      //     .split(' ')
      //     .map((e) => e.characters.slices(width - 1).map((f) => f.join()))
      //     .flattened;

      // final words = <String>[];
      // for (final word in line.split(' ')) {
      //   if (word.length > width) {
      //     words.add(word.substring(word.length - width));
      //   } else {
      //     words.add(word);
      //   }
      // }
      // if (words.isEmpty) {
      //   words.add('');
      // }
      final split = line.split(' ').toList();
      final lastIndex = split.length - 1;
      final words = [
        for (final e in split.mapIndexed((i, e) => i == lastIndex ? e : '$e '))
          if (e.isEmpty || width == 0 || e.length < width)
            e
          else
            ...e.divide(width),
      ];

      for (final word in words) {
        if (row.length + word.length > width) {
          // if (row.isEmpty) {
          //   row.write(word);
          //   row.write(' ');
          //   rows.add(row.toString());
          //   row.clear();
          //   continue;
          // }
          // not enough space left on this line

          // add current row to result
          rows.add(row.toString());
          row.clear();
        }
        // enough space on this line for the word
        row.write(word);
      }
      if (row.isNotEmpty) {
        rows.add(row.toString());
        row.clear();
      }
    }

    if (height != null) {
      return rows.take(height).toList();
    }

    return rows;
  }
}

extension _DivideString on String {
  List<String> divide(int divisor) {
    return [
      for (int i = 0; i <= (length ~/ divisor); i++)
        substring(i * divisor, min(length, (i + 1) * divisor)),
    ];
  }
}
