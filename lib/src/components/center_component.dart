import '../component.dart';
import '../renderable.dart';
import 'flex_component.dart';

/// Centres a single sized child in both axes. American spelling for consistency with Flutter.
class Center extends Component {
  /// Child to centre!
  final Renderable child;

  /// Centres a single sized child in both axes. American spelling for consistency with Flutter.
  const Center({required this.child});

  @override
  Renderable build(ProviderContainer container) {
    return Column(
      alignment: FlexAlignment.center,
      children: [
        Row(
          alignment: FlexAlignment.center,
          crossAxisTight: true,
          children: [child],
        ),
      ],
    );
  }
}
