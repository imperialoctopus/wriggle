import '../component.dart';
import '../model/axis.dart';
import '../renderable.dart';
import 'flex_component.dart';

/// Lays out children in a grid.
class Grid extends Component {
  /// Children to lay out.
  final List<Renderable> children;

  /// Number of children to display in each cross axis item.
  final int crossAxisCount;

  /// Direction of main axis.
  final Axis mainAxis;

  /// FlexAlignment along main axis.
  final FlexAlignment mainAxisAlignment;

  /// FlexAlignment along cross axis items.
  final FlexAlignment crossAxisAlignment;

  /// Lays out children in a grid.
  const Grid({
    required this.children,
    required this.crossAxisCount,
    this.mainAxis = Axis.vertical,
    this.mainAxisAlignment = FlexAlignment.start,
    this.crossAxisAlignment = FlexAlignment.tight,
  });

  @override
  Renderable build(ProviderContainer container) => FlexComponent(
        alignment: mainAxisAlignment,
        axis: mainAxis,
        children: [
          for (int i = 0; i < (children.length / crossAxisCount).ceil(); i++)
            FlexComponent(
              alignment: crossAxisAlignment,
              axis: mainAxis.cross,
              children: [
                ...children.skip(i * crossAxisCount).take(crossAxisCount),
              ],
            ),
        ],
      );
}
