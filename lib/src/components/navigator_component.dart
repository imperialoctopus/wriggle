import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Key for navigator component.
class NavKey<T> {
  /// Starting value.
  final T startingValue;

  /// Function to get parent for current state (used for navigate up).
  final T Function(T current) navigateUp;

  /// Key for navigator component.
  const NavKey({
    required this.startingValue,
    required this.navigateUp,
  });
}

/// Builds child based on public route state.
/// Use Navigator.setRoute<T> and Navigator.clearRoute<T> to change route.
/// Provided type and key are used to differentiate states.
class Navigator<T> extends Renderable {
  /// Key used to differentiate states.
  final NavKey<T> key;

  /// Builder for child based on state.
  /// Null for starting state.
  final Renderable Function(ProviderContainer container, T route) builder;

  /// Builds child based on public route state.
  /// Use Navigator.setRoute<T> and Navigator.clearRoute<T> to change route.
  /// Provided type and key are used to differentiate states.
  const Navigator({
    required this.key,
    required this.builder,
  });

  /// Gets the current route for the type provided. Null if starting route.
  static T currentRoute<T>(
    ProviderContainer container,
    NavKey<T> key,
  ) {
    final prov = _stateProvider(key);
    final state = container.read(prov);
    if (state is T?) {
      return state ?? key.startingValue;
    } else {
      throw StateError('Provider returned invalid type.');
    }
  }

  /// Set navigator route.
  /// Return true if page changed, otherwise false.
  static bool navigate<T>(
    ProviderContainer container,
    NavKey<T> key,
    T? route,
  ) {
    if (currentRoute<T>(container, key) == route) {
      return false;
    }
    container.read(_stateProvider(key).notifier).state = route;
    return true;
  }

  /// Reset navigator to starting state.
  /// Returns true if page changed, otherwise false.
  static bool navigateHome<T>(
    ProviderContainer container,
    NavKey<T> key,
  ) =>
      navigate<T>(container, key, key.startingValue);

  /// Navigate to the parent of the current route.
  /// Returns true if page changed, otherwise false.
  static bool navigateUp<T>(
    ProviderContainer container,
    NavKey<T> key,
  ) {
    final current = currentRoute<T>(container, key);

    final parent = key.navigateUp(current);
    if (parent == current) {
      return false;
    }
    // if (parent is! T?) {
    //   throw ArgumentError('Route\'s parent must match its own type.');
    // }
    navigate<T>(container, key, parent);
    return true;
  }

  /// Get current child.
  Renderable getChild(ProviderContainer container) =>
      builder(container, getState(container) ?? key.startingValue);

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      getChild(container).getSize(container, axis, crossSize);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      getChild(container).getUsageInfo(container);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      getChild(container).keyPressed(container, key);

  @override
  AxelMap render(ProviderContainer container, int width, int height) =>
      getChild(container).render(container, width, height);

  /// Get local state of this object.
  T? getState(ProviderContainer container) {
    return container.read(stateProvider<T>(key));
  }

  /// Set local state of this object.
  void setState(ProviderContainer container, T? Function(T?) update) {
    final prov = _stateProvider(key);
    container.read(prov.notifier).update((_) => update(getState(container)));
  }

  /// Provider for local state
  static AlwaysAliveProviderListenable<T?> stateProvider<T>(NavKey<T> key) =>
      _stateProvider(key).select<T?>((value) {
        if (value is T?) {
          return value;
        } else {
          throw StateError('Provider returned invalid type.');
        }
      });

  static final _stateProvider =
      StateProvider.family<Object?, NavKey<Object?>>((ref, arg) => null);
}
