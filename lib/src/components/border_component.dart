import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/border_data.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Renders the child, then adds a configurable border.
class Border extends Renderable {
  /// Data describing the border to draw.
  final BorderData border;

  /// Child.
  final Renderable child;

  /// Renders the child, then adds a configurable border.
  const Border({
    required this.border,
    required this.child,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    final childSize = child.getSize(
      container,
      axis,
      switch (axis.cross) {
        Axis.horizontal => crossSize - (border.left + border.right),
        Axis.vertical => crossSize - (border.top + border.bottom),
      },
    );
    if (childSize == null) return null;

    switch (axis) {
      case Axis.horizontal:
        return childSize + border.left + border.right;
      case Axis.vertical:
        return childSize + border.top + border.bottom;
    }
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final verticalMargin = border.top + border.bottom;
    final horizontalMargin = border.left + border.right;
    // If there isn't enough space for the border, just draw the child.
    if (width <= horizontalMargin || height <= verticalMargin) {
      return AxelMap(
        List.generate(
          height,
          (_) => List.filled(width, border.fill),
        ),
      );
    }

    final renderedChild = child.render(
      container,
      width - horizontalMargin,
      height - verticalMargin,
    );

    // Left
    renderedChild.addColumns(
      List.generate(
        border.left,
        (_) => List.filled(renderedChild.height, border.fill),
      ),
      start: true,
    );

    // Right
    renderedChild.addColumns(
      List.generate(
        border.right,
        (_) => List.filled(renderedChild.height, border.fill),
      ),
    );

    // Top
    renderedChild.addRows(
      List.generate(
        border.top,
        (_) => List.filled(renderedChild.width, border.fill),
      ),
      start: true,
    );

    // Bottom
    renderedChild.addRows(
      List.generate(
        border.bottom,
        (_) => List.filled(renderedChild.width, border.fill),
      ),
    );

    return renderedChild;
  }
}
