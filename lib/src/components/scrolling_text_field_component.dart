import 'dart:math';

import 'package:collection/collection.dart';

import '../context.dart';
import '../extensions/pad_axels.dart';
import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/color/color.dart';
import '../model/direction.dart';
import '../model/key_input/key_input.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import '../stateful_component.dart';
import 'flex_component.dart';
import 'text_component.dart';

typedef _State = ({
  String? content,
  int row,
  int column,
  ({int course, int fine}) scrollOffset,
  int height,
  int width,
});

/// Type of margin for the scrolling text field.
enum MarginType {
  /// No margin at all.
  none,

  /// Just a 3-pixel margin (highlights line with cursor on).
  margin,

  /// Normal line numbers.
  lineNumbers,
}

/// Multi-line text field that allows scrolling.
class ScrollingTextField extends StatefulComponent<_State> {
  /// Multi-line content.
  final String startingContent;

  /// Whether to show line numbers on the left side.
  final MarginType marginType;

  /// Whether to default cursor to start or end of the content.
  final bool startCursorAtEnd;

  /// Shows cursor and highlights cursor's line if focused.
  final bool focused;

  /// Called when the content has changed.
  final void Function(ProviderContainer, String)? contentChanged;

  /// Tightens size in the main axis.
  final bool tight;

  /// Multi-line text field that allows scrolling.
  const ScrollingTextField({
    required super.key,
    required this.startingContent,
    this.marginType = MarginType.none,
    this.startCursorAtEnd = true,
    this.focused = true,
    this.tight = false,
    this.contentChanged,
    super.dependency,
  });

  int get _marginSize => switch (marginType) {
        MarginType.none => 0,
        MarginType.lineNumbers => 4,
        MarginType.margin => 2,
      };

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) => null;

  String _getContent(ProviderContainer container) =>
      getState(container).content ?? startingContent;

  void _updateContent(ProviderContainer container, String newValue) {
    contentChanged?.call(container, newValue);
    setState(
      container,
      (state) => (
        content: newValue,
        height: state.height,
        width: state.width,
        row: state.row,
        column: state.column,
        scrollOffset: state.scrollOffset,
      ),
    );
  }

  @override
  Renderable build(ProviderContainer container) {
    final state = getState(container);
    final lines = _getContent(container).split('\n');

    int? passthrough;
    if (state.row < lines.length) {
      passthrough = state.row - state.scrollOffset.course;
    }

    return Column(
      alignment: tight ? FlexAlignment.tight : FlexAlignment.start,
      passthrough: passthrough,
      children: lines
          .skip(state.scrollOffset.course)
          .mapIndexed(
            (index, line) => _ScrollingTextFieldLine(
              content: line,
              index: index + state.scrollOffset.course,
              offset: index == 0 ? state.scrollOffset.fine : 0,
              width: state.width,
              marginSize: _marginSize,
              marginType: marginType,
              cursorPos: state.row == (index + state.scrollOffset.course)
                  ? state.column
                  : null,
              focused: focused,
              onMoveCursor: (container, newPosition) {
                _setCursor(container, column: newPosition);
              },
              onNavigateOut: (container, direction, column, width) {
                switch (direction) {
                  case Direction.up:
                    if (state.row == 0) {
                      return false;
                    }
                    if (column == null) {
                      _setCursor(
                        container,
                        row: state.row - 1,
                        column: lines[state.row - 1].length,
                      );
                      return true;
                    } else {
                      _setCursorAscending(
                        container,
                        state.row - 1,
                        column,
                        width,
                      );
                      return true;
                    }
                  case Direction.down:
                    if (state.row == lines.length - 1) {
                      if (lines.last.isNotEmpty) {
                        _insertCharacters(
                          container,
                          (row: state.row, column: lines.last.length),
                          '\n',
                        );
                        _setCursor(container, row: state.row + 1, column: 0);
                        return true;
                      }
                      return false;
                    }
                    _setCursor(container, row: state.row + 1, column: column);
                    return true;
                  case Direction.right:
                  case Direction.left:
                    return false;
                }
              },
            ),
          )
          .toList(),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    final state = getState(container);
    final lines = _getContent(container).split('\n');

    // If text changes and the cursor is outside it, we wait for a key press to
    // move the cursor back in bounds (gives state above us a chance to update).
    // I'm sure there's a less stupid way of fixing this state problem...
    //      but I'm tired.
    if (state.row >= lines.length) {
      _setCursor(container, row: lines.length - 1, column: lines.last.length);
      return true;
    } else if (state.column > lines[state.row].length) {
      _setCursor(container, column: lines[state.row].length);
      return true;
    }

    if (build(container).keyPressed(container, key)) {
      return true;
    }

    switch (key) {
      case KeyInput.enter:
        _insertCharacters(
          container,
          (row: state.row, column: state.column),
          '\n',
        );
        _setCursor(container, row: state.row + 1, column: 0);
        return true;

      case KeyInput.tab:
        _insertCharacters(
          container,
          (row: state.row, column: state.column),
          '    ',
        );
        _setCursor(container, row: state.row, column: state.column + 4);
        return true;

      case KeyInput.backspace:
        if (state.column == 0) {
          if (state.row > 0) {
            _setCursor(
              container,
              row: state.row - 1,
              column: lines[state.row - 1].length,
            );
            _deleteCharacters(
              container,
              (row: state.row - 1, column: lines[state.row - 1].length),
            );
          }
          return true;
        } else {
          _setCursor(container, column: state.column - 1);
          _deleteCharacters(
            container,
            (row: state.row, column: state.column - 1),
          );
          return true;
        }

      case KeyInput.delete:
        _deleteCharacters(
          container,
          (row: state.row, column: state.column),
        );
        return true;

      case KeyInput.controlDelete:
        final text = lines[state.row];
        if (state.column == text.length) {
          _deleteCharacters(
            container,
            (row: state.row, column: state.column),
          );
          return true;
        }

        final characters = text.split('');
        var position = state.column + 1;
        while (position < characters.length - 1) {
          if (characters[position] == ' ' && characters[position + 1] != ' ') {
            _deleteCharacters(
              container,
              (row: state.row, column: state.column),
              (row: state.row, column: position),
            );
            return true;
          }
          position++;
        }
        _deleteCharacters(
          container,
          (row: state.row, column: state.column),
          (row: state.row, column: text.length),
        );
        return true;

      case KeyInput.controlHome:
        _setCursor(container, row: 0, column: 0);
        return true;

      case KeyInput.controlEnd:
        _setCursor(
          container,
          row: lines.length - 1,
          column: lines.last.length,
        );
        return true;

      case KeyInput.altArrowUp:
        if (state.row > 0) {
          _updateContent.call(
            container,
            (lines.toList()..swap(state.row, state.row - 1)).join('\n'),
          );
          _setCursor(container, row: state.row - 1);
        }
        return true;

      case KeyInput.altArrowDown:
        if (state.row < lines.length - 1) {
          _updateContent.call(
            container,
            (lines.toList()..swap(state.row, state.row + 1)).join('\n'),
          );
          _setCursor(container, row: state.row + 1);
        }
        return true;

      case KeyInput.controlY:
        final text = lines[state.row];
        if (text.isNotEmpty) {
          Wriggle.clipboard.write(text);
        }
        return true;

      case KeyInput.controlK:
        final text = lines[state.row];
        if (text.isNotEmpty) {
          Wriggle.clipboard.write(text);
        }
        _deleteCharacters(
          container,
          (row: state.row, column: 0),
          (row: state.row, column: text.length),
        );
        _setCursor(container, column: 0);
        return true;

      case KeyInput.controlU:
        Wriggle.clipboard.read().then((text) {
          _insertCharacters(
            container,
            (row: state.row, column: state.column),
            text,
          );
          final newLines = lines.toList();
          final rowOffset = lines.length - state.row;
          final colOffset = lines[state.row].length - state.column;
          final newRow = newLines.length - rowOffset;
          final newCol = newLines[newRow].length - colOffset;
          _setCursor(
            container,
            row: newRow,
            column: newCol + text.length,
          );
        });
        return true;

      default:
        break;
    }

    if (key.isPrintable) {
      _insertCharacters(
        container,
        (row: state.row, column: state.column),
        key.string,
      );
      final newLines = lines.toList();
      final rowOffset = lines.length - state.row;
      final colOffset = lines[state.row].length - state.column;
      final newRow = newLines.length - rowOffset;
      final newCol = newLines[newRow].length - colOffset;
      _setCursor(container, row: newRow, column: newCol + key.string.length);
      return true;
    }

    return false;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => [
        (key: 'Ctrl+K', name: 'Cut'),
        (key: 'Ctrl+Y', name: 'Copy'),
        (key: 'Ctrl+U', name: 'Paste'),
      ];

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    _setSize(container, height, width);
    return super.render(container, width, height);
  }

  void _insertCharacters(
    ProviderContainer container,
    ({int row, int column}) position,
    String chars,
  ) {
    final lines = _getContent(container).split('\n');
    final line = lines[position.row];
    final prefix = line.substring(0, position.column);
    final suffix = line.substring(position.column);

    lines[position.row] = "$prefix$chars$suffix";
    _updateContent(container, lines.join('\n'));
  }

  void _deleteCharacters(
    ProviderContainer container,
    ({int row, int column}) start, [
    ({int row, int column})? end,
  ]) {
    end ??= start;
    if (start.row > end.row) {
      throw ArgumentError('End should be after start');
    } else if (start.row == end.row) {
      final lines = _getContent(container).split('\n');
      final line = lines[start.row];

      bool removeBreak = false;
      if (end.column == line.length) {
        removeBreak = true;
      }

      final prefix = line.substring(0, start.column);
      final suffix = line.substring(end.column + (removeBreak ? 0 : 1));
      lines[start.row] = prefix + suffix;

      if (removeBreak && start.row != lines.length - 1) {
        lines[start.row] = lines[start.row] + lines.removeAt(start.row + 1);
      }

      _updateContent(container, lines.join('\n'));
    } else {
      final lines = _getContent(container).split('\n');
      lines[start.row] = lines[start.row].substring(0, start.column);
      lines[end.row] = lines[end.row].substring(end.column + 1);
      lines.removeRange(start.row + 1, end.row);
      _updateContent(container, lines.join('\n'));
    }
  }

  ({int course, int fine}) _checkCursorBounds(
    ProviderContainer container,
    int row,
    int column,
    ({int course, int fine}) scrollOffset,
    int height,
    int width,
  ) {
    int lineFromOffset(List<int> sizes, int course, int fine) {
      if (course == 0) {
        return fine;
      } else {
        return sizes.take(course).reduce((a, b) => a + b) + fine;
      }
    }

    ({int course, int fine}) offsetFromLine(List<int> sizes, int line) {
      int remainder = line;
      for (int i = 0; i < sizes.length; i++) {
        if (remainder < sizes[i]) {
          return (course: i, fine: max(0, remainder));
        }
        remainder -= sizes[i];
      }
      return (course: 0, fine: 0);
    }

    final lines = _getContent(container)
        .split('\n')
        .map((e) => Text.wrapText(e, width - _marginSize, null))
        .toList();
    final sizes = lines.map((e) => e.length).toList();

    final cursorColumnSubRow = () {
      int runningTotal = 0;
      final cursorLine = lines[min(row, lines.length - 1)];

      for (int i = 0; i < cursorLine.length; i++) {
        if (runningTotal + cursorLine[i].length > column) {
          return i;
        }
        runningTotal += cursorLine[i].length;
      }
      return cursorLine.length - 1;
    }();

    final cursorLine = lineFromOffset(sizes, row, cursorColumnSubRow);
    final topLine =
        lineFromOffset(sizes, scrollOffset.course, scrollOffset.fine);
    final bottomLine = topLine + height - 1;

    if (cursorLine < topLine) {
      return offsetFromLine(sizes, cursorLine);
    } else if (cursorLine > bottomLine) {
      return offsetFromLine(sizes, cursorLine - bottomLine + topLine);
    }

    return scrollOffset;
  }

  void _setCursor(ProviderContainer container, {int? row, int? column}) {
    setState(
      container,
      (state) => (
        content: state.content,
        row: row ?? state.row,
        column: column ?? state.column,
        scrollOffset: _checkCursorBounds(
          container,
          row ?? state.row,
          column ?? state.column,
          state.scrollOffset,
          state.height,
          state.width,
        ),
        height: state.height,
        width: state.width,
      ),
    );
  }

  void _setCursorAscending(
    ProviderContainer container,
    int row,
    int column,
    int width,
  ) {
    final remainder = column % width;
    final unwrapped = _getContent(container).split('\n');
    final lines = Text.wrapText(unwrapped[row], width, null);

    final col = () {
      if (lines.length < 2) {
        return remainder;
        //return lines.single.length - 1;
      } else {
        //final lastIndex = lines.last.length - 1;
        return lines
                .take(lines.length - 1)
                .map((e) => e.length)
                .reduce((a, b) => a + b) +
            remainder;
      }
    }();

    _setCursor(container, row: row, column: col);
  }

  void _setSize(ProviderContainer container, int newHeight, int newWidth) {
    final state = getState(container);

    if (newHeight > state.height) {
      setState(
        container,
        (state) => (
          content: state.content,
          row: 0,
          column: 0,
          scrollOffset: (course: 0, fine: 0),
          height: newHeight,
          width: newWidth,
        ),
      );
      _setCursor(container, row: state.row, column: state.column);
    } else if (state.height != newHeight || state.width != newWidth) {
      setState(
        container,
        (state) => (
          content: state.content,
          row: state.row,
          column: state.column,
          scrollOffset: _checkCursorBounds(
            container,
            state.row,
            state.column,
            state.scrollOffset,
            newHeight,
            newWidth,
          ),
          height: newHeight,
          width: newWidth,
        ),
      );
    }
    // Initialisation
    if (state.height == 0 && startCursorAtEnd) {
      final lines = _getContent(container).split('\n');
      _setCursor(
        container,
        row: lines.length - 1,
        column: lines.last.length,
      );
    }
  }

  @override
  StateProvider<_State> get stateProvider => _stateProvider(key);

  static final _stateProvider = StateProvider.family<_State, String?>(
    (ref, arg) => (
      content: null,
      row: 0,
      column: 0,
      scrollOffset: (course: 0, fine: 0),
      height: 0,
      width: 0,
    ),
  );
}

class _ScrollingTextFieldLine extends Renderable {
  final String content;
  final int index;
  final int offset;
  final int width;
  final int? cursorPos;
  final bool focused;
  final int marginSize;
  final MarginType marginType;
  final void Function(ProviderContainer container, int newPosition)
      onMoveCursor;
  final bool Function(
    ProviderContainer container,
    Direction direction,
    int? column,
    int width,
  ) onNavigateOut;

  const _ScrollingTextFieldLine({
    required this.content,
    required this.index,
    required this.offset,
    required this.width,
    required this.cursorPos,
    required this.focused,
    required this.marginSize,
    required this.marginType,
    required this.onMoveCursor,
    required this.onNavigateOut,
  });

  bool get showLineNumberWhenOffset => true;

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    switch (axis) {
      case Axis.vertical:
        if (content.isEmpty) {
          return 1 - offset;
        }
        return Text.wrapText(content, width - marginSize, null).length - offset;
      case Axis.horizontal:
        return width;
    }
  }

  ({int row, int col})? get cursorCoords {
    final cursor = cursorPos;
    if (!focused || cursor == null) {
      return null;
    } else {
      if (content.isEmpty) {
        return (row: 0, col: 0);
      }
      final lines = Text.wrapText(content, width - marginSize, null);
      //int runningTotal = 0;

      int cursorRemaining = cursor;
      for (int i = 0; i < lines.length; i++) {
        final length = lines[i].length;
        if (cursorRemaining < length) {
          if (cursorRemaining > 75) throw Error();
          return (row: i, col: cursorRemaining);
        }
        // if (length == width - marginSize) {
        //   cursorRemaining++;
        // }
        cursorRemaining -= length;
      }
      return (row: lines.length - 1, col: min(width - 1, lines.last.length));

      // for (int i = 0; i < lines.length; i++) {
      //   if (runningTotal + lines[i].length >= cursor) {
      //     final val = cursor - runningTotal;
      //     if (val < 0 || val > 75) throw Error();
      //     return (row: i, col: val);
      //   }
      //   runningTotal += lines[i].length;
      // }
      // if (lines.last.length - 1 > 75) throw Error();
      // return (row: lines.length - 1, col: lines.last.length - 1);

      // return (
      //   row: (cursor / (width - marginSize)).floor(),
      //   col: cursor % (width - marginSize),
      // );
    }
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => const [];

  int cursorCoordsToIndex(int row, int col) {
    if (row == 0) {
      return col;
    }
    if (content.isEmpty) {
      return 0;
    }
    return Text.wrapText(content, width - marginSize, null)
            .take(row)
            .map((e) => e.length)
            .reduce((a, b) => a + b) +
        col;
    //return lines.take(row).map((s) => s.length).reduce((a, b) => a + b) + col;
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    final lines = Text.wrapText(content, width - marginSize, null);

    switch (key) {
      case KeyInput.arrowUp:
        final coords = cursorCoords;
        if (coords == null) {
          return false;
        }
        if (coords.row == 0) {
          return onNavigateOut(
            container,
            Direction.up,
            coords.col,
            width - marginSize,
          );
          //return true;
        }
        onMoveCursor(
          container,
          cursorCoordsToIndex(coords.row - 1, coords.col),
        );
        return true;
      case KeyInput.arrowDown:
        final coords = cursorCoords;
        if (coords == null) {
          return false;
        }
        if (coords.row + 1 >= lines.length) {
          return onNavigateOut(
            container,
            Direction.down,
            coords.col,
            width - marginSize,
          );
          //return true;
        }
        onMoveCursor(
          container,
          cursorCoordsToIndex(coords.row + 1, coords.col),
        );
        return true;
      case KeyInput.arrowLeft:
        final coords = cursorCoords;
        if (coords == null) {
          return false;
        }
        final newCursor = cursorCoordsToIndex(coords.row, coords.col - 1);
        if (newCursor < 0) {
          return onNavigateOut(
            container,
            Direction.up,
            null,
            width - marginSize,
          );
          //return true;
        }
        onMoveCursor(container, newCursor);
        return true;
      case KeyInput.arrowRight:
        final coords = cursorCoords;
        if (coords == null) {
          return false;
        }
        final newCursor = cursorCoordsToIndex(coords.row, coords.col + 1);
        if (newCursor > content.length) {
          return onNavigateOut(
            container,
            Direction.down,
            0,
            width - marginSize,
          );
          //return true;
        }
        onMoveCursor(container, newCursor);
        return true;
      case KeyInput.controlArrowRight:
        int? position = cursorPos;
        if (position == null) {
          return false;
        }
        if (position == content.length) {
          return onNavigateOut(
            container,
            Direction.down,
            0,
            width - marginSize,
          );
          //return true;
        }
        final characters = content.split('');
        position++;
        while (position! < characters.length - 1) {
          if (characters[position] == ' ' && characters[position + 1] != ' ') {
            onMoveCursor(container, position + 1);
            return true;
          }
          position++;
        }
        onNavigateOut(container, Direction.down, 0, width - marginSize);
        return true;
      case KeyInput.controlArrowLeft:
        int? position = cursorPos;
        if (position == null) {
          return false;
        }
        if (position == 0) {
          return onNavigateOut(
            container,
            Direction.up,
            null,
            width - marginSize,
          );
          //return true;
        }
        if (position == 1) {
          onMoveCursor(container, 0);
          return true;
        }
        final characters = content.split('');
        position--;
        while (position! > 0) {
          if (characters[position] != ' ' && characters[position - 1] == ' ') {
            break;
          }
          position--;
        }
        onMoveCursor(container, position);
        return true;
      case KeyInput.home:
        onMoveCursor(container, 0);
        return true;
      case KeyInput.end:
        onMoveCursor(container, content.length);
        return true;
      default:
        return false;
    }
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    // if (text.isEmpty) {
    //   return AxelMap.empty(width, height);
    // }

    final preparedText = content.isEmpty
        ? [' ']
        : Text.wrapText(content, width - marginSize, null).skip(offset);

    // if (offset > 0) {
    //   return AxelMap.filled(
    //       width, height, Axel(preparedText.length.toString()));
    // }

    final axelMap = AxelMap(
      preparedText.mapIndexed(
        (i, row) => [
          if (marginSize > 1)
            ...((marginType == MarginType.lineNumbers &&
                        i == 0 &&
                        (offset == 0 || showLineNumberWhenOffset))
                    ? index + 1
                    : '')
                .toString()
                .padLeft(marginSize - 1)
                .split('')
                .map(
                  (c) => Axel(
                    c,
                    background: Colors.black,
                    textStyle: TextStyle(
                      inverted: cursorPos != null,
                      faint: (marginType == MarginType.margin) || !focused,
                    ),
                  ),
                ),
          if (marginSize > 0) Axel.space,
          ...row
              .split('')
              .take(width - marginSize)
              .map(
                (c) => Axel(
                  c,
                  //color: color,
                  //background: background,
                  //textStyle: style ?? const TextStyle(),
                ),
              )
              .pad(width - marginSize),
        ],
      ),
    )..resize(width, height);

    final cursor = cursorCoords;
    if (cursor != null) {
      //axelMap.editAxel(0, 0, highlight);
      //axelMap.editAxel(0, 1, highlight);
      //axelMap.editAxel(0, 2, highlight);
      //final mapWidth = axelMap.width;

      axelMap.editAxel(
        cursor.row - offset,
        cursor.col + marginSize,
        (axel) => axel.copyWith(textStyle: const TextStyle(inverted: true)),
      );
    }

    // axelMap.editAxel(0, 0, (axel) => axel.copyWith(background: Color.black));
    // axelMap.editAxel(0, 1, (axel) => axel.copyWith(background: Color.black));
    // axelMap.editAxel(0, 2, (axel) => axel.copyWith(background: Color.black));

    return axelMap;
  }
}
