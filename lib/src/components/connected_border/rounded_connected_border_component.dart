import '../../model/axel.dart';
import 'connected_border_data.dart';

/// Connected border using rounded corners.
class RoundedConnectedBorderData extends ConnectedBorderData {
  /// Connected border using rounded corners.
  const RoundedConnectedBorderData();

  static const Axel _vertical = Axel('│');
  static const Axel _horizontal = Axel('─');
  static const Axel _cornerBR = Axel('╯');
  static const Axel _cornerBL = Axel('╰');
  static const Axel _cornerTR = Axel('╮');
  static const Axel _cornerTL = Axel('╭');
  static const Axel _triU = Axel('┬');
  static const Axel _triR = Axel('┤');
  static const Axel _triD = Axel('┴');
  static const Axel _triL = Axel('├');
  static const Axel _cross = Axel('┼');

  @override
  List<Axel> get data => const [
        _horizontal, // 0000
        _horizontal, // 0001
        _vertical, // 0010
        _cornerTR, // 0011
        _horizontal, // 0100
        _horizontal, // 0101
        _cornerTL, // 0110
        _triU, // 0111
        _vertical, // 1000
        _cornerBR, // 1001
        _vertical, // 1010
        _triR, // 1011
        _cornerBL, // 1100
        _triD, // 1101
        _triL, // 1110
        _cross, // 1111
      ];
}
