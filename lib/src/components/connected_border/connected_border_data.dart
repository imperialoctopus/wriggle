import '../../model/axel.dart';

/// Shape of the data required by the connected border component.
abstract class ConnectedBorderData {
  /// Shape of the data required by the connected border component.
  const ConnectedBorderData();

  /// Axel data for a connected border.
  List<Axel> get data;
}
