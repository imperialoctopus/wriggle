import '../../model/axel.dart';
import '../../model/axel_map.dart';
import '../../model/axis.dart';
import '../../model/direction.dart';
import '../../model/key_input/key_input.dart';
import '../../model/usage_info.dart';
import '../../renderable.dart';
import 'connected_border_data.dart';
import 'rounded_connected_border_component.dart';
import 'thin_connected_border_data.dart';

export 'connected_border_data.dart';
export 'rounded_connected_border_component.dart';
export 'thin_connected_border_data.dart';

/// Replaces borderPlaceholder Axels in the child with connected border characters.
class ConnectedBorderComponent extends Renderable {
  /// Child to alter.
  final Renderable child;

  /// Data for the connected border to use. Can be provided manually, or use a pre-made constructor.
  final ConnectedBorderData borderData;

  /// Replaces borderPlaceholder Axels in the child with connected border characters.
  const ConnectedBorderComponent({
    required this.child,
    required this.borderData,
  });

  /// Replaces borderPlaceholder Axels in the child with a thin connected border.
  const ConnectedBorderComponent.thin({
    required this.child,
  }) : borderData = const ThinConnectedBorderData();

  /// Replaces borderPlaceholder Axels in the child with a rounded connected border.
  const ConnectedBorderComponent.rounded({
    required this.child,
  }) : borderData = const RoundedConnectedBorderData();

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      child.getSize(container, axis, crossSize);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final renderedChild = child.render(container, width, height);
    final rebuilt =
        List.generate(height, (_) => List.filled(width, Axel.space));

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        final Axel self = renderedChild.getPosition(x, y)!;

        if (!self.isBorder) {
          rebuilt[y][x] = self;
        } else {
          final bool left = _testConnection(
            self,
            renderedChild.getPosition(x - 1, y),
            Direction.left,
          );
          final bool right = _testConnection(
            self,
            renderedChild.getPosition(x + 1, y),
            Direction.right,
          );
          final bool up = _testConnection(
            self,
            renderedChild.getPosition(x, y - 1),
            Direction.up,
          );
          final bool down = _testConnection(
            self,
            renderedChild.getPosition(x, y + 1),
            Direction.down,
          );
          rebuilt[y][x] = _getBorderPoint(self, left, right, up, down);
        }
      }
    }
    return AxelMap(rebuilt);
  }

  bool _testConnection(Axel self, Axel? neighbour, Direction direction) {
    if (!self.isBorder || neighbour == null || !neighbour.isBorder) {
      return false;
    }
    switch (direction) {
      case Direction.left:
      case Direction.right:
        return (self == Axel.borderPlaceholder ||
                self == Axel.borderPlaceholderHorizontal) &&
            (neighbour == Axel.borderPlaceholder ||
                neighbour == Axel.borderPlaceholderHorizontal);
      case Direction.up:
      case Direction.down:
        return (self == Axel.borderPlaceholder ||
                self == Axel.borderPlaceholderVertical) &&
            (neighbour == Axel.borderPlaceholder ||
                neighbour == Axel.borderPlaceholderVertical);
    }
  }

  Axel _getBorderPoint(Axel self, bool left, bool right, bool up, bool down) =>
      borderData.data[
          (up ? 8 : 0) + (right ? 4 : 0) + (down ? 2 : 0) + (left ? 1 : 0)];
}
