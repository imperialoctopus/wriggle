import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Renders a horizontal progress bar in the space provided.
class ProgressBar extends Renderable {
  /// Proportion of space to fill.
  final double progress;

  /// Reverse direction (left to right).
  final bool reverse;

  /// Faint version.
  final bool faint;

  /// Child to highlight; if null, just draws spaces.
  final Renderable? child;

  /// Renders a horizontal progress bar in the space provided.
  const ProgressBar({
    required this.progress,
    this.reverse = false,
    this.faint = true,
    this.child,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      child?.getSize(container, axis, crossSize);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final cutoff = reverse
        ? width - (width * progress).round()
        : (width * progress).round();

    final axelMap = child?.render(container, width, height) ??
        AxelMap.filled(width, height, Axel.space);

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        axelMap.editAxel(
          y,
          x,
          (axel) => Axel(
            axel.character,
            textStyle: TextStyle(
              inverted: reverse ^ (x < cutoff),
              faint:
                  (faint && (reverse ^ (x < cutoff))) || axel.textStyle.faint,
            ),
          ),
        );
      }
    }

    return axelMap;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child?.getUsageInfo(container) ?? const [];

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child?.keyPressed(container, key) ?? false;
}
