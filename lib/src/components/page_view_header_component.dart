import 'package:collection/collection.dart';

import '../../components.dart';
import '../component.dart';
import '../model/axel.dart';
import '../model/axis.dart';
import '../model/border_data.dart';
import '../renderable.dart';

/// Header for page view component
final class PageViewHeader extends Component {
  /// Index of title to show as selected.
  final int? selected;

  final List<Renderable Function(bool)>? _selectedBuilders;

  final List<Renderable Function(int?)>? _indexBuilders;

  /// Header for page view component
  /// Constructs children by passing selected index to each of them.
  const PageViewHeader({
    required this.selected,
    required List<Renderable Function(bool focused)> items,
  })  : _indexBuilders = null,
        _selectedBuilders = items;

  /// Header for page view component
  /// Constructs children by passing selected index to each of them.
  const PageViewHeader.indexed({
    required this.selected,
    required List<Renderable Function(int?)> items,
  })  : _indexBuilders = items,
        _selectedBuilders = null;

  @override
  Renderable build(ProviderContainer container) {
    return Border(
      border: const BorderData(bottom: 1, fill: Axel.borderPlaceholder),
      child: Flow(
        itemCrossSize: 1,
        //direction: Axis.horizontal,
        //crossAxisTight: true,
        children: [
          if (_indexBuilders != null)
            ..._indexBuilders.map(
              (builder) => builder(selected),
            ),
          if (_selectedBuilders != null)
            ..._selectedBuilders.mapIndexed(
              (i, builder) => builder(i == selected),
            ),
        ],
      ),
    );
  }
}

/// Template header item for PageViewHeader.
class PageViewHeaderItem extends Component {
  /// Child component.
  final Renderable child;

  /// Whether to invert child.
  final bool focused;

  /// If not null, overrides faint value to this when selected.
  final bool? faint;

  /// Template header item for PageViewHeader.
  const PageViewHeaderItem({
    required this.focused,
    this.faint = false,
    required this.child,
  });

  /// Template header item for PageViewHeader.
  /// Convenience function which takes [focused] and wraps this in an Invert.
  static Renderable Function(bool?) wrap(
    Renderable child, {
    bool? faint = false,
  }) =>
      (focused) => PageViewHeaderItem(
            child: child,
            faint: faint,
            focused: focused ?? false,
          );

  /// Template header item for PageViewHeader.
  /// Convenience function which takes [focused] and wraps this in an Invert.
  static Renderable Function(int?) index(
    int index,
    Renderable child, {
    bool? faint = false,
  }) =>
      (i) =>
          PageViewHeaderItem(child: child, faint: faint, focused: index == i);

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    switch (axis) {
      case Axis.vertical:
        return 1;

      case Axis.horizontal:
        return super.getSize(container, axis, crossSize);
    }
  }

  @override
  Renderable build(ProviderContainer container) {
    return Invert(
      enable: focused,
      faint: faint,
      child: Border(
        border: const BorderData.symmetric(horizontal: 1),
        child: child,
      ),
    );
  }
}
