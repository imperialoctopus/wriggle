import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import '../stateful_component.dart';
import 'flex_component.dart';
import 'flexible.dart';
import 'selector_flex_component.dart';

/// Shows one of a number of pages, and can switch between them.
class PageView extends StatefulComponent<int?> {
  /// Index of page to start on.
  final int startingPage;

  /// List of pages.
  final List<Renderable> pages;

  /// Header to display above the page.
  final Renderable Function(int? page)? header;

  /// Extra usage info for this component's controls.
  final UsageInfo Function(ProviderContainer container)? extraUsageInfo;

  /// Adds extra controls.
  final bool Function(
    ProviderContainer container,
    KeyInput key,
    int Function(ProviderContainer) getState,
    void Function(
      ProviderContainer container,
      int? Function(int) update,
    ) setState,
    int length,
  )? control;

  /// Adds extra controls before consulting the child.
  final bool controlOverrideChild;

  /// Shows one of a number of pages, and can switch between them.
  const PageView({
    required super.key,
    required this.pages,
    this.header,
    UsageInfo Function(ProviderContainer container)? usageInfo,
    this.control,
    this.controlOverrideChild = false,
    this.startingPage = 0,
    super.dependency,
  }) : extraUsageInfo = usageInfo;

  @override
  Renderable build(ProviderContainer container) {
    final state = getState(container);
    return Column(
      children: [
        if (header != null) SkipSelect(header!(state)),
        Flexible(
          child: pages[state],
        ),
      ],
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (!controlOverrideChild && super.keyPressed(container, key)) {
      return true;
    }

    if (control != null) {
      if (control!(container, key, getState, setState, pages.length)) {
        return true;
      }
    }

    if (controlOverrideChild && super.keyPressed(container, key)) {
      return true;
    }

    return false;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      (extraUsageInfo?.call(container) ?? const []) +
      pages[getState(container)].getUsageInfo(container);

  @override
  int getState(ProviderContainer container) =>
      (super.getState(container) ?? startingPage).clamp(0, pages.length - 1);

  @override
  void setState(ProviderContainer container, int? Function(int) update) =>
      super.setState(container, (_) => update(getState(container)));

  @override
  StateProvider<int?> get stateProvider => provider(key);

  /// Get this component's internal state provider.
  static final provider =
      StateProvider.family<int?, String?>((ref, key) => null);

  /// Read this component's internal state.
  static int read(
    ProviderContainer container,
    String? key, [
    int startingState = 0,
  ]) {
    final state = container.read(provider(key));
    return state ?? startingState;
  }
}
