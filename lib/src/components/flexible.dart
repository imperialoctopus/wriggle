import '../component.dart';
import '../renderable.dart';

/// Component that expands within Flex layouts.
class Flexible extends Component {
  /// Child to draw.
  final Renderable child;

  /// Proportion of flexible space in the parent to take up. Must be positive and non-zero.
  final int flex;

  /// Component that expands within Flex layouts.
  const Flexible({
    this.flex = 1,
    required this.child,
  });

  // @override
  // int? getSize(ProviderContainer container, Axis axis, int crossSize) => null;

  @override
  Renderable build(ProviderContainer container) => child;
}
