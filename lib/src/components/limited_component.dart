import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Layout component to limit its child to a certain size range.
class Limited extends Renderable {
  /// Minimum allowed width.
  final int minimumWidth;

  /// Maximum allowed width.
  final int maximumWidth;

  /// Minimum allowed height.
  final int minimumHeight;

  /// Maximum allowed height.
  final int maximumHeight;

  /// Child to size.
  final Renderable child;

  /// Layout component to limit its child to a certain size range.
  const Limited({
    required this.child,
    this.minimumWidth = 0,
    this.maximumWidth = 10000,
    this.minimumHeight = 0,
    this.maximumHeight = 10000,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    return switch (axis) {
      Axis.horizontal => child
              .getSize(container, axis, crossSize)
              ?.clamp(minimumWidth, maximumWidth) ??
          maximumWidth,
      Axis.vertical => child
              .getSize(container, axis, crossSize)
              ?.clamp(minimumHeight, maximumHeight) ??
          maximumHeight,
    };
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    return child.render(container, width, height);
  }
}
