import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Renders child at its full size and then clips it by [offset] lines/columns.
class Offset extends Renderable {
  /// Child to offset.
  final Renderable child;

  /// Axis to clip in.
  final Axis axis;

  /// Columns/rows to remove from start of child.
  final int offset;

  /// If false, operates in reverse (removing lines from the end of the child).
  final bool start;

  /// Renders child at its full size and then clips it by [offset] lines/columns.
  const Offset({
    required this.child,
    this.axis = Axis.vertical,
    required this.offset,
    this.start = true,
  });

  // @override
  // int? getSize(ProviderContainer container, Axis axis, int crossSize) {
  //   if (axis == this.axis) {
  //     return null;
  //   } else {
  //     return child.getSize(container, axis, crossSize);
  //   }
  // }

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      child.getSize(container, axis, crossSize);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    switch (axis) {
      case Axis.vertical:
        final childHeight =
            child.getSize(container, Axis.vertical, width) ?? height;
        final renderedChild = child.render(container, width, childHeight);
        renderedChild.removeRows(offset, start: start);
        renderedChild.resize(width, height, verticalStart: !start);
        return renderedChild;
      case Axis.horizontal:
        final childWidth =
            child.getSize(container, Axis.horizontal, height) ?? width;
        final renderedChild = child.render(container, childWidth, height);
        renderedChild.removeColumns(offset, start: start);
        return renderedChild..resize(width, height, horizontalStart: !start);
    }
  }
}
