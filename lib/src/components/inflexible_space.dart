import '../model/axel.dart';
import '../renderable.dart';
import 'filled_component.dart';
import 'inflexible.dart';

/// Space which takes up [size] space within a flexible layout.
class InflexibleSpace extends Inflexible {
  /// Axel to draw as fill.
  final Axel fill;

  /// Space which takes up [size] space within a flexible layout.
  const InflexibleSpace(
    int size, {
    this.fill = Axel.space,
  }) : super(size: size, child: const Filled(fill: Axel.space));

  @override
  Renderable build(ProviderContainer container) => Filled(fill: fill);
}
