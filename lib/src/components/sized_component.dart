import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Layout component to force its child to a certain size.
class Sized extends Renderable {
  /// Forced width; defaults to child size.
  final int? width;

  /// Forced height; defaults to child size.
  final int? height;

  /// Child to size.
  final Renderable child;

  /// Layout component to force its child to a certain size.
  const Sized({this.width, this.height, required this.child});

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    return switch (axis) {
      Axis.horizontal =>
        width ?? child.getSize(container, axis, height ?? crossSize),
      Axis.vertical =>
        height ?? child.getSize(container, axis, width ?? crossSize),
    };
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    return child.render(container, width, height);
  }
}
