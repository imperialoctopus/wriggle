import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Draws a list of Renderables on top of each other, each taking up the whole space.
/// Start of the list is the bottom of the stack.
/// Axel.space in higher items is ignored- use Axel.opaqueSpace to overwrite lower Axels.
class Stack extends Renderable {
  /// List of children to stack.
  final List<Renderable> children;

  /// Draws a list of Renderables on top of each other, each taking up the whole space.
  /// Start of the list is the top of the stack.
  /// Axel.space in higher items is ignored- use Axel.opaqueSpace to overwrite lower Axels.
  const Stack({required this.children});

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) => null;

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      children.first.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      children.first.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    assert(children.isNotEmpty);

    final renderedChildren = children.reversed
        .map((child) => child.render(container, width, height).data);

    return AxelMap(
      renderedChildren.reduce((bottom, top) {
        return [
          for (int y = 0; y < bottom.length; y++)
            [
              for (int x = 0; x < bottom[y].length; x++)
                (top[y][x] == Axel.space) ? bottom[y][x] : top[y][x],
            ],
        ];
        // return bottom.mapIndexed((rowIndex, row) {
        //   return row.mapIndexed((colIndex, baseAxel) {
        //     return (top[rowIndex][colIndex] == Axel.space)
        //         ? baseAxel
        //         : top[rowIndex][colIndex];
        //   }).toList();
        // }).toList();
      }),
    );

    // for (int y = 0; y < height; y++) {
    //   for (int x = 0; x < width; x++) {
    //     for (final child in renderedChildren) {
    //       final axel = child.getPosition(x, y)!;
    //       if (axel != Axel.space) {
    //         result.setPosition(x, y, axel);
    //         break;
    //       }
    //     }
    //   }
    // }
    // return result;
  }
}
