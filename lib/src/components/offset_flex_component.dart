import 'dart:math';

import 'package:collection/collection.dart';

import '../../components.dart';
import '../component.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../renderable.dart';

/// Parent class for offset row and column.
class OffsetFlexComponent extends Component {
  /// Child objects.
  final List<Renderable> children;

  /// Index of child to position around.
  final int alignTo;

  /// Whether aligned child should be placed at the top or bottom.
  final bool alignToStart;

  /// Flex axis.
  final Axis axis;

  /// Flex component alignment to pass through.
  final FlexAlignment alignment;

  /// Alignment perpendicular to the axis direction.
  final CrossAxisAlignment crossAxisAlignment;

  /// Expands to fill cross space if false.
  final bool crossAxisTight;

  /// Which child to pass keys to.
  final int? passthrough;

  /// Parent class for offset row and column.
  const OffsetFlexComponent({
    required this.children,
    required this.alignTo,
    required this.alignToStart,
    required this.axis,
    required this.alignment,
    required this.crossAxisAlignment,
    required this.crossAxisTight,
    required this.passthrough,
  });

  @override
  Renderable build(ProviderContainer container) {
    //return children[passthrough ?? 0];
    return FlexComponent(
      children: children,
      axis: axis,
      alignment: alignment,
      crossAxisAlignment: crossAxisAlignment,
      crossAxisTight: crossAxisTight,
      passthrough: passthrough,
    );
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final mainAxisSize = switch (axis) {
      Axis.vertical => height,
      Axis.horizontal => width,
    };
    final crossAxisSize = switch (axis) {
      Axis.vertical => width,
      Axis.horizontal => height,
    };

    final childrenToShow = alignToStart
        ? children.skip(alignTo).toList()
        : children.take(alignTo + 1).toList();
    final totalChildSize = childrenToShow
        .map((e) => e.getSize(container, axis, crossAxisSize) ?? 0)
        .sum;

    final renderedChild = FlexComponent(
      children: childrenToShow,
      axis: axis,
      alignment: totalChildSize > height ? FlexAlignment.tight : alignment,
      crossAxisAlignment: crossAxisAlignment,
      crossAxisTight: crossAxisTight,
      passthrough: passthrough,
    ).render(
      container,
      switch (axis) {
        Axis.vertical => crossAxisSize,
        Axis.horizontal => max(totalChildSize, mainAxisSize),
      },
      switch (axis) {
        Axis.vertical => max(totalChildSize, mainAxisSize),
        Axis.horizontal => crossAxisSize,
      },
    );
    renderedChild.resize(
      width,
      height,
      horizontalStart: alignToStart,
      verticalStart: alignToStart,
    );
    return renderedChild;
  }
}

/// Row component that skips some children, anchoring a child to either the start or end.
class OffsetRow extends OffsetFlexComponent {
  /// Row component that skips some children, anchoring a child to either the start or end.
  const OffsetRow({
    required super.children,
    required super.alignTo,
    super.alignToStart = true,
    super.alignment = FlexAlignment.start,
    super.crossAxisAlignment = CrossAxisAlignment.stretch,
    super.crossAxisTight = false,
    super.passthrough,
  }) : super(axis: Axis.vertical);
}

/// Column component that skips some children, anchoring a child to either the start or end.
class OffsetColumn extends OffsetFlexComponent {
  /// Column component that skips some children, anchoring a child to either the start or end.
  const OffsetColumn({
    required super.children,
    required super.alignTo,
    super.alignToStart = true,
    super.alignment = FlexAlignment.start,
    super.crossAxisAlignment = CrossAxisAlignment.stretch,
    super.crossAxisTight = false,
    super.passthrough,
  }) : super(axis: Axis.horizontal);
}
