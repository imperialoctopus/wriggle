import 'dart:math';

import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Fit method for AspectRatio if it's forced to render with an incorrect aspect ratio.
enum AspectRatioFit {
  /// Stretch child to size given, breaking aspect ratio.
  stretch,

  /// Renders the child with the correct aspect ratio fitting the larger axis, and crops to fit the smaller.
  cover,

  /// Renders the child with the correct aspect ratio fitting the smaller axis, and centre-floats it to fit the larger.
  float,
}

/// In a flexible layout forces the child to have the specified aspect ratio.
class AspectRatio extends Renderable {
  /// Aspect ratio to use: (height / width).
  final double ratio;

  /// Child Renderable.
  final Renderable child;

  /// Fit method if given non-conforming sizes.
  final AspectRatioFit fit;

  /// In a flexible layout forces the child to have the specified aspect ratio.
  const AspectRatio({
    required this.ratio,
    required this.child,
    this.fit = AspectRatioFit.stretch,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    switch (axis) {
      case Axis.vertical:
        return (crossSize * ratio).round();
      case Axis.horizontal:
        return (crossSize / ratio).round();
    }
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    switch (fit) {
      case AspectRatioFit.stretch:
        return child.render(container, width, height);
      case AspectRatioFit.cover:
        final childHeight = max(height, (width * ratio).floor());
        final childWidth = max(width, (height / ratio).floor());
        return child.render(container, childWidth, childHeight)
          ..resizeCentered(width, height);
      case AspectRatioFit.float:
        final childHeight = min(height, (width * ratio).ceil());
        final childWidth = min(width, (height / ratio).ceil());
        return child.render(container, childWidth, childHeight)
          ..resizeCentered(width, height);
    }
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);
}
