import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Draws a single axel, reporting 1 for its size.
class SingleAxel extends Renderable {
  /// Axel to draw.
  final Axel axel;

  /// Draws a single axel, reporting 1 for its size.
  const SingleAxel(this.axel);

  @override
  AxelMap render(ProviderContainer container, int width, int height) => AxelMap(
        [
          [axel],
        ],
      )..resize(width, height);

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) => 1;

  @override
  UsageInfo getUsageInfo(ProviderContainer container) => const [];

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) => false;
}
