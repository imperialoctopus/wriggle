import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';
import '../stateful_component.dart';
import 'divider_component.dart';
import 'flex_component.dart';
import 'flexible.dart';
import 'usage_panel_component.dart';

/// Container which shows/hides a usage panel when a specified key is pressed.
class UsagePanelContainer extends StatefulComponent<bool> {
  /// Key to open and close the panel.
  final KeyInput toggle;

  /// Extra usage info to add (e.g. key to open/close panel).
  final UsageInfo? additionalUsageInfo;

  /// Whether to open panel by default.
  final bool startOpen;

  /// Show a horizontal divider between the child and the panel when open.
  final bool showDivider;

  /// Component to wrap. Uses child's usage info by default.
  final Renderable child;

  /// If not null, displays usage data from this source instead of from the child.
  final Renderable? usageDataSourceOverride;

  /// Container which shows/hides a usage panel when a specified key is pressed.
  const UsagePanelContainer({
    this.toggle = KeyInput.function12,
    this.additionalUsageInfo,
    this.showDivider = true,
    this.startOpen = false,

    /// Displays usage data from this source instead of from the child.
    this.usageDataSourceOverride,
    required this.child,
  });

  // @override
  // int? getSize(_, Axis axis, int crossSize) => switch (axis) {
  //       Axis.horizontal => null,
  //       Axis.vertical => 4,
  //     };

  @override
  Renderable build(ProviderContainer container) {
    if (startOpen == getState(container)) {
      return child;
    }
    return Column(
      //alignment: FlexAlignment.spaceBetween,
      children: [
        Flexible(child: child),
        if (showDivider) const HorizontalDivider(),
        UsagePanel(
          usageInfo: _getDisplayedUsageInfo(container),
        ),
      ],
    );
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  UsageInfo _getDisplayedUsageInfo(ProviderContainer container) =>
      (additionalUsageInfo ?? []) +
      (usageDataSourceOverride ?? child).getUsageInfo(container);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    if (child.keyPressed(container, key)) {
      return true;
    }
    if (key == toggle) {
      setState(container, (state) => !state);
      return true;
    }
    return false;
  }

  @override
  StateProvider<bool> get stateProvider => _stateProvider;

  static final _stateProvider = StateProvider((ref) => false);
}
