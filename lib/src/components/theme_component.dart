import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/color/color.dart';
import '../model/key_input/key_input.dart';
import '../model/text_style.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Overrides the color, background, or textStyle of children.
class Theme extends Renderable {
  /// Child to theme.
  final Renderable child;

  /// Colour to apply to child.
  final ColorPlaceholder? color;

  /// Background colour to apply to child.
  final ColorPlaceholder? background;

  /// Text style to apply to child.
  final TextStyle? textStyle;

  /// Overrides the color, background, or textStyle of children.
  const Theme({
    this.color,
    this.background,
    this.textStyle,
    required this.child,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      child.getSize(container, axis, crossSize);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    return child.render(container, width, height)
      ..overrideStyle(
        color: color,
        background: background,
        textStyle: textStyle,
      );
  }
}
