import '../component.dart';
import '../model/axis.dart';
import '../renderable.dart';

/// Causes the Renderable to report a fixed size to its parent FlexComponent in its main axis.
/// Still passes the child's cross axis size through.
mixin InflexibleSized on Renderable {
  /// Size to force.
  int get size;
}

/// Component which provides an inflexible size for its child; reporting that size to parent FlexComponents.
class Inflexible extends Component with InflexibleSized {
  /// Child to size.
  final Renderable child;
  @override
  final int size;

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      child.getSize(container, axis, crossSize);

  /// Component which provides an inflexible size for its child; reporting that size to parent FlexComponents.
  const Inflexible({
    required this.size,
    required this.child,
  });

  @override
  Renderable build(ProviderContainer container) => child;
}
