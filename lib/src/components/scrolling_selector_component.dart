import 'package:collection/collection.dart';

import '../../components.dart';
import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../renderable.dart';
import '../stateful_component.dart';
import 'offset_flex_component.dart';

typedef _State = ({
  int selected,
  int anchor,
  bool anchorStart,
  int mainAxisSize,
  int crossAxisSize,
});

/// SelectorFlex which scrolls if the children would overflow.
class ScrollingSelector extends StatefulComponent<_State> {
  /// List of builders returning children (depending on [focused] for their decoration).
  /// Children should not change in size depending on [focused].
  final List<Renderable Function(bool focused)> children;

  /// FlexAlignment in its main axis.
  final FlexAlignment alignment;

  /// Alignment in cross axis.
  final CrossAxisAlignment crossAxisAlignment;

  /// Whether to tighten to cross axis.
  final bool crossAxisTight;

  /// Whether to pass [focused] to selected child.
  final bool focused;

  /// Main axis of the Flex.
  final Axis axis;

  /// Selected value for using external state control; if null, this component tracks its own selected child.
  final int? selected;

  /// Function for when selected child should change; if null, this component tracks its own selected child.
  final void Function(ProviderContainer container, int newSelected)?
      selectedChanged;

  /// SelectorFlex which scrolls if the children would overflow.
  const ScrollingSelector({
    required super.key,
    required this.children,
    required this.focused,
    this.axis = Axis.vertical,
    this.alignment = FlexAlignment.start,
    this.crossAxisAlignment = CrossAxisAlignment.stretch,
    this.crossAxisTight = false,
    //this.onNavigateOut,
    this.selected,
    this.selectedChanged,
  });

  /// Children which can be selected (i.e. not wrapped in SkipSelect).
  Iterable<Renderable Function(bool focused)> get selectableChildren =>
      children.where((e) => e(false) is! SkipSelect);

  @override
  Renderable build(ProviderContainer container) {
    final state = getState(container);
    final preparedSelectableChildren = selectableChildren.toList();

    if (children.isEmpty) {
      return const Filled(fill: Axel.space);
    }

    if (selected != null && selected != state.selected) {
      setState(
        container,
        (state) => _setSelected(container, state, selected!, false),
      );
      return const Filled(fill: Axel.space);
    }

    if (state.selected < 0 || state.selected >= selectableChildren.length) {
      setState(
        container,
        (state) =>
            _setSelected(container, state, selectableChildren.length - 1, true),
      );
      return const Filled(fill: Axel.space);
    }

    return OffsetFlexComponent(
      axis: axis,
      alignTo: state.anchor,
      alignToStart: state.anchorStart,
      passthrough: state.selected,
      alignment: alignment,
      crossAxisAlignment: crossAxisAlignment,
      crossAxisTight: crossAxisTight,
      children: children
          .map(
            (builder) => builder(
              focused && builder == preparedSelectableChildren[state.selected],
            ),
          )
          .map((e) => e is SkipSelect ? e.child : e)
          .toList(),
    );
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    final state = getState(container);
    if (selectableChildren.length <= state.selected) {
      return false;
    }
    if (selectableChildren
        .elementAt(state.selected)(true)
        .keyPressed(container, key)) {
      return true;
    }

    switch (key) {
      case KeyInput.arrowUp:
        final newSelected = state.selected - 1;
        if (newSelected < 0) {
          //onNavigateOut?.call(Direction.up);
          return false;
        }
        setState(
          container,
          (state) => _setSelected(container, state, newSelected, true),
        );
        return true;
      case KeyInput.arrowDown:
        final newSelected = state.selected + 1;
        if (newSelected >= selectableChildren.length) {
          //onNavigateOut?.call(Direction.down);
          return false;
        }
        setState(
          container,
          (state) => _setSelected(container, state, newSelected, true),
        );
        return true;
      default:
        return false;
    }
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final state = getState(container);
    final mainAxisSize = switch (axis) {
      Axis.vertical => height,
      Axis.horizontal => width,
    };
    final crossAxisSize = switch (axis) {
      Axis.vertical => width,
      Axis.horizontal => height,
    };

    if (state.crossAxisSize != crossAxisSize ||
        state.mainAxisSize != mainAxisSize) {
      setState(
        container,
        (state) => _setSize(container, state, mainAxisSize, crossAxisSize),
      );
      return const Filled(fill: Axel.space).render(container, width, height);
    }
    return super.render(container, width, height);
  }

  _State _setSelected(
    ProviderContainer container,
    _State state,
    int newValue,
    bool setExternalState,
  ) {
    if (setExternalState) selectedChanged?.call(container, newValue);
    return _checkScrollBounds(
      container,
      (
        selected: newValue,
        anchor: state.anchor,
        anchorStart: state.anchorStart,
        mainAxisSize: state.mainAxisSize,
        crossAxisSize: state.crossAxisSize,
      ),
    );
  }

  _State _setSize(
    ProviderContainer container,
    _State state,
    int mainAxisSize,
    int crossAxisSize,
  ) =>
      _checkScrollBounds(
        container,
        (
          selected: state.selected,
          anchor: state.anchor,
          anchorStart: state.anchorStart,
          mainAxisSize: mainAxisSize,
          crossAxisSize: crossAxisSize,
        ),
      );

  _State _checkScrollBounds(ProviderContainer container, _State state) {
    if (selectableChildren.isEmpty) {
      return (
        selected: 0,
        anchor: 0,
        anchorStart: true,
        mainAxisSize: state.mainAxisSize,
        crossAxisSize: state.crossAxisSize,
      );
    }
    if (state.selected >= selectableChildren.length) {
      return (
        selected: selectableChildren.length - 1,
        anchor: state.anchor,
        anchorStart: state.anchorStart,
        mainAxisSize: state.mainAxisSize,
        crossAxisSize: state.crossAxisSize,
      );
    }
    final selectedChildIndex =
        children.indexOf(selectableChildren.elementAt(state.selected));
    final mainAxisSizes = children
        .map((e) => e(false))
        .map((e) => e is SkipSelect ? e.child : e)
        .map((e) => e.getSize(container, axis, state.crossAxisSize) ?? 0)
        .toList();
    if (state.anchorStart) {
      if (selectedChildIndex <= state.anchor) {
        return (
          selected: state.selected,
          anchor: selectedChildIndex,
          anchorStart: true,
          mainAxisSize: state.mainAxisSize,
          crossAxisSize: state.crossAxisSize,
        );
      } else {
        final heightToSelected = mainAxisSizes
            .skip(state.anchor)
            .take(selectedChildIndex - state.anchor + 1)
            .sum;
        if (heightToSelected >= state.mainAxisSize) {
          return (
            selected: state.selected,
            anchor: selectedChildIndex,
            anchorStart: false,
            mainAxisSize: state.mainAxisSize,
            crossAxisSize: state.crossAxisSize,
          );
        } else {
          return state;
        }
      }
    } else {
      // anchor at bottom
      if (selectedChildIndex >= state.anchor) {
        return (
          selected: state.selected,
          anchor: selectedChildIndex,
          anchorStart: false,
          mainAxisSize: state.mainAxisSize,
          crossAxisSize: state.crossAxisSize,
        );
      } else {
        final selectedToBottom = mainAxisSizes
            .skip(selectedChildIndex)
            .take(state.anchor - selectedChildIndex + 1)
            .sum;
        if (selectedToBottom >= state.mainAxisSize) {
          return (
            selected: state.selected,
            anchor: selectedChildIndex,
            anchorStart: true,
            mainAxisSize: state.mainAxisSize,
            crossAxisSize: state.crossAxisSize,
          );
        } else {
          return state;
        }
      }
    }
  }

  @override
  StateProvider<_State> get stateProvider => provider(key);

  /// Get this component's internal state provider.
  static final provider = StateProvider.family<_State, String?>(
    (ref, _) => (
      selected: 0,
      anchor: 0,
      anchorStart: true,
      mainAxisSize: 0,
      crossAxisSize: 0,
    ),
  );

  /// Read this component's internal state.
  static int read(ProviderContainer container, String? key) {
    final state = container.read(provider(key)).selected;
    return state;
  }
}
