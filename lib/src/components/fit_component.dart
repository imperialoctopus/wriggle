import 'dart:math';

import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Method of fitting used by Fit component.
enum FitMethod {
  /// Forces child to fit parent size.
  stretch,

  /// Renders the child at its intrinsic size and floats it to the start of this axis.
  /// If child's intrinsic size is larger than this, use stretch instead.
  floatStart,

  /// Renders the child at its intrinsic size and floats it to the end of this axis.
  /// If child's intrinsic size is larger than this, use stretch instead.
  floatEnd,

  /// Reports child's intrinsic size.
  tight,
}

/// Layout component to fit the child independently in width and height.
class Fit extends Renderable {
  /// Child to fit.
  final Renderable child;

  /// Method of fitting in the vertical axis.
  final FitMethod verticalFit;

  /// Method of fitting in the horizontal axis.
  final FitMethod horizontalFit;

  /// Layout component to fit the child independently in width and height.
  const Fit({
    required this.child,
    this.verticalFit = FitMethod.stretch,
    this.horizontalFit = FitMethod.stretch,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) {
    if (switch (axis) {
          Axis.horizontal => horizontalFit,
          Axis.vertical => verticalFit,
        } ==
        FitMethod.tight) {
      return child.getSize(container, axis, crossSize);
    }
    return null;
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final childWidth = switch (horizontalFit) {
      FitMethod.tight || FitMethod.stretch => width,
      FitMethod.floatStart ||
      FitMethod.floatEnd =>
        child.getSize(container, Axis.horizontal, height) ?? width,
    };
    final childHeight = switch (verticalFit) {
      FitMethod.tight || FitMethod.stretch => height,
      FitMethod.floatStart ||
      FitMethod.floatEnd =>
        child.getSize(container, Axis.vertical, width) ?? height,
    };
    final renderedChild = child.render(
      container,
      min(childWidth, width),
      min(childHeight, height),
    );
    renderedChild.resize(
      width,
      height,
      horizontalStart: horizontalFit == FitMethod.floatEnd,
      verticalStart: verticalFit == FitMethod.floatEnd,
    );
    return renderedChild;
  }

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);
}
