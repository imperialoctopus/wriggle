import 'dart:math';

import 'package:characters/characters.dart';
import 'package:collection/collection.dart';

import '../../model/axel.dart';
import '../../model/axel_map.dart';
import '../../model/axis.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../stateful_component.dart';
import '../text_component.dart';

/// Text-containing cell for the ScrollingCells component
class TextCell extends StatefulComponent<int> {
  /// Index of this item.
  final int index;

  /// Content of this cell.
  final String content;

  /// Update content.
  final void Function(int index, String content) contentChanged;

  /// Whether this cell is focused.
  final bool focused;

  /// Whether this cell is being edited.
  final bool editing;

  /// Text-containing cell for the ScrollingCells component
  const TextCell({
    required super.key,
    required this.index,
    required this.content,
    required this.contentChanged,
    required this.focused,
    required this.editing,
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      switch (axis) {
        Axis.horizontal => null,
        Axis.vertical => 1,
      };

  @override
  Renderable build(ProviderContainer container) => Text(content);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final state = getState(container);
    if (state > content.length) {
      setState(
        container,
        (state) => content.length,
      );
    }
    if (!focused && content.isEmpty) {
      return AxelMap([
        [
          const Axel('-', textStyle: TextStyle(faint: true)),
        ],
      ])
        ..resize(width, height);
    }
    return AxelMap(
      [
        [
          ...content.characters.takeLast(width - 1),
          ' ',
          if (content.length < width - 1)
            for (int i = 0; i < width - content.length; i++) ' ',
        ].mapIndexed(
          (i, c) => Axel(
            c,
            textStyle: focused &&
                    (!editing ||
                        (i + max(0, content.length - width + 1)) == state)
                ? const TextStyle(inverted: true)
                : const TextStyle(),
          ),
        ),
      ],
    )..resize(width, height);
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    final state = getState(container);

    if (!editing) {
      // Not editing
      switch (key) {
        case KeyInput.backspace:
        case KeyInput.delete:
          // Delete cell
          contentChanged(index, '');
          return true;
        default:
          return false;
      }
    }
    // Editing
    switch (key) {
      case KeyInput.backspace:
        // backspace
        if (state > 0) {
          contentChanged(
            index,
            content.replaceRange(state - 1, state, ''),
          );
        }
        return true;
      case KeyInput.delete:
        // delete
        if (state < content.length) {
          contentChanged(
            index,
            content.replaceRange(state, state + 1, ''),
          );
        }
        return true;
      case KeyInput.arrowLeft:
        // left
        setState(
          container,
          (state) => max(state - 1, 0),
        );
        return true;
      case KeyInput.arrowRight:
        // right
        setState(
          container,
          (state) => min(state + 1, content.length),
        );
        return true;
      default:
        if (key.isPrintable) {
          contentChanged(
            index,
            content.replaceRange(state, state, key.string),
          );
          setState(
            container,
            (state) => min(
              state + key.string.length,
              content.length + key.string.length,
            ),
          );
          return true;
        }
        return false;
    }
  }

  @override
  StateProvider<int> get stateProvider => _stateProvider((key, index, editing));

  static final _stateProvider =
      StateProvider.family<int, (String?, int, bool)>((ref, arg) => 0);
}
