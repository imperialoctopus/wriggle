import 'dart:math';

import 'package:collection/collection.dart';

import '../../model/axel.dart';
import '../../model/axel_map.dart';
import '../../model/border_data.dart';
import '../../model/key_input/key_input.dart';
import '../../model/text_style.dart';
import '../../renderable.dart';
import '../../stateful_component.dart';
import '../border_component.dart';
import '../flex_component.dart';
import '../offset_component.dart';
import '../text_component.dart';

typedef _State = ({int selected, bool editing, int height});

/// Function to build cells.
typedef CellBuilder<T> = Renderable Function({
  String? key,
  required int index,
  required T data,
  required void Function(ProviderContainer, int, T) dataChanged,
  required bool focused,
  required bool editing,
  required void Function(bool) setEditing,
  required void Function(int) nextRow,
  required void Function(int) nextCell,
});

/// Sheet of rows of editable cells that scroll with the selection.
class ScrollingCells<T> extends StatefulComponent<_State> {
  /// Data to display.
  final List<T> data;

  /// Builder for the cells. Consider returning TextCell components as a default.
  final CellBuilder<T> builder;

  /// Function for data changing. [index] is the index of the changed item in the data list, and [newValue] is the value it has changed to.
  final void Function(ProviderContainer container, int index, T newValue)
      dataChanged;

  /// Function for switching the contents of two cells. Separate from the [dataChanged] function to allow more efficient switching with Alt+arrow keys.
  final void Function(ProviderContainer container, int first, int second)
      switchCells;

  /// Shows highlight on selected cell.
  final bool focused;

  /// Number of rows.
  final int verticalSize;

  /// Number of cells in each row.
  final int horizontalSize;

  /// Whether to show line numbers to the left of the grid.
  final bool lineNumbers;

  /// Sheet of rows of editable cells that scroll with the selection.
  const ScrollingCells({
    required super.key,
    required this.data,
    required this.builder,
    required this.dataChanged,
    required this.switchCells,
    required this.focused,
    required this.verticalSize,
    required this.horizontalSize,
    this.lineNumbers = false,
    //this.onNavigateOut,
  }) : assert(data.length == verticalSize * horizontalSize);

  @override
  Renderable build(ProviderContainer container) {
    final state = getState(container);
    //
    if (state.selected >= data.length) {
      setState(
        container,
        (state) =>
            (selected: data.length - 1, editing: false, height: state.height),
      );
    }
    final offset =
        max(0, (state.selected / horizontalSize).floor() - state.height + 1);
    return Offset(
      offset: offset,
      start: offset > 0,
      child: Column(
        passthrough: (state.selected / horizontalSize).floor(),
        //alignment: FlexAlignment.tight,
        alignment: FlexAlignment.tight,
        children: data
            //.skip(state.scrollOffset * horizontalSize)
            .slices(horizontalSize)
            .mapIndexed(
              (y, rowContent) => Row(
                passthrough: state.selected % horizontalSize,
                crossAxisTight: true,
                children: [
                  ...rowContent.mapIndexed(
                    (x, content) => Border(
                      border: BorderData(
                        left: x != 0 ? 1 : 0,
                        fill:
                            const Axel('│', textStyle: TextStyle(faint: true)),
                      ),
                      child: builder(
                        key: key,
                        index: (y * horizontalSize) + x,
                        data: content,
                        dataChanged: dataChanged,
                        focused: focused &&
                            (y * horizontalSize) + x == state.selected,
                        editing: state.editing,
                        setEditing: (ed) => setState(
                          container,
                          (state) => (
                            editing: ed,
                            height: state.height,
                            selected: state.selected
                          ),
                        ),
                        nextRow: (change) {
                          setState(
                            container,
                            (state) => (
                              selected: min(
                                data.length - 1,
                                state.selected + (change * horizontalSize),
                              ),
                              editing: false,
                              height: state.height
                            ),
                          );
                        },
                        nextCell: (change) {
                          setState(
                            container,
                            (state) => (
                              selected:
                                  min(data.length - 1, state.selected + change),
                              editing: false,
                              height: state.height
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  if (lineNumbers)
                    Text(
                      '${y + 1}'.padLeft(2, '0'),
                      style: const TextStyle(faint: true),
                    ),
                ],
              ),
            )
            //.take(state.height)
            //.skip(offset)
            .toList(),
      ),
    );
  }

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    final recordedHeight = getState(container).height;
    if (height != recordedHeight) {
      setState(
        container,
        (state) =>
            (selected: state.selected, editing: state.editing, height: height),
      );
    }
    return super.render(container, width, height);
  }

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) {
    final state = getState(container);

    // if (children[state.selected](true).keyPressed(container, key)) {
    //   return true;
    // }
    if (build(container).keyPressed(container, key)) {
      return true;
    }

    switch (key) {
      case KeyInput.arrowUp:
        final newSelected = state.selected - horizontalSize;
        if (newSelected < 0) {
          //onNavigateOut?.call(Direction.up);
          return false;
        }
        setState(
          container,
          (state) =>
              (selected: newSelected, editing: false, height: state.height),
        );
        // if ((newSelected / horizontalSize).floor() < state.scrollOffset) {
        //   _setScrollOffset(container, newSelected);
        // }
        return true;
      case KeyInput.arrowDown:
        final newSelected = state.selected + horizontalSize;
        if (newSelected >= data.length) {
          //onNavigateOut?.call(Direction.down);
          return false;
        }
        setState(
          container,
          (state) =>
              (selected: newSelected, editing: false, height: state.height),
        );
        // if (newSelected >= state.scrollOffset + state.height) {
        //   _setScrollOffset(container, newSelected - state.height + 1);
        // }
        return true;
      case KeyInput.arrowRight:
        final newSelected = state.selected + 1;
        if (newSelected % horizontalSize == 0) {
          //onNavigateOut?.call(Direction.down);
          return false;
        }
        setState(
          container,
          (state) =>
              (selected: newSelected, editing: false, height: state.height),
        );
        // if (newSelected >= state.scrollOffset + state.height) {
        //   _setScrollOffset(container, newSelected - state.height + 1);
        // }
        return true;
      case KeyInput.arrowLeft:
        if (state.selected % horizontalSize == 0) {
          //onNavigateOut?.call(Direction.down);
          return false;
        }
        setState(
          container,
          (state) => (
            selected: state.selected - 1,
            editing: false,
            height: state.height
          ),
        );
        // if (newSelected >= state.scrollOffset + state.height) {
        //   _setScrollOffset(container, newSelected - state.height + 1);
        // }
        return true;
      case KeyInput.altArrowLeft:
        if (state.selected % horizontalSize == 0) {
          return false;
        }
        switchCells(container, state.selected, state.selected - 1);
        setState(
          container,
          (state) => (
            selected: state.selected - 1,
            editing: false,
            height: state.height
          ),
        );
        return true;
      case KeyInput.altArrowRight:
        final otherIndex = state.selected + 1;
        if (otherIndex % horizontalSize == 0) {
          return false;
        }
        switchCells(container, otherIndex, otherIndex - 1);
        setState(
          container,
          (state) => (
            selected: state.selected + 1,
            editing: false,
            height: state.height
          ),
        );
        return true;
      case KeyInput.altArrowUp:
        if (state.selected / horizontalSize < 1) {
          return false;
        }
        switchCells(
          container,
          state.selected,
          state.selected - horizontalSize,
        );
        setState(
          container,
          (state) => (
            selected: state.selected - horizontalSize,
            editing: false,
            height: state.height
          ),
        );
        return true;
      case KeyInput.altArrowDown:
        if (state.selected + horizontalSize >= data.length) {
          return false;
        }
        switchCells(
          container,
          state.selected,
          state.selected + horizontalSize,
        );
        setState(
          container,
          (state) => (
            selected: state.selected + horizontalSize,
            editing: false,
            height: state.height
          ),
        );
        return true;
      default:
        return false;
    }
  }

  @override
  StateProvider<_State> get stateProvider => _stateProvider(key);

  static final _stateProvider = StateProvider.family<_State, String?>(
    (ref, arg) => (selected: 0, height: 0, editing: false),
  );

  /// Reads the selected cell index.
  static int read(ProviderContainer container, String? key) {
    return container.read(_stateProvider(key)).selected;
  }
}
