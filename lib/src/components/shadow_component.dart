import '../model/axel.dart';
import '../model/axel_map.dart';
import '../model/axis.dart';
import '../model/key_input/key_input.dart';
import '../model/usage_info.dart';
import '../renderable.dart';

/// Surrounds the child on the right and bottom with a dark shadow.
class Shadow extends Renderable {
  /// Child.
  final Renderable child;

  /// Axel to draw as the shadow.
  final Axel shadow;

  /// Surrounds the child on the right and bottom with a dark shadow.
  const Shadow({
    required this.child,
    this.shadow = const Axel('▒'),
  });

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) => null;

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      child.keyPressed(container, key);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      child.getUsageInfo(container);

  @override
  AxelMap render(ProviderContainer container, int width, int height) {
    // If there isn't enough space for the border AND the child, just draw the child.
    if (width <= 1 || height <= 1) {
      return child.render(container, width, height);
    }

    final renderedChild = child.render(
      container,
      width - 1,
      height - 1,
    );

    // Right
    renderedChild.addColumn(
      [
        Axel.space,
        ...List.filled(renderedChild.height - 1, shadow),
      ],
    );

    // Bottom
    renderedChild.addRow(
      [
        Axel.space,
        ...List.filled(renderedChild.width - 1, shadow),
      ],
    );

    return renderedChild;
  }
}
