import 'package:freezed_annotation/freezed_annotation.dart';

import 'component.dart';
import 'renderable.dart';

/// Base class for providers which can be used as a stateful component dependency.
typedef ProviderDependency = ProviderListenable<Object?>;

/// Base class for stateful components. Exposes get and set state methods.
@immutable
abstract class StatefulComponent<T> extends Component {
  /// Key used to differentiate the states of different instances of this component.
  final String? key;

  /// Provider dependency; when it changes this component's state is invalidated.
  final ProviderDependency? dependency;

  /// Reference to this class's state provider. Provide a static final StateProvider of the correct type, using [key] if relevant.
  StateProvider<T> get stateProvider;

  /// Base class for stateful components. Exposes get and set state methods.
  const StatefulComponent({this.key, this.dependency});

  /// Update this component's state.
  void setState(ProviderContainer container, T Function(T) update) {
    final provider = container.read(stateProvider.notifier);
    provider.state = update(provider.state);
  }

  /// Read the current state of this component.
  T getState(ProviderContainer container) {
    dependency?.addListener(
      container,
      (_, __) => container.invalidate(stateProvider),
      onError: (_, __) {},
      fireImmediately: false,
      onDependencyMayHaveChanged: () {},
    );
    return container.read(stateProvider);
  }
}
