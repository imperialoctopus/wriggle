import 'package:riverpod/riverpod.dart';

import 'animated.dart';

/// Custom ProviderObserver for the Wriggle engine.
class EngineProviderObserver extends ProviderObserver {
  /// Function called when the container's state changes.
  final void Function() onUpdate;

  /// Custom ProviderObserver for the Wriggle engine.
  const EngineProviderObserver({required this.onUpdate});

  @override
  void didUpdateProvider(
    ProviderBase<Object?> provider,
    Object? previousValue,
    Object? newValue,
    ProviderContainer container,
  ) {
    if (provider == Animated.baseline ||
        provider == Animated.animationFramesWaiting) {
      return;
    }

    if (provider == Animated.animationRebuild) {
      //throw "Banana";
    }

    onUpdate();
  }
}
