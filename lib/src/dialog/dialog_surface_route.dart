import 'dart:async';

import 'package:riverpod/riverpod.dart';

import '../component.dart';

/// Representation of a route for the dialog surface.
class DialogSurfaceRoute<T extends Object?> {
  final FutureOr<void> Function(ProviderContainer, T) _pop;

  /// Application.
  final Component child;

  /// Representation of a route for the dialog surface.
  const DialogSurfaceRoute({
    required FutureOr<void> Function(ProviderContainer, T) pop,
    required this.child,
  }) : _pop = pop;

  /// Call when this route should be removed to run its onPop function.
  FutureOr<void> pop(ProviderContainer container, Object? result) async {
    if (result is T) {
      return _pop(container, result);
    } else {
      throw ArgumentError(
        "Route popped with invalid result type: check you've created the route using the same type parameter as the route returns.",
      );
    }
  }
}
