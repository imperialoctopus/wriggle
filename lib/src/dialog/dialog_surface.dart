import '../component.dart';
import '../components/connected_border/connected_border_component.dart';
import '../components/stack_component.dart';
import '../context.dart';
import '../renderable.dart';

/// Facilitates popup dialogs. Used internally (not for public use).
class DialogSurface extends Component {
  /// Child to display dialogs over.
  final Renderable child;

  /// Type of connected border to use for dialogs.
  final ConnectedBorderData connectedBorder;

  /// Facilitates popup dialogs. Used internally (not for public use).
  const DialogSurface({
    required this.child,
    this.connectedBorder = const ThinConnectedBorderData(),
  });

  @override
  Renderable build(ProviderContainer container) {
    final dialogs = container.read(Wriggle.dialog.state);
    if (dialogs.isEmpty) {
      return child;
    }
    return Stack(
      children: [
        ConnectedBorderComponent(
          borderData: connectedBorder,
          child: dialogs.last.child,
        ),
        child,
      ],
    );
  }
}
