import 'dart:async';

import 'package:riverpod/riverpod.dart';

import '../route_contract.dart';
import 'dialog_surface_route.dart';

/// Adapter for adding and removing dialogs.
class DialogAdapter {
  const DialogAdapter._();

  /// Push a dialog onto the screen. Displays over the child component.
  void push<T>(
    ProviderContainer container,
    RouteContract<T> child, [
    FutureOr<void> Function(ProviderContainer container, T result)? onPop,
  ]) {
    container.read(state.notifier).update(
          (state) => [
            ...state,
            DialogSurfaceRoute<T>(
              pop: onPop ?? (_, __) {},
              child: child,
            ),
          ],
        );
  }

  /// Closes the current dialog route, returning [result].
  /// If [result] is null, returns nothing.
  Future<void> pop<T>(ProviderContainer container, [T? result]) async {
    final dialogs = container.read(state);

    if (dialogs.isEmpty) {
      return;
    }
    final dialog = dialogs.last;

    container
        .read(state.notifier)
        .update((state) => state.take(state.length - 1).toList());

    if (result != null) {
      await dialog.pop(container, result);
    }
  }

  /// Public provider that can be used to read the current state of this dialog surface.
  StateProvider<List<DialogSurfaceRoute<Object?>>> get state => _state;

  static final StateProvider<List<DialogSurfaceRoute<Object?>>> _state =
      StateProvider<List<DialogSurfaceRoute<Object?>>>((_) => []);
}

/// Gets dialog adapter.
DialogAdapter dialogAdapter = const DialogAdapter._();
