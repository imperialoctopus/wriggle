import 'package:freezed_annotation/freezed_annotation.dart';

import 'model/axel_map.dart';
import 'model/axis.dart';
import 'model/key_input/key_input.dart';
import 'model/usage_info.dart';
import 'renderable.dart';

/// Base class for Components. Exposes a build method which returns Renderables instead of directly creating an AxelMap.
@immutable
abstract class Component extends Renderable {
  ///
  const Component();

  /// Return a Renderable which is to be displayed.
  Renderable build(ProviderContainer container);

  @override
  int? getSize(ProviderContainer container, Axis axis, int crossSize) =>
      build(container).getSize(container, axis, crossSize);

  @override
  AxelMap render(ProviderContainer container, int width, int height) =>
      build(container).render(container, width, height)..resize(width, height);

  @override
  UsageInfo getUsageInfo(ProviderContainer container) =>
      build(container).getUsageInfo(container);

  @override
  bool keyPressed(ProviderContainer container, KeyInput key) =>
      build(container).keyPressed(container, key);
}
