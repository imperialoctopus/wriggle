import 'dart:io';

import 'package:dart_synthizer/dart_synthizer.dart';

/// Audio adapter.
final class AudioAdapter {
  Synthizer? _synthizer;

  late final Context _context;
  late final DirectSource _musicSource;
  late final DirectSource _effectSource;
  Generator? _musicGenerator;

  AudioAdapter._();

  void _init() {
    if (_synthizer == null) {
      try {
        _synthizer = Synthizer(filename: 'lib/synthizer.dll')
          ..initialize(loggingBackend: LoggingBackend.none);
        _context = _synthizer!.createContext();
        _musicSource = _context.createDirectSource();
        _effectSource = _context.createDirectSource();
      } catch (e) {
        _synthizer = null;
      }
    }
  }

  /// Plays file [filename] in /audio/ directory.
  Future<void> playOnce(String filename) async {
    if (_synthizer == null) {
      return;
    }
    final file = File('audio/$filename');
    if (!await file.exists()) {
      return;
    }

    final generator = _context.createBufferGenerator(
      buffer: Buffer.fromFile(_synthizer!, file),
    );
    _effectSource.addGenerator(generator);
  }

  /// Plays file [filename] in /audio/ directory. Replaces previous music track.
  Future<void> playMusic(String filename, {bool loop = true}) async {
    if (_synthizer == null) {
      return;
    }
    final file = File('audio/$filename');
    if (!await file.exists()) {
      return;
    }

    final generator = _context.createBufferGenerator(
      buffer: Buffer.fromFile(_synthizer!, file),
    )..looping.value = loop;
    _musicSource.addGenerator(generator);
    if (_musicGenerator != null) {
      _musicSource.removeGenerator(_musicGenerator!);
    }
    _musicGenerator = generator;
  }
}

/// Gets audio adapter.
AudioAdapter audioAdapter = AudioAdapter._();

/// Visible for internal use (in Wriggle engine) only.
void initialiseAudioAdapter() => audioAdapter._init();
