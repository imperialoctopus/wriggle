import 'package:win32_clipboard/win32_clipboard.dart';

/// Gets the content of the clipboard.
Future<String> nativeGetClipboard() async {
  return Clipboard.getText() ?? '';
}

/// Sets the content of the clipboard.
Future<bool> nativeSetClipboard(String value) async {
  return Clipboard.setText(value);
}
