import 'native_api.dart';

/// Clipboard adapter.
class ClipboardAdapter {
  const ClipboardAdapter._();

  /// Get contents of clipboard.
  Future<String> read() async {
    try {
      final value = (await nativeGetClipboard()).cleanLineBreaks();
      return value;
    } catch (e) {
      return '';
    }
  }

  /// Sets clipboard.
  Future<bool> write(String value) async {
    try {
      final error = await nativeSetClipboard(value);
      return error;
    } catch (e) {
      return false;
    }
  }
}

/// Gets clipboard adapter.
ClipboardAdapter clipboardAdapter = const ClipboardAdapter._();

/// Utility functions for clipboard values.
extension ClipboardHelpers on String {
  /// Turn weird [13, 10] line breaks into dart '\n' line breaks.
  String cleanLineBreaks() {
    return replaceAll(String.fromCharCodes([13, 10]), ' ');
  }

  /// Removes line breaks from this string.
  String removeLineBreaks() {
    return cleanLineBreaks().replaceAll('\n', ' ');
  }
}
