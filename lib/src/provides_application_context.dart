import '../components.dart';
import 'component.dart';
import 'model/color/color.dart';
import 'model/color/color_scheme.dart';
import 'renderable.dart';

/// Provides a colour scheme for the application. Use on your top-most component provided to
mixin ProvidesApplicationContext on Component {
  /// Colour scheme for app. Override in your app to remap colours.
  ColorScheme? getColorScheme(ProviderContainer container) =>
      defaultColorScheme;

  /// Default colour scheme.
  static const ColorScheme defaultColorScheme = ColorScheme();

  /// Connected borders to use for dialogs.
  ConnectedBorderData getDialogBorders(ProviderContainer container) =>
      defaultDialogBorders;

  /// Default connected borders for dialogs.
  static const ConnectedBorderData defaultDialogBorders =
      ThinConnectedBorderData();
}
