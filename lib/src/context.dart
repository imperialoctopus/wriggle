// ignore_for_file: avoid_classes_with_only_static_members

import 'package:riverpod/riverpod.dart';

import 'animated.dart';
import 'dialog/dialog_adapter.dart';
import 'services/audio/audio_adapter.dart';
import 'services/clipboard/clipboard_adapter.dart';

export 'services/clipboard/clipboard_adapter.dart'
    show ClipboardAdapter, ClipboardHelpers;

/// A collection of data to provide context to your application.
class Wriggle {
  /// Provider for frame counter. Increments by 1 every animation frame.
  static StreamProvider<int> get frameCount => Animated.baseline;

  /// Clipboard adapter. Use [read] and [write] to get and set contents.
  static ClipboardAdapter get clipboard => clipboardAdapter;

  /// Audio adapter.
  static AudioAdapter get audio => audioAdapter;

  /// Dialog adapter. Use [push] and [pop] to add and remove dialogs from the stack.
  static DialogAdapter get dialog => dialogAdapter;

  /// Checks if asserts are enabled.
  static bool get isDebugMode {
    bool inDebugMode = false;
    assert(inDebugMode = true);
    return inDebugMode;
  }
}
