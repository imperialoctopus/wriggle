Wriggle is a text-based UI framework for command-line Dart. It uses a nested component system that should be familiar to Flutter users.

## Features

Selling points include:

- Slow
- Inefficient
- Cute!

## Getting started

## Usage

Run the top-level function "runApp", providing a Component as your application.
Extend the Component class and override the "build" method, returning a tree of nested Renderable objects.

```dart
void main(List<String> arguments) {
  runApp(MyApp());
}

class MyApp extends Component {
  @override
  Renderable build(ProviderContainer container) {
    return const Center(child: Text('Hello world!'));
  }
}
```

State in Wriggle is managed using the StatefulComponent<T>, built on top of Riverpod. Use any Riverpod providers you like, accessed using the ProviderContainer supplied in the "build" and "keyPressed" functions.

## Additional information

For a working example, see the project I created Wriggle for: Morning Princess, a text-based character sheet for D&D 5e (https://codeberg.org/imperialoctopus/morning-princess/).

## Credits

- Dart console from https://pub.dev/packages/dart_console
- Malison from https://pub.dev/packages/malison
